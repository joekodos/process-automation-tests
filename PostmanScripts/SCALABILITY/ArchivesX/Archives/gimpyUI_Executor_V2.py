#!/usr/bin/python



# Core Written by Alex Hernandez
# Executor Created by Joseph Kodos
# 10/03/2015
# Updated 06/15/2016 -- Version 2.0:
        #Enable executing test cases with multiple steps
""" This script will read an excel file and will automatically
    update the test cases in GIMPY.
"""
# Import all necessary modules.
import xlrd
from collections import OrderedDict
import simplejson as json
import shutil
from openpyxl import load_workbook
import bll
import sys
# Title
print "\n"
print "                                             |---- GIMPY UI EXECUTOR v2.0 ----|"
print "\n"
print "\n"

"""Login to GIMPY"""

print "   -Login into GIMPY-"

print "\n"

# Stores the username in username variable
username = raw_input(" > What is your username: ")

# Logs user in and stores the token
loginToken = bll.db.login_gimpy(username)

print "\n"

print "  -Login Successful-"

print "\n"

"""Check Paths"""

# Check path config
#Disabled for combined TCCS
# bll.db.checkPathConfig()

# Set directory path
dirPath = bll.db.setDirPath()

# Set template path
templatePath_pass_1 = bll.db.setTemplatePath_pass_1()
templatePath_pass_2 = bll.db.setTemplatePath_pass_2()
templatePath_pass_3 = bll.db.setTemplatePath_pass_3()
templatePath_fail_1 = bll.db.setTemplatePath_fail_1()
templatePath_fail_2 = bll.db.setTemplatePath_fail_2()
templatePath_fail_3 = bll.db.setTemplatePath_fail_3()
templatePath_fail_4 = bll.db.setTemplatePath_fail_4()
templatePath_fail_5 = bll.db.setTemplatePath_fail_5()


# Set excel path
excelPath = bll.db.setExcelPath_execute()

print "\n"

# Set Test Case set

name = raw_input("What do you want to name this set of Test Cases?: ")

# Set Test Case Set name

testCaseSet = dirPath + name
print "\n"
print "Your test cases will be stored here: " + testCaseSet

# Create testCaseSet

bll.db.setTSname(testCaseSet)

print "\n"
print "\n"
print "\n"

# Declare the starting position of the test cases that have been created.
print " /!\ " 
print " The Excel document may be rendered unreadable if this process is killed! "
print "\n"
print "Do NOT CTRL+C or exit this script after this point! "
print " /!\ "
print "\n"
print "\n"
ExcelQuestion = raw_input("*** Please save and close the Excel file *** \n\n"
                          "   Continue?: (Y/N) " ).lower()
if ExcelQuestion == "y":
    print "\n"
    # Declare the starting position of the test cases that have been created.
    start_row = int(raw_input(" > What row does your first test case start at: ")) - 1
    while start_row <= 1:
        start_row = int(raw_input("     > *** Please enter a number higher than 1 or 2: *** ")) - 1
else:
    sys.exit(0)

print "\n"

print "\n"

print "---------------------------- TEST CASE RESULTS---------------------------------"

print "\n"

"""Open Excel File"""

# Open the workbook and select the first worksheet
wb = bll.db.openWB(excelPath)
sh = bll.db.openWS(wb)

# List to hold dictionaries
test_cases_list = []

# List to hold feature suite id's
fs_list = []

# List to hold test Case id's
test_case_id = []


# List to hold test Case Execution Ids
test_case_execution_id = []


""" Iterate through each row in worksheet and fetch values into dict  """
for rownum in range(start_row, sh.nrows):
        test_case = OrderedDict()
        row_values = sh.row_values(rownum)
        if row_values[0] != "":
                test_case['Name'] = row_values[0]
                test_case['Description'] = row_values[1]
                test_case['Assumption'] = row_values[2]
                test_case['Limitation'] = row_values[3]
                test_case['Note'] = row_values[4]
                test_case['EstimatedTimeToRun'] = row_values[5]
                test_case['Data'] = row_values[6]
                test_case['State ID'] = int(row_values[7])
                test_case['Product ID'] = int(row_values[8])
                test_case['Priority ID'] = int(row_values[9])
                test_case['TestType ID'] = int(row_values[10])
                test_case['HowFound ID'] = int(row_values[11])
                test_case['CodeBase ID'] = int(row_values[12])
                test_case['Release ID'] = int(row_values[13])
                test_case['Action'] = row_values[14]
                test_case['Result'] = row_values[15]
                test_case['Comment'] = row_values[16]
                test_case['Test Case #'] = row_values[18]
                test_case['TC Version'] = row_values[19]
                test_case['Exec Result'] = row_values[20]
                test_case['Exec Step Result'] = row_values[21]
                test_case['Exec Step Comment'] = row_values[22]
                test_case['Exec Step Create DateTime'] = row_values[23]
                test_case['Exec Architecture Id'] = row_values[24]
                test_case['Exec Build Id'] = row_values[25]
                test_case['Exec Note'] = row_values[26]
                test_case['Exec Fail Reason Id'] = row_values[27]
                test_case['Exec Fail Reason Name'] = row_values[28]
                test_case['Exec Fail Reason Comment'] = row_values[29]
                test_case['Exec Fail Create DateTime'] = row_values[30]
                test_case['Exec Issue Label'] = row_values[31]
                test_case['Exec Issue Label State Id'] = row_values[32]
                test_case['Exec Issue Label State Name'] = row_values[33]


                # Append the values to the test_cases_list
                test_cases_list.append(test_case)

                # Append the values to the fs_list
                fs_list.append(int(row_values[17]))
                
                
        # If no title, but an action exists...
        if row_values[0] == "" and row_values[14] != "":
                test_case['Exec Step Result'] = row_values[21]
                test_case['Exec Step Comment'] = row_values[22]
                test_case['Exec Step Create DateTime'] = row_values[23]
                
                # Append the values to the test_cases_list
                test_cases_list.append(test_case)


"""
    Iterate through every item on the list, create a file for it
    and copy the information from the excel information to the template file copied.
"""

# tcTracker -- array of tcs and steps
#       e.g. ["tc", "step", "step", "tc", "tc", "step"]
tcTracker = []

for j in range(len(test_cases_list)):
        #Need to traverse through array to detect TCs and Steps
        if ('Name' in test_cases_list[j]):
                tcTracker.append("tc")
                        
        if not 'Name' in test_cases_list[j]:
                tcTracker.append("step")
        #else:
                #if (test_cases_list[j]['Name'] == "" and test_cases_list[j]['Action'] != ""):
               #if (test_cases_list[j]['Action'] != ""):
                #if ('Action' in test_cases_list[j]):
                       # tcTracker.append("step")
                

# currentTCtracker -- array of tc status
#       e.g. ["startTC","endTC","startTC","midTC","midTC","endTC","oneStepTC"]
print "tcTracker tcTracker tcTracker tcTracker tcTracker"
print tcTracker
currentTCtracker = []
                
print "len(tcTracker)"
print len(tcTracker)

#For each item in tcTracker
for k in range(len(tcTracker)):
        #if current index is the length of tcTracker list
        #e.g. len(tcTracker) == 5;
        #     k+1 = 5 (5th iteration: 0,1,2,3,4) == len(tcTracker) = 1,2,3,4,5
        #If 1+currentIndex is equal to length of the list --> we're at the end
        if(k+1 == len(tcTracker)):
                print "0xAAA  ENTERING LAST TEST CASE"
                if(tcTracker[-1] == "tc"):
                        tcFlag = "oneStepTC"
                        if not k == len(tcTracker):
                                currentTCtracker.append(tcFlag)
                                print "LAST! TC! -- Appended oneStepTC ; current is TC"
                                print "(tcTracker[-1])"
                                print tcTracker[-1]
                                print "\n"
                        
                #if last entry is step or tag or stepTag... end the current TC
                if(tcTracker[-1] == "step" or tcTracker[-1] == "stepTag" or tcTracker[-1] == "tag"):
                        tcFlag = "endTC"
                        currentTCtracker.append(tcFlag)
                        print "LAST! TC! -- Appended endTC ; current is step"
                        print "(tcTracker[-1])"
                        print tcTracker[-1]
                        print "\n"
        else:
                #Need to mark where test cases begin and end, and if a tc has only one step
                if (tcTracker[k] == "tc" and (tcTracker[k+1] == "step" or tcTracker[k+1] == "tag" or tcTracker[k+1] == "stepTag")):
                        tcFlag = "startTC"
                        currentTCtracker.append(tcFlag)
                        print "NOT Last TC -- Appended startTC ; current is TC and next (tcTracker[k+1]) is step/tag/stepTag"
                        print "(tcTracker[k])"
                        print tcTracker[k]
                        print "(tcTracker[k+1])"
                        print tcTracker[k+1]
                        print "\n"
                
                #If current is a Step, stepTag, or tag, and next is TC
                if ((tcTracker[k] == "step" or tcTracker[k] == "stepTag" or tcTracker[k] == "tag") and tcTracker[k+1] == "tc"):
                        #if current is a step and next is a tc
                        tcFlag = "endTC"
                        currentTCtracker.append(tcFlag)
                        print "NOT Last TC -- Appended endTC ; current is step and next (tcTracker[k+1]) is TC"
                        print "(tcTracker[k])"
                        print tcTracker[k]
                        print "(tcTracker[k+1])"
                        print tcTracker[k+1]
                        print "\n"
                
                #if current is step and next is a step
                if ((tcTracker[k] == "step" or tcTracker[k] == "stepTag" or tcTracker[k] == "tag") and (tcTracker[k+1] == "step" or tcTracker[k+1] == "stepTag" or tcTracker[k+1] == "tag")):
                        tcFlag = "midTC"
                        currentTCtracker.append(tcFlag)
                        print "NOT Last TC -- Appended midTC ; tcTracker[k] is step/tag/stepTag and next is also a step"
                        print "(tcTracker[k])"
                        print tcTracker[k]
                        print "(tcTracker[k+1])"
                        print tcTracker[k+1]
                        print "\n"
                        
                #if current and next is a tc
                if(tcTracker[k] == "tc" and tcTracker[k+1] == "tc"):
                        tcFlag = "oneStepTC"
                        print "NOT Last TC -- Appended oneStepTC ; current is TC and next is TC"
                        currentTCtracker.append(tcFlag)
                        print "(tcTracker[k])"
                        print tcTracker[k]
                        print "(tcTracker[k+1])"
                        print tcTracker[k+1]
                        print "\n"
                        

# for k in range(len(tcTracker) + 1):

        # #what about last test case? -- current is a tc and next is empty
        # #if( (k+1) > ( len(tcTracker) - 1)):
        # if( (k+1) > ( len(tcTracker) - 1)):
                # #if last entry is tc... it is a one step TC
                # if(tcTracker[k-1] == "tc"):
                        # tcFlag = "oneStepTC"
                        # print "ja893j393nm3893nmd83h48t5ngf8"
                        # if not k == len(tcTracker):
                                # currentTCtracker.append(tcFlag)
                                # print "LAST! TC! -- Appended oneStepTC ; current is TC"
                                # print "(tcTracker[k-1])"
                                # print tcTracker[k-1]
                                # print "\n"
                        
                # #if last entry is step... end the current TC
                # if(tcTracker[k-1] == "step"):
                        # tcFlag = "endTC"
                        # currentTCtracker.append(tcFlag)
        # else:
                # #Need to mark where test cases begin and end, and if a tc has only one step
                # if (tcTracker[k] == "tc" and tcTracker[k+1] == "step"):
                        # tcFlag = "startTC"
                        # currentTCtracker.append(tcFlag)
                        
                # #if current is a step and next is a tc
                # if (tcTracker[k] == "step" and tcTracker[k+1] == "tc"):
                        # tcFlag = "endTC"
                        # currentTCtracker.append(tcFlag)
                
                # #if current is step and next is a step
                # if (tcTracker[k] == "step" and tcTracker[k+1] == "step"):
                        # tcFlag = "midTC"
                        # currentTCtracker.append(tcFlag)
                        
                # #if current and next is a tc
                # if(tcTracker[k] == "tc" and tcTracker[k+1] == "tc"):
                        # tcFlag = "oneStepTC"
                        # print "0000000000000_aaaaaaaaaaa"
                        # currentTCtracker.append(tcFlag)
                

tcIDtracker = 0
#For Each Test Case (or set of TC Steps):
for i in range(len(test_cases_list)):


        
        #Need to Create File based on # of Steps and on # of Issues (if applicable)
    
        #if a new TC
        if(currentTCtracker[i] == "startTC"):
                
                #When writing file, use this to write to the correct file
                curTCindex = i
                
                #if pass...
                if(test_cases_list[i]['Exec Result'] == "Passed"):
                        curTCstatus = "Passed"
                        
                        
                        #Assemble pass_a_test_case
                        currentTestCase = open(templatePath_pass_1, "r").read()
                        #for first step
                        currentTestCase = currentTestCase + open(templatePath_pass_2, "r").read()
                        #append comma
                        currentTestCase = currentTestCase + ","
                        print "AAAAAAAAAAAAAAAAAAAAAAAAAA"
                        print currentTestCase
                        print "AAAAAAAAAAAAAAAAAAAAAAAAAA"
                        
                #if fail...
                if(test_cases_list[i]['Exec Result'] == "Failed"):
                        curTCstatus = "Failed"
                        #Assemble fail_a_test_case
                        currentTestCase = open(templatePath_fail_1, "r").read()
                        #for first step
                        currentTestCase = currentTestCase + open(templatePath_fail_2, "r").read()
                        #append comma
                        currentTestCase = currentTestCase + ","
                        print "BBBBBBBBBBBBBBBBBBBBBBBBBB"
                        print currentTestCase
                        print "BBBBBBBBBBBBBBBBBBBBBBBBBB"
                        
                        
                        
        
        #if complete TC
        if(currentTCtracker[i] == "oneStepTC"):
                
                #When writing file, use this to write to the correct file
                curTCindex = i
                
                #if pass...
                print "test_cases_list[i]"
                print test_cases_list[i]
                print i
                print "currentTCtracker[i]"
                print currentTCtracker[i]
                print "len(currentTCtracker)"
                print len(currentTCtracker)
                print "len(test_cases_list)"
                print len(test_cases_list)
                if(test_cases_list[i]['Exec Result'] == "Passed"):
                        curTCstatus = "Passed"
                        #Assemble pass_a_test_case
                        currentTestCase = open(templatePath_pass_1, "r").read()
                        #for first step
                        currentTestCase = currentTestCase + open(templatePath_pass_2, "r").read()
                        #append end
                        currentTestCase = currentTestCase + open(templatePath_pass_3, "r").read()
                        print "oneStepTC ASSEMBLY oneStepTC ASSEMBLY  PASS"
                        print currentTestCase
                        print "oneStepTC ASSEMBLY oneStepTC ASSEMBLY  PASS"
                        
                #if fail...
                if(test_cases_list[i]['Exec Result'] == "Failed"):
                        curTCstatus = "Failed"
                        #Assemble fail_a_test_case
                        currentTestCase = open(templatePath_fail_1, "r").read()
                        #for first step
                        currentTestCase = currentTestCase + open(templatePath_fail_2, "r").read()
                        #append end
                        currentTestCase = currentTestCase + open(templatePath_fail_3, "r").read()
                        currentTestCase = currentTestCase + open(templatePath_fail_4, "r").read()
                        currentTestCase = currentTestCase + open(templatePath_fail_5, "r").read()
                        print "oneStepTC ASSEMBLY oneStepTC ASSEMBLY  FAIL"
                        print currentTestCase
                        print "oneStepTC ASSEMBLY oneStepTC ASSEMBLY  FAIL"
                        
                print "currentTestCase currentTestCase currentTestCase currentTestCase"
                print currentTestCase
                        
                        
        #if a step
        if(currentTCtracker[i] == "midTC"):
                #if pass...
                if(test_cases_list[curTCindex]['Exec Result'] == "Passed"):
                        #append step
                        currentTestCase = currentTestCase + open(templatePath_pass_2, "r").read()
                        #append comma
                        currentTestCase = currentTestCase + ","
                        print "midTC ASSEMBLY midTC ASSEMBLY  PASS"
                        print currentTestCase
                        print "midTC ASSEMBLY midTC ASSEMBLY  PASS"
                        
                #if fail...
                if(test_cases_list[curTCindex]['Exec Result'] == "Failed"):
                        #append step
                        currentTestCase = currentTestCase + open(templatePath_fail_2, "r").read()
                        #append comma
                        currentTestCase = currentTestCase + ","
                        print "midTC ASSEMBLY midTC ASSEMBLY  FAIL"
                        print currentTestCase
                        print "midTC ASSEMBLY midTC ASSEMBLY  FAIL"
                        
                        
                        
        #if a final step
        if(currentTCtracker[i] == "endTC"):
                #if pass...
                #need logic here to check previous test_cases_list[i-1] --> we're in a step, we have no way of telling if this is a passed or failed TC based on just the step
                #if a step fails, we need to record it and track it and reset it when TC ends
                #if(test_cases_list[i]['Exec Result'] == "Passed"):
                print "test_cases_list[i]  $$$$"
                print test_cases_list[i]
                print "curTCstatus"
                print curTCstatus
                if(curTCstatus == "Passed"):
                        #append comma
                        #currentTestCase = currentTestCase + ","
                        #append final step
                        currentTestCase = currentTestCase + open(templatePath_pass_2, "r").read()
                        #append end
                        currentTestCase = currentTestCase + open(templatePath_pass_3, "r").read()
                        print "endTC ASSEMBLY endTC ASSEMBLY  PASS"
                        print currentTestCase
                        print "endTC ASSEMBLY endTC ASSEMBLY  PASS"
                
                #if fail...
                if(curTCstatus == "Failed"):
                        #append comma
                        #currentTestCase = currentTestCase + ","
                        #append final step
                        currentTestCase = currentTestCase + open(templatePath_fail_2, "r").read()
                        #append end
                        currentTestCase = currentTestCase + open(templatePath_fail_3, "r").read()
                        currentTestCase = currentTestCase + open(templatePath_fail_4, "r").read()
                        currentTestCase = currentTestCase + open(templatePath_fail_5, "r").read()
                        print "endTC ASSEMBLY endTC ASSEMBLY  FAIL"
                        print currentTestCase
                        print "endTC ASSEMBLY endTC ASSEMBLY  FAIL"
                        
                        
        #Write template file
        templateFile = open(testCaseSet + '/%s%d.json' % (name,i), 'w')
        templateFile.write(currentTestCase)
        templateFile.close()
        
        
        
        
        #if (test_cases_list[i]['Exec Result'] == "Passed" and test_cases_list[i]['Exec Step Result'] == "Passed"):
        #       shutil.copyfile(templatePath, testCaseSet + '/%s%d.json' % (name,i))
            
        #if (test_cases_list[i]['Exec Result'] == "Failed" and test_cases_list[i]['Exec Step Result'] == "Failed"):
        #       shutil.copyfile(templatePath2, testCaseSet + '/%s%d.json' % (name,i))
        
        # tcTracker -- array of tcs and steps
        #       e.g. ["tc", "step", "step", "tc", "tc", "step"]
        # currentTCtracker -- array of tc status
        #       e.g. ["startTC","endTC","startTC","midTC","midTC","endTC","oneStepTC"]

        # execute_fail_a_test_case_01.json -- Top                   templatePath_fail_1
        # execute_fail_a_test_case_02.json -- Step Body             templatePath_fail_2
        # execute_fail_a_test_case_03.json -- Middle                templatePath_fail_3
        # execute_fail_a_test_case_04.json -- Issue Body            templatePath_fail_4
        # execute_fail_a_test_case_05.json -- Bottom                templatePath_fail_5

        # execute_pass_a_test_case_01.json -- Top                   templatePath_pass_1
        # execute_pass_a_test_case_01.json -- Step Body             templatePath_pass_2
        # execute_pass_a_test_case_01.json -- Bottom                templatePath_pass_2

        #assembled_fail_tc_body = open('E:\OneDrive - Interactive Intelligence\PureCloud\REPOS\process-automation-tests\PostmanScripts\GimpyUI_Executor_V2\execute_fail_a_test_case_01.json' + '/%s%d.json' % (name,i), "r")
    
    

        print "currentTCtracker[i]"
        print currentTCtracker[i]
        print "currentTCtracker"
        print currentTCtracker
        
        print "iiiii xxxxxx"
        print i
        
        
        #if a completeTC
        if(currentTCtracker[i] == "endTC" or currentTCtracker[i] == "oneStepTC"):
                # Opens, loads file we want to be written and stores it in the "data" variable
                jsonFile = open(testCaseSet + '/%s%d.json' % (name,i), "r")
                data = json.load(jsonFile, object_pairs_hook=OrderedDict)
                jsonFile.close()
                
                print "currentTCtracker 0000EEEEE"
                print currentTCtracker
                print "curTCstatus curTCstatus curTCstatus 00000EEEE"
                print curTCstatus
                
                
                #if (test_cases_list[i]['Exec Result'] == "Passed" and test_cases_list[i]['Exec Step Result'] == "Passed"):
                if (curTCstatus == "Passed"):
                        # Fetches each entity in JSON file and replaces element with new imported information
                        data["Note"] = test_cases_list[curTCindex]['Exec Note']
                        trim_exec_build_id = str(test_cases_list[curTCindex]['Exec Build Id']).split('.')
                        data['Build']['Id'] = trim_exec_build_id[0]
                        data['Result'] = test_cases_list[curTCindex]['Exec Result']
                        trim_exec_architecture_id = str(test_cases_list[curTCindex]['Exec Architecture Id']).split('.')
                        data['Architecture']['Id'] = trim_exec_architecture_id[0]
                        print "len(data['ExecutionSteps']) mmmmm    passs pass pass"
                        print len(data['ExecutionSteps'])
                        tempIndex = curTCindex
                        for m in range(len(data['ExecutionSteps'])):
                                data['ExecutionSteps'][m]['Result'] = test_cases_list[tempIndex]['Exec Step Result']
                                data['ExecutionSteps'][m]['Comment'] = test_cases_list[tempIndex]['Exec Step Comment']
                                data['ExecutionSteps'][m]['CreatedDateTime'] = test_cases_list[tempIndex]['Exec Step Create DateTime']
                                tempIndex = tempIndex + 1
                                #if(m > 1):
                                #        i = i + 1
                                        
                
               # if (test_cases_list[i]['Exec Result'] == "Failed" and test_cases_list[i]['Exec Step Result'] == "Failed"):
                #if (test_cases_list[i]['Exec Result'] == "Failed"):
                if (curTCstatus == "Failed"):
                        data["Note"] = test_cases_list[curTCindex]['Exec Note']
                        trim_exec_build_id = str(test_cases_list[curTCindex]['Exec Build Id']).split('.')
                        data['Build']['Id'] = trim_exec_build_id[0]
                        data['Result'] = test_cases_list[curTCindex]['Exec Result']
                        trim_exec_architecture_id = str(test_cases_list[curTCindex]['Exec Architecture Id']).split('.')
                        data['Architecture']['Id'] = trim_exec_architecture_id[0]
                        print "len(data['ExecutionSteps']) nnnnnn    fail fail fail"
                        print len(data['ExecutionSteps'])
                        tempIndex = curTCindex
                        for n in range(len(data['ExecutionSteps'])):
                                data['ExecutionSteps'][n]['Result'] = test_cases_list[tempIndex]['Exec Step Result']
                                data['ExecutionSteps'][n]['Comment'] = test_cases_list[tempIndex]['Exec Step Comment']
                                data['ExecutionSteps'][n]['CreatedDateTime'] = test_cases_list[tempIndex]['Exec Step Create DateTime']
                                tempIndex = tempIndex + 1
                                #if(n > 1):
                                #        i = i + 1
                        
                        trim_exec_fail_reason_id = str(test_cases_list[curTCindex]['Exec Fail Reason Id']).split('.')
                        data['FailureReason']['Reason']['Id'] = trim_exec_fail_reason_id[0]
                        data['FailureReason']['Reason']['Name'] = test_cases_list[curTCindex]['Exec Fail Reason Name']
                        data['FailureReason']['Comment'] = test_cases_list[curTCindex]['Exec Fail Reason Comment']
                        data['FailureReason']['CreatedDateTime'] = test_cases_list[curTCindex]['Exec Fail Create DateTime']
                        data['FailureReason']['Issues'][0]['IssueLabel'] = test_cases_list[curTCindex]['Exec Issue Label']
                        trim_exec_issue_label_state_id = str(test_cases_list[curTCindex]['Exec Issue Label State Id']).split('.')
                        data['FailureReason']['Issues'][0]['State']['Id'] = trim_exec_issue_label_state_id[0]
                        data['FailureReason']['Issues'][0]['State']['Name'] = test_cases_list[curTCindex]['Exec Issue Label State Name']
                
                #Add test case number to TC ID list
                tcIDtracker = tcIDtracker + 1
                test_case_id.append(test_cases_list[curTCindex]['Test Case #'])
                print "test_cases_list[i]['Test Case #']"
                print test_cases_list[curTCindex]['Test Case #']
                print "str(test_case_id[i])"
                print str(test_case_id)
                print "iiii"
                print i
                print "curTCindex curTCindex curTCindex"
                print curTCindex
                print "tcIDtracker tcIDtracker tcIDtracker"
                print tcIDtracker
                test_case_id_str = str(test_case_id[tcIDtracker - 1])
                test_case_id_trimmed = test_case_id_str.split('.')
                
                """Get the Test Case Version"""
                getTestCase_response = bll.db.get_a_test_case(loginToken, test_case_id_trimmed[0])
                
                
                """Execute the Test Case"""
                # tc_id will hold the value of the id in the test_case_id list
                #tc_id = str(test_case_id[i])
                executeTestCase_response = bll.db.execute_a_test_case(data, loginToken, test_case_id_trimmed[0], getTestCase_response['Version'])
                print "RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR str(executeTestCase_response)"
                print str(executeTestCase_response)
                
                # Print the results of test cases of each test case that has been created
                print "Test Case"
                print "---------"
                print "Test Case Name: " +    str(test_cases_list[curTCindex]['Name'])
                print "Test Case Execution ID: " + str(executeTestCase_response['Id'])
                print "Test Case Execution Status: " + str(data['Result'])
                print "Executed Successfully!"
                
                
                # Append the responses to test_case_execution_id list to write to excel file
                test_case_execution_id.append(executeTestCase_response['Id'])
                
                """Writes new information to new file and closes it"""
                
                jsonFile = open(testCaseSet + "/%s%d.json" % (name,i), "w+")
                jsonFile.write(json.dumps(test_cases_list[curTCindex]['Name'], indent=4 * ' '))
                jsonFile.write(json.dumps(executeTestCase_response, indent=4 * ' '))
                jsonFile.close()
                
                print "\n"

    
"""Writes the Test Case Version back to the excel file"""
# Load the excel file
wb = load_workbook(filename=excelPath)
# Get the sheet
ws = wb.get_sheet_by_name("Sheet1")
row1 = start_row + 1

#item counter for test_case_execution_id array
item = 0

#Iterate over tcs and steps
for entry in range(len(tcTracker)):
        #Check if current row is blank
        if str(ws['A'+str(row1)].value) == "None" and str(ws['O'+str(row1)].value) == "None":
                print "Entering blank row!"
                #Skip 3 excel rows
                row1 = row1 + 3
                # Write the test case execution id to the excel file
                ws['AI'+str(row1)] = test_case_execution_id[item]
                #Move to next excel row
                row1 = row1 + 1
                #Increment test_case_execution_id counter
                item = item + 1
        #Check if current row is just a step
        elif str(ws['A'+str(row1)].value) == "None" and str(ws['O'+str(row1)].value) != "None":
                #Skip the row
                row1 = row1 + 1
                ##Revert the item
                #item = item - 1
        #Else, current row is not blank and not a step -- a full TC
        else:
                # Write the test case execution id to the excel file
                ws['AI'+str(row1)] = test_case_execution_id[item]
                row1 = row1 + 1
                item = item + 1



#Iterate through the returned set of test case id's
#for item in range(len(test_case_execution_id)):

        #Check if current row is blank
        #if str(ws['A'+str(row1)].value) == "None" and str(ws['O'+str(row1)].value) == "None":
         #       print "Entering blank row!"
                #Skip the row
          #      row1 = row1 + 3
                # Write the test case execution id to the excel file
           #     ws['AI'+str(row1)] = test_case_execution_id[item]
            #    row1 = row1 + 1
        #Check if current row is just a step
        #elif str(ws['A'+str(row1)].value) == "None" and str(ws['O'+str(row1)].value) != "None":
                #Skip the row
         #       row1 = row1 + 1
                ##Revert the item
                #item = item - 1
        #Else, current row is not blank and not a step -- a full TC
        #else:
                # Write the test case execution id to the excel file
         #       ws['AI'+str(row1)] = test_case_execution_id[item]
#                row1 = row1 + 1

# save the excel file
wb.save(excelPath)

print "\n"

print "---------------------------- EXECUTIONS COMPLETED---------------------------------"

print "\n"















