Welcome to the Test Case Creation System (TCCS) Readme!

This is a suite of scripts to help create, update, retrieve, and execute test cases in bulk using Excel.


# BEFORE BEGINNING NOTE: The scripts are pointed to the TEST TCDB (QF-Kentucky) by default
        1| Possibly execute a test run by following the readme below using bogus data
        2| When ready, open /TCCS/bll/db.py
        3| Find "qf-kentucky" and replace with "tcdb.inin.com"
        4| Save file. Now you will be hitting the production TCDB.
        5| Open /TCCS/config.ini
        6| Update the paths to the files on your local system
        7| Save file. Now the scripts will point to the desired excel files and TCDB templates.
        
# SUPPORTED DATA NOTE:
        1| Attributes are not yet supported
        2| Only 1 issue per test case is supported
        3| Only 1 feature suite per test case is supported
        4| Multiple steps are supported
        5| Multiple tags are supported
        6| Release tags are not yet supported

        
####  gimpyCreator_V2             - Create TCs
        1| Open TestCaseTracker.xlsx
        2| Populate each row with test case data, steps, and tags
        3| If a test case has more than one step or tag, insert data into the next row
                a| Additional steps and/or tags must NOT have data in other fields.
                b| Test Case Example -- 3 steps, 2 tags:
                        row3            <TC Title>,<TC_Action>,<TC_Tag>         TC30001.base&step1&tag1
                        row4                       <TC_Action>,<TC_Tag>         TC30001.step2&tag2
                        row5                       <TC_Action                   TC30001.step3
        4| If visual separation is desired, skip 3 rows. A common use case for this is to separate feature suites. Skipping less than 3 rows is not yet supported.
        5| Once data is inserted, save and close the Excel file
        6| Run the python script
        7| Log in as your Gimpy User
        8| Give the set of test cases a name (not too important, just needs to be a unique name because a new folder will be created)
        9| Specify the row the first test case is at. If using the default Excel file and starting from the first blank row, the row number is 3
       10| Hit enter and wait for test cases to be created
       11| New test case numbers are written to Excel, so open the Excel file to retrieve them

       
####  gimpyUI_Executor_V2         - Execute TCs
        1| Open TestCaseUI_Executor.xlsx
        2| Populate each row, just like for gimpyCreator_V2
                a| Tags are not yet supported, so when copying data from other Excel spreadsheets, do not select the tags column
        3| Put "Failed" or "Passed" for each TC step. Remember, if one TC step fails, the "Exec Result" column will be "Failed"
                a| If a TC has multiple steps, the "Exec Result" column only needs to be populated for the first step
        4| Update the build, and any other execution-related data fields
        5| Close the Excel file
        5| Run the script
        6| Open the Excel file and verify that execution IDs are present for each TC in the "Exec Id" column
        
        
        
####  gimpyUpdater_V2             - Update TCs
        1| Open TestCaseUpdater.xlsx
        2| Populate each row, just like for gimpyCreator_V2
        3| Update fields as necessary
                a| May remove a test case from a feature suite by removing the FS ID for a given TC
                b| May add a test case to a feature suite by adding a FS ID to a given TC in the "Feature Suite" column
        4| Close the Excel file
        5| Run the script
        6| The latest version is written back to the Excel file, so open the file up to view the latest version



####  tcdb_pull_data_tcList       - Retrieve a list of testcases
        1| Open TestCaseRetriever.xlsx and clear any TC data
        2| Open testSuiteList.txt and add in test case IDs, one per line
        3| Save and close testSuiteList.txt
        4| Run the script
        5| Open TestCaseRetriever.xlsx to see data for the specified list of test cases



####  tcdb_pull_data_tsList       - Retrieve all test cases from each testsuite in a list of testsuites
        1| Open TestCaseRetriever.xlsx and clear any TC data
        2| Open testSuiteList.txt and add in test suite IDs, one per line
        3| Save and close testSuiteList.txt
        4| Run the script
        5| Open TestCaseRetriever.xlsx to see all test cases' data for the specified list of test suites
        6| Each test suite will be separated by 3 blank lines


#### Helpful Tips:
        1| Run tcdb_pull_data_tcList or tcdb_pull_data_tsList to retrieve test cases
        2| Copy data (minus "tags" column if pasting into gimpyUI_Executor_V2) and paste into desired Excel file (such as TestCaseUpdater.xlsx, to update a bunch of test cases in bulk)
