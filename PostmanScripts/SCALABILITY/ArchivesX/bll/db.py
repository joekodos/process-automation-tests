import platform
import dal


def get_oauth2_access_token(env, authorization):
    """Login to Public API V2"""
    # Get a Client Authorization Grant
    url = "https://login.inin" + env + ".com/oauth/token"

    # Method Headers:
    requestHeaders = {
        'Authorization': 'Basic ' + authorization,
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    # Method body:
    requestBody = {
        'grant_type': 'client_credentials'
    }
    # Send the request with parameters and store it in login_response
    response = dal.apiv2.get_oauth2_access_token(url, requestBody, requestHeaders)

    # Use the .json function() to get the data in json format and then we store it in login_response_json variable
    response_json = response.json()
    print "response_json response_json response_json response_json"
    print response_json

    # Store the token
    access_token = response_json["access_token"]
    return access_token



def private_publish_flow(userId, orgId, flowId, version):

    """Publish a Flow using Private API"""
    #orgId = "8a3a828a-5e49-4cc5-a59b-627a1e36f2f6"
    #flowId = "1bc85720-d6a1-9842-b0bb-385b317ad547"
    #version = "1.0"
    host = "automate.us-east-1.inindca.com"

    # This is the process automation URL used to publish a flow
    publish_flow_url = "https://" + host + "/processautomation/v1/organizations/" + orgId + "/flows/definitions/" + flowId + "/commands/architectPublish?version=" + version

    # Method Headers
    publish_flow_headers = {'Content-Type': 'application/json',
                               'ININ-User-Id': '%s' % userId}

    # Specify request body json data and headers
    publish_flow_response = dal.privateAPI.publish_flow(publish_flow_url, publish_flow_headers)


    """Store the publish_flow_response """

    # Use the .json function() to get the data in json format and then we store it in addTestCase_response variable
    publish_flow_response = publish_flow_response.json()
    return publish_flow_response



def launch_flow(host, flowConfigId, launchType, access_token):

    """Launch a flow using Public API v2"""
    # This is the process automation URL used to publish a flow
    launch_flow_url = "https://" + host + "/api/v2/processautomation/flows/instances"

    # Method Headers
    launch_flow_headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer %s' % access_token
    }
    # Method Body
    launch_flow_body = {
        "flowConfigId": '%s' % flowConfigId,
        "launchType": '%s' % launchType
    }
    # Specify request body json data and headers
    launch_flow_response = dal.apiv2.launch_flow(launch_flow_url, launch_flow_body, launch_flow_headers)

    """Store the launch_flow_response """
    # Use the .json function() to get the data in json format and then we store it in addTestCase_response variable
    launch_flow_response = launch_flow_response.json()
    return launch_flow_response
















""" ***************************************  LEGACY *************************************** """

# OLD LOGIN
def login_api(env, username, orgName, password):

    """Login to Public API V2"""

    # This is the Public API v2 URL used to login
    apiv2URL = "https://apps.inin" + env + ".com/api/v2/login"

    # Method body:
    loginPayload = {
        "email": username,
        "orgName": orgName,
        "password": password
    }

    # Method Headers:
    loginHeaders = {'content-type': 'application/json'}

    # Send the request with parameters and store it in login_response
    login_response = dal.apiv2.login_apiv2_call(apiv2URL, loginPayload, loginHeaders)

    # Use the .json function() to get the data in json format and then we store it in login_response_json variable
    login_response_json = login_response.json()

    # Store the token
    token = login_response_json["res"]["person"]["general"]["guid"]
    return token





def add_a_test_case(body, token):

    """Create the Test Case"""

    # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
    addTestCaseURL = "http://tcdb.inin.com/tcdbv2/api/testcases" # <--- Use this test env url first then Prod
                                                            # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases

    # Method body
    addTestCase_PayLoad = body

    # Method Headers
    addTestCase_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    addTestCase_response = dal.tcdb.add_a_test_case_call(addTestCaseURL, addTestCase_PayLoad, addTestCase_headers)


    """Store the addTestCase response"""

    # Use the .json function() to get the data in json format and then we store it in addTestCase_response variable
    addTestCase_response = addTestCase_response.json()
    return addTestCase_response




def update_a_test_case(body, token, testCaseNumber, testCaseVersion):

    """Create the Test Case"""

    # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
    updateTestCaseURL = 'http://tcdb.inin.com/tcdbv2/api/testcases/' + str(testCaseNumber) + '/' + str(testCaseVersion) # <--- Use this test env url first then Prod
                                                            # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases

    # Method body
    updateTestCase_PayLoad = body

    # Method Headers
    updateTestCase_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    updateTestCase_response = dal.tcdb.update_a_test_case_call(updateTestCaseURL, updateTestCase_PayLoad, updateTestCase_headers)


    """Store the addTestCase response"""

    # Use the .json function() to get the data in json format and then we store it in updateTestCase_response variable
    updateTestCase_response = updateTestCase_response.json()
    
    return updateTestCase_response


def get_a_test_case(token, testCaseNumber):

        """Get a Test Case"""
        # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
        getTestCaseURL = "http://tcdb.inin.com/tcdbv2/api/testcases/" + str(testCaseNumber)   # <--- Use this test env url first then Prod

                                                                                                  # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases
        # Method Headers
        getTestCase_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}
        # Specify request body json data and headers
        getTestCase_response = dal.tcdb.get_a_test_case_call(getTestCaseURL, getTestCase_headers)
        
        """Store the getTestCase response"""
        # Use the .json function() to get the data in json format and then we store it in getTestCase_response variable
        getTestCase_response = getTestCase_response.json()
        return getTestCase_response

def execute_a_test_case(body, token, testCaseNumber, testCaseVersion):

    """Create the Test Case"""

    # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
    executeTestCaseURL = 'http://tcdb.inin.com/tcdbv2/api/testcases/' + str(testCaseNumber) + '/' + str(testCaseVersion) + '/Executions' # <--- Use this test env url first then Prod
                                                            # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases


    # Method body
    executeTestCase_PayLoad = body

    # Method Headers
    executeTestCase_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    executeTestCase_response = dal.tcdb.execute_a_test_case_call(executeTestCaseURL, executeTestCase_PayLoad, executeTestCase_headers)


    """Store the addTestCase response"""

    # Use the .json function() to get the data in json format and then we store it in updateTestCase_response variable
    executeTestCase_response = executeTestCase_response.json()
    return executeTestCase_response
""" ***************************************  FEATURE SUITE *************************************** """

def get_a_test_suite(token, testSuiteNumber):

        """Get a Test SUITE"""
        # This is the gimpy URL used to get a test suite from the DB                       *****IMPORTANT*****
        getTestSuiteURL = "http://tcdb.inin.com/tcdbv2/api/testsuites/" + str(testSuiteNumber)   # <--- Use this test env url first then Prod

                                                                                                  # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases
        # Method Headers
        getTestSuite_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}
        # Specify request body json data and headers
        getTestSuite_response = dal.tcdb.get_a_test_suite_call(getTestSuiteURL, getTestSuite_headers)
        
        """Store the getTestSuite response"""
        # Use the .json function() to get the data in json format and then we store it in getTestCase_response variable
        getTestSuite_response = getTestSuite_response.json()
        return getTestSuite_response

def update_feature_suite(fsid, testcaseid, token):
    fixedFSID = fsid.split(".")
    fsidX = fixedFSID[0]
    """Update Feature Suite"""

    # This is the gimpy URL used to add a test case to the DB
    updateFsURL = "http://tcdb.inin.com/tcdbv2/api/testsuites/"+fsidX+"/testcases"
    print "updateFsURL updateFsURL updateFsURL updateFsURL"
    print updateFsURL
    # Method body
    updateFsPayload = [
    {
        "Id": testcaseid
    }
    ]
    print "updateFsPayload updateFsPayload updateFsPayload"
    print updateFsPayload

    # Method Headers
    updateFsHeaders = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    updateFsResponse = dal.tcdb.update_feature_suite_call(updateFsURL, updateFsPayload, updateFsHeaders)
    return updateFsResponse

def delete_tc_from_feature_suite(fsid, testcaseid, token):
    fsidStr = str(fsid)
    fixedFSID = fsidStr.split(".")
    fsidX = fixedFSID[0]
    """Delete From Feature Suite"""

    # This is the gimpy URL used to add a test case to the DB
    deleteFsURL = "http://tcdb.inin.com/tcdbv2/api/testsuites/"+fsidX+"/testcases/" + testcaseid
    print "deleteFsURL deleteFsURLdeleteFsURL"
    print deleteFsURL

    # Method Headers
    deleteFsHeaders = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    deleteFsResponse = dal.tcdb.delete_tc_from_feature_suite_call(deleteFsURL, deleteFsHeaders)
    return deleteFsResponse
""" ***************************************  TAGS *************************************** """
    
def get_a_test_case_tags(token, testCaseNumber, testCaseVersion):

        """Get a Test Case's Tags"""
        # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
        getTestCaseTagsURL = "http://tcdb.inin.com/tcdbv2/api/testcases/" + str(testCaseNumber) + "/" + str(testCaseVersion) + "/tags"    # <--- Use this test env url first then Prod

                                                                                                  # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases
        # Method Headers
        getTestCaseTags_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}
        # Specify request body json data and headers
        getTestCaseTags_response = dal.tcdb.get_a_test_case_tags_call(getTestCaseTagsURL, getTestCaseTags_headers)

        """Store the getTestCaseTags response"""
        # Use the .json function() to get the data in json format and then we store it in getTestCaseTags_response variable
        getTestCaseTags_response = getTestCaseTags_response.json()
        print "getTestCaseTags_response: "
        print getTestCaseTags_response
        return getTestCaseTags_response
        
        
def patch_a_test_case_tags(token, testCaseNumber, testCaseVersion, patchTestCaseTags_data):
        print "patchTestCaseTags_data *********"
        print "patchTestCaseTags_data *********"
        print "patchTestCaseTags_data *********"
        print patchTestCaseTags_data
        """Add to a Test Case's Tags"""
        # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
        patchTestCaseTagsURL = "http://tcdb.inin.com/tcdbv2/api/testcases/" + str(testCaseNumber) + "/" + str(testCaseVersion) + "/tags"    # <--- Use this test env url first then Prod

                                                                                                  # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases
        # Method Headers
        patchTestCaseTags_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}
        
        # Specify request body json data and headers
        patchTestCaseTags_response = dal.tcdb.patch_a_test_case_tags_call(patchTestCaseTagsURL, patchTestCaseTags_data, patchTestCaseTags_headers)
        
        """Store the getTestCaseTags response"""
        # Use the .json function() to get the data in json format and then we store it in getTestCaseTags_response variable
        patchTestCaseTags_response = patchTestCaseTags_response.json()
        print "patchTestCaseTags_response: "
        print patchTestCaseTags_response
        return patchTestCaseTags_response


def delete_a_test_case_tag(token, testCaseNumber, testCaseVersion, tagToDelete):
        
        """Add to a Test Case's Tags"""
        # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
        deleteTestCaseTagsURL = "http://tcdb.inin.com/tcdbv2/api/testcases/" + str(testCaseNumber) + "/" + str(testCaseVersion) + "/tags/" + tagToDelete + "/"   # <--- Use this test env url first then Prod
        
                                                                                                  # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases
        # Method Headers
        deleteTestCaseTags_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}
                               
        print "deleteTestCaseTagsURL deleteTestCaseTagsURL deleteTestCaseTagsURL"
        print deleteTestCaseTagsURL
        
        # Specify request body json data and headers
        deleteTestCaseTags_response = dal.tcdb.delete_a_test_case_tag_call(deleteTestCaseTagsURL, deleteTestCaseTags_headers)
        print "deleteTestCaseTags_response deleteTestCaseTags_response deleteTestCaseTags_response"
        print deleteTestCaseTags_response
        """Store the getTestCaseTags response"""
        # Use the .json function() to get the data in json format and then we store it in getTestCaseTags_response variable
        deleteTestCaseTags_response = deleteTestCaseTags_response.json()
        print "deleteTestCaseTags_response: "
        print deleteTestCaseTags_response
        return deleteTestCaseTags_response
    
    
    
""" ***************************************  CONFIG *************************************** """

def checkPathConfig():
    # Get os type
    os_type = platform.system()
    if os_type == "Windows":
        dal.config.checkWindowsConfig()
    else:
        dal.config.checkMacConfig()

def setDirPath():
    dirPath = dal.config.getDirPath()
    return dirPath


def set_add_a_test_case_01():
    add_a_test_case_01 = dal.config.get_add_a_test_case_01()
    return add_a_test_case_01

def set_add_a_test_case_02():
    add_a_test_case_02 = dal.config.get_add_a_test_case_02()
    return add_a_test_case_02

def set_add_a_test_case_03():
    add_a_test_case_03 = dal.config.get_add_a_test_case_03()
    return add_a_test_case_03



def setTemplatePath_update_01():
    templatePath_update_01 = dal.config.getTemplatePath_update_01()
    return templatePath_update_01
    
def setTemplatePath_update_02():
    templatePath_update_02 = dal.config.getTemplatePath_update_02()
    return templatePath_update_02
    
def setTemplatePath_update_03():
    templatePath_update_03 = dal.config.getTemplatePath_update_03()
    return templatePath_update_03




def setTemplatePath_pass_1():
    templatePath_pass_1 = dal.config.getTemplatePath_pass_1()
    return templatePath_pass_1

def setTemplatePath_pass_2():
    templatePath_pass_2 = dal.config.getTemplatePath_pass_2()
    return templatePath_pass_2
    
def setTemplatePath_pass_3():
    templatePath_pass_3 = dal.config.getTemplatePath_pass_3()
    return templatePath_pass_3
    
def setTemplatePath_fail_1():
    templatePath_fail_1 = dal.config.getTemplatePath_fail_1()
    return templatePath_fail_1
    
def setTemplatePath_fail_2():
    templatePath_fail_2 = dal.config.getTemplatePath_fail_2()
    return templatePath_fail_2
    
def setTemplatePath_fail_3():
    templatePath_fail_3 = dal.config.getTemplatePath_fail_3()
    return templatePath_fail_3
    
def setTemplatePath_fail_4():
    templatePath_fail_4 = dal.config.getTemplatePath_fail_4()
    return templatePath_fail_4
    
def setTemplatePath_fail_5():
    templatePath_fail_5 = dal.config.getTemplatePath_fail_5()
    return templatePath_fail_5



def setConfigPath():
    configPath = dal.config.getConfigPath()
    return configPath
    
def setExcelPath_retrieve():
    excelPath = dal.config.getExcelPath_retrieve()
    return excelPath
    
def setExcelPath_add():
    excelPath = dal.config.getExcelPath_add()
    return excelPath
    
def setExcelPath_execute():
    excelPath = dal.config.getExcelPath_execute()
    return excelPath
    
def setExcelPath_update():
    excelPath = dal.config.getExcelPath_update()
    return excelPath
    
    

def setTSname(name):
    dal.config.createTCSet(name)


def openWB(path):
    wb = dal.excel.openExcileFile(path)
    return wb

def openWS(path):
    ws = dal.excel.getSheet(path)
    return ws