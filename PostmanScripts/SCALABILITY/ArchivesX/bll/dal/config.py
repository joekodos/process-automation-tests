import ConfigParser
import os

'''-------------------ALL-------------------'''
def getDirPath():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "directoryPath")
    return path

def createTCSet(name):
    os.makedirs(name)

'''-------------------CREATE-------------------'''
def get_add_a_test_case_01():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_add", "add_a_test_case_01")
    return path

def get_add_a_test_case_02():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_add", "add_a_test_case_02")
    return path

def get_add_a_test_case_03():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_add", "add_a_test_case_03")
    return path


'''-------------------EXECUTE-------------------'''
def getTemplatePath_pass_1():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_execute", "templatepath_pass_1")
    return path
    
def getTemplatePath_pass_2():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_execute", "templatepath_pass_2")
    return path
    
def getTemplatePath_pass_3():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_execute", "templatepath_pass_3")
    return path
    
def getTemplatePath_fail_1():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_execute", "templatepath_fail_1")
    return path

def getTemplatePath_fail_2():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_execute", "templatepath_fail_2")
    return path
    
def getTemplatePath_fail_3():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_execute", "templatepath_fail_3")
    return path
    
def getTemplatePath_fail_4():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_execute", "templatepath_fail_4")
    return path
    
def getTemplatePath_fail_5():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_execute", "templatepath_fail_5")
    return path


'''-------------------UPDATE-------------------'''
def getTemplatePath_update_01():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_update", "templatePath_update_01")
    return path

def getTemplatePath_update_02():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_update", "templatePath_update_02")
    return path

def getTemplatePath_update_03():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_update", "templatePath_update_03")
    return path

'''-------------------RETRIEVE-------------------'''

def getConfigPath():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_retrieve", "configPath_retrieve")
    return path


'''-------------------EXCEL-------------------'''


def getExcelPath_add():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_add", "excelpath_add")
    return path

def getExcelPath_execute():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_execute", "excelpath_execute")
    return path

def getExcelPath_update():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_update", "excelpath_update")
    return path

def getExcelPath_retrieve():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths_retrieve", "excelPath_retrieve")
    return path