import requests
import simplejson as json

''' ----------- Private API ----------- '''

def publish_flow(url, headers):
    # Send the request with parameters and store it in response
    response = requests.post(url, headers=headers)
    return response