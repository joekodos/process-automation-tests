import requests
import simplejson as json

''' ----------- Public API ----------- '''

def get_oauth2_access_token(url, body, headers):
    # Send the request with parameters and store it in response
    #response = requests.post(url, data=json.dumps(body), headers=headers)
    response = requests.post(url, data=body, headers=headers)
    return response

def launch_flow(url, body, headers):
    # Send the request with parameters and store it in response
    response = requests.post(url, data=json.dumps(body), headers=headers)
    return response