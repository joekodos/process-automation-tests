function touch {set-content -Path ($args[0]) -Value ($null)}
function GetLineAt([String] $path, [Int32] $index)
{
    [System.IO.FileMode] $mode = [System.IO.FileMode]::Open;
    [System.IO.FileAccess] $access = [System.IO.FileAccess]::Read;
    [System.IO.FileShare] $share = [System.IO.FileShare]::Read;
    [Int32] $bufferSize = 16 * 1024;
    [System.IO.FileOptions] $options = [System.IO.FileOptions]::SequentialScan;
    [System.Text.Encoding] $defaultEncoding = [System.Text.Encoding]::UTF8;
    # FileStream(String, FileMode, FileAccess, FileShare, Int32, FileOptions) constructor
    # http://msdn.microsoft.com/library/d0y914c5.aspx
    [System.IO.FileStream] $input = New-Object `
        -TypeName 'System.IO.FileStream' `
        -ArgumentList ($path, $mode, $access, $share, $bufferSize, $options);
    # StreamReader(Stream, Encoding, Boolean, Int32) constructor
    # http://msdn.microsoft.com/library/ms143458.aspx
    [System.IO.StreamReader] $reader = New-Object `
        -TypeName 'System.IO.StreamReader' `
        -ArgumentList ($input, $defaultEncoding, $true, $bufferSize);
    [String] $line = $null;
    [Int32] $currentIndex = 0;

    try
    {
        while (($line = $reader.ReadLine()) -ne $null)
        {
            if ($currentIndex++ -eq $index)
            {
                return $line;
            }
        }
    }
    finally
    {
        # Close $reader and $input
        $reader.Close();
    }

    # There are less than ($index + 1) lines in the file
    return $null;
}

$array_KeyFilePath = @()
$array_ValFilePath = @()
$totalVars = jq -r ".values | length" TEST_DEV-CW-JOEK.postman_environment
For($curVar=0; $curVar -lt $totalVars; $curVar++){
        $curVarA = $curVar + 1
        
    #Extract, Set, and Get
        #Key (newlines removed)
        $curKeyFilePath = ("U:\PostmanScripts\POSTMAN_ENVIRONMENT_PARSER\Keys\Key" + "$curVarA" + ".txt")
        $curKey = jq -j ".values[$curVar].key" TEST_DEV-CW-JOEK.postman_environment
        [System.IO.File]::WriteAllText($curKeyFilePath, $curKey)
        $array_KeyFilePath += $curKeyFilePath
        
        #Value (newlines removed)
        $curValFilePath = ("U:\PostmanScripts\POSTMAN_ENVIRONMENT_PARSER\Values\Value" + "$curVarA" + ".txt")
        $curVal = jq -j ".values[$curVar].value" TEST_DEV-CW-JOEK.postman_environment
        [System.IO.File]::WriteAllText($curValFilePath, $curVal)
        $array_ValFilePath += $curValFilePath
        
}