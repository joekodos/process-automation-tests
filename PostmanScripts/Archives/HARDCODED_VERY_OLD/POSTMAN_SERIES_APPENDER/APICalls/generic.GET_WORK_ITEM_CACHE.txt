		{
			"id": "itemIDnum",
			"headers": "Authorization: IPC {{auth-dca}}\nININ-Session: {{sess-id}}\n",
			"url": "{{server_url_dca}}/api/v1/processautomation/workitems/instances/{{workitemInstanceId}}/workitemcache",
			"preRequestScript": "",
			"pathVariables": {},
			"method": "GET",
			"data": [],
			"dataMode": "params",
			"version": 2,
			"tests": "",
			"currentHelper": "normal",
			"helperAttributes": {},
			"time": 1440782332363,
			"name": "GET_WORK_ITEM_CACHE",
			"description": "",
			"collectionId": "00000000-aaaa-0000-0000-0000000000collectionNumber"
		},