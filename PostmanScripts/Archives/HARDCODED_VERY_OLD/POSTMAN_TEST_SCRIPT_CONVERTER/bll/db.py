import platform
import dal

def checkPathConfig ():
    # Get os type
    os_type = platform.system()
    if os_type == "Windows":
        dal.config.checkWindowsConfig()
    else:
        dal.config.checkMacConfig()

def setApiPoolDir():
    apiPoolDir = dal.config.getApiPoolDir()
    return apiPoolDir


def setTestSeriesDir():
    testSeriesDir = dal.config.getTestSeriesDir()
    return testSeriesDir

def setTestSeriesFile():
    testSeriesFile = dal.config.getTestSeriesFile()
    return testSeriesFile


def setHeaderADir():
    headerADir = dal.config.getHeaderADir()
    return headerADir
    
def setHeaderBDir():
    headerBDir = dal.config.getHeaderBDir()
    return headerBDir
    
def setFooterADir():
    footerADir = dal.config.getFooterADir()
    return footerADir