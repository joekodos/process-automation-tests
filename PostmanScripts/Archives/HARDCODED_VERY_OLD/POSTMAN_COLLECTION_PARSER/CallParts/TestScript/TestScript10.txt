if (responseCode.code >= 200 && responseCode.code <= 299) {

        tests["NULL Publish Flow"] = responseCode.code === 200;

        var data = JSON.parse(responseBody);

        postman.setEnvironmentVariable("flow-version", data.version);

}

else {

        if (responseCode.code >= 400 && responseCode.code <= 499) {

                tests["Response Code is Incorrect Range!"] = false;

        }

        if (responseCode.code >= 500 && responseCode.code <= 599) {

                tests["Response Code is Incorrect Range!"] = false;

        }

        if (responseCode.code >= 600 || responseCode.code < 200 ) {

                tests["Response Code is Incorrect Range!"] = false;

        }

}
