--SAT3210/Lab 10
--Name:  Joseph Kodos
--File: dependent.ctl
--LogFile: dependent.log

LOAD DATA
INFILE 'export_clear.txt'
INTO TABLE dependent
fields terminated by "|"
        ( dep_emp_ssn,
        dep_name ,
        dep_gender,
        dep_date_of_birth,
        dep_relationship)
