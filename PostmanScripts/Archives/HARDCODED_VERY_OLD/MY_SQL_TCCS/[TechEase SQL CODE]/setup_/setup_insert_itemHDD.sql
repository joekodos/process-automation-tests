USE item;
\! echo Inserting itemHDD data...
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         3, 
         'IDE',
         'Fujitsu', 
         4 );

         
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         4, 
         'IDE',
         'Seagate', 
         4 );
   

INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         6, 
         'IDE',
         'Seagate', 
         4 );
         
               
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         6, 
         'IDE',
         'Maxtor', 
         4 );
         
               
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         8, 
         'IDE',
         'Maxtor', 
         7 );
         
               
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         8, 
         'IDE',
         'IBM', 
         7 );
         
               
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 2,
         9, 
         'IDE',
         'Western Digital', 
         7 );
         
               
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         10, 
         'IDE',
         'Western Digital', 
         4 );
           
               
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         10, 
         'IDE',
         'Maxtor', 
         7 );
         
               
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         10, 
         'IDE',
         'Trigem', 
         7 );
         
               
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         37, 
         'IDE',
         'Seagate', 
         13 ); 
         
               
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 2,
         40, 
         'IDE',
         'Western Digital', 
         13 );            
               
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         40, 
         'IDE',
         'Maxtor', 
         13 );   
         
               
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         80, 
         'IDE',
         'Maxtor', 
         19 );  
         
               
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         80, 
         'IDE',
         'Western Digital', 
         19 );    
         
               
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         100, 
         'IDE',
         'Maxtor', 
         24 );    
         
               
INSERT INTO itemHDD ( itemHDD_quantity, itemHDD_size, itemHDD_interface, itemHDD_brand, itemHDD_cost ) 
VALUES ( 1,
         120, 
         'IDE',
         'Western Digital', 
         28 );