/*
To create a text file containing a table's data, you can use
SELECT * INTO OUTFILE 'file_name' FROM tbl_name;
*/
/* 
Another way to create text data files (along with files containing CREATE TABLE statements for the backed
up tables) is to use mysqldump with the --tab option
*/
/*
To reload a delimited-text data file, use LOAD DATA INFILE or mysqlimport.
*/

/* 
USING MYSQLDUMP IN CENTOS6.2/MYSQL5.1: 
Do this once a week - purges old logs:
        [root@lamp1 user]#mysqldump -p --single-transaction --flush-logs --master-data=2 --all-databases --delete-master-logs > backup_sunday_3_PM.sql
*/

/*  Create incremental backup, flush logs to begin new binary log file: 
Do this once a day:
                [root@lamp1 user]#mysqladmin -p flush-logs
  
/*      **IF REPLICATION SERVER BEGIN **::

Deleting the MySQL binary logs with mysqldump --delete-master-logs can
be dangerous if your server is a replication master server, because slave servers
might not yet fully have processed the contents of the binary log. The description
for the PURGE BINARY LOGS statement explains what should be verified before
deleting the MySQL binary logs. See PURGE BINARY LOGS Syntax.
http://dev.mysql.com/doc/refman/5.0/en/purge-binary-logs.html
*/



/* **          RESTORING   EXAMPLE      ** 
Server crashes Wednesday...
Full backup from Sunday...
        [root@lamp1 user]#mysql -p < backup_sunday_3_PM.sql
Incremental backups from monday (.000007) , then tuesday (.000008)...
        [root@lamp1 user]#mysqlbinlog -p gbichot2-bin.000007 gbichot2-bin.000008 | mysql -p
 
/* 
That is, we can start the server with
a --log-bin option that specifies a location on a different physical device from the one on which the data
directory resides. That way, the logs are safe even if the device containing the directory is lost.) If we had
done this, we would have the gbichot2-bin.000009 file (from Tuesday to Wednesday)

*/
/* 
Always run the MySQL server with the --log-bin option, or even --log-bin=log_name, where
the log file name is located on some safe media different from the drive on which the data directory is
located. If you have such safe media, this technique can also be good for disk load balancing (which
results in a performance improvement).
*/
/* END *******RESTORING EXAMPLE ****** END */