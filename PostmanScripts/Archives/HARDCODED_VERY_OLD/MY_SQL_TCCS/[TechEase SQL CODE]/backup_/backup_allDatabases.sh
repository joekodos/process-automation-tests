#!/bin/sh
clear
echo " ...... "
echo "Starting backup_allDatabases.sh..."
echo "This creates a file: /bak/mysql/bak-mysql-alldb-full-%DATE%.sql"
echo "This destroys the master binary logs"
echo " ...... "
mysqldump -p'|cff00|r' --single-transaction --flush-logs --master-data=2 --all-databases --delete-master-logs > /bak/mysql/bak-mysql-alldb-full-$(date +%Y%m%d).sql
echo " Backup complete!"
