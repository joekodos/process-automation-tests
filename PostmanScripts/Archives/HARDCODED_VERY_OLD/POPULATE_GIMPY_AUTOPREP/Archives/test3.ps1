function GetLineAt([String] $path, [Int32] $index)
{
    [System.IO.FileMode] $mode = [System.IO.FileMode]::Open;
    [System.IO.FileAccess] $access = [System.IO.FileAccess]::Read;
    [System.IO.FileShare] $share = [System.IO.FileShare]::Read;
    [Int32] $bufferSize = 16 * 1024;
    [System.IO.FileOptions] $options = [System.IO.FileOptions]::SequentialScan;
    [System.Text.Encoding] $defaultEncoding = [System.Text.Encoding]::UTF8;
    # FileStream(String, FileMode, FileAccess, FileShare, Int32, FileOptions) constructor
    # http://msdn.microsoft.com/library/d0y914c5.aspx
    [System.IO.FileStream] $input = New-Object `
        -TypeName 'System.IO.FileStream' `
        -ArgumentList ($path, $mode, $access, $share, $bufferSize, $options);
    # StreamReader(Stream, Encoding, Boolean, Int32) constructor
    # http://msdn.microsoft.com/library/ms143458.aspx
    [System.IO.StreamReader] $reader = New-Object `
        -TypeName 'System.IO.StreamReader' `
        -ArgumentList ($input, $defaultEncoding, $true, $bufferSize);
    [String] $line = $null;
    [Int32] $currentIndex = 0;

    try
    {
        while (($line = $reader.ReadLine()) -ne $null)
        {
            if ($currentIndex++ -eq $index)
            {
                return $line;
            }
        }
    }
    finally
    {
        # Close $reader and $input
        $reader.Close();
    }

    # There are less than ($index + 1) lines in the file
    return $null;
}
$test_case_prepared_set = (Get-Content U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\TC_current_prepared_set.txt)
$test_case_array = @()
for($aa=0; $aa -le $test_case_prepared_set.Length; $aa++){
        $test_case_array  += GetLineAt 'U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\TC_current_prepared_set.txt' $aa;
        #$test_case_array[0] will contain 1st line of file
        
        #Determine Test Case Boundary & Increment TC Counter
        if ( $test_case_array[$aa] -eq "::----------------------------------------------------------------------------------------------------------------------------------------::" ) {
                $currentTestCasePos = $currentTestCasePos + 1;
                
                $test_case_arrayInternal = @()
                $intX = 0
                for($hh=$aa; $hh -le $aa+8; $hh++){
                        $test_case_arrayInternal  += GetLineAt 'U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\TC_current_prepared_set.txt' $hh;
                        $intX = $intX + 1
                        #$test_case_arrayInternal[0] will contain 1st line of file
                        #Get Current Test Case Number
                        if ( $test_case_arrayInternal[$intX] -like 'TC#*' ) {
                                $curLine = $test_case_arrayInternal[$hh].Split("=");
                                $currentTestCaseNum = $curLine[1];
                        }
                        
                        #Get Current Test Case Version
                        if ( $test_case_arrayInternal[$intX] -like 'Version*' ) {
                                $curLine = $test_case_arrayInternal[$hh].Split("=");
                                $currentTestCaseVer = $curLine[1];
                        }
                        if ( $test_case_arrayInternal[$intX] -like '*|*' ) {
                                #Get Current Test Case Blob
                                Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\preparedX.txt -Value $test_case_arrayInternal[$intX]
                        }
                } #END NESTED FOR
                
                U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\POPULATE_GIMPY_TEMPLATE.bat
                Start-Sleep -s 1
                $gimpyReady = (Get-Content U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\Insert_Gimpy_TC_ready.txt) | Out-String
                $gimpyReady | Out-File ($currentTestCaseNum + "v" + $currentTestCaseVer.txt)
                #Write-Host $currentTestCaseNum
                #Write-Host $currentTestCaseVer
        } #END IF
} #END FOR