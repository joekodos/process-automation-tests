$test_case_prepared_set = (Get-Content U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\TC_current_prepared_set.txt)
$test_case_array = @()
for($aa=0; $aa -le $test_case_prepared_set.Length; $aa++){
        $test_case_array  += GetLineAt 'U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\TC_current_prepared_set.txt' $aa;
        #$test_case_array[0] will contain 1st line of file
        
        #Determine Test Case Boundary & Increment TC Counter
        if ( $test_case_array[$aa] == "::----------------------------------------------------------------------------------------------------------------------------------------::" ) {
                $currentTestCasePos = $currentTestCasePos + 1;
                
                $test_case_arrayInternal = @()
                for($hh=$aa; $hh -le $aa+8; $hh++){
                        $test_case_arrayInternal  += GetLineAt 'U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\TC_current_prepared_set.txt' $hh;
                        #$test_case_arrayInternal[0] will contain 1st line of file
                           
                        #Get Current Test Case Number
                        if ( $test_case_arrayInternal[0] -like 'TC#*' ) {
                                $curLine = $test_case_arrayInternal[$hh].Split("=");
                                $currentTestCaseNum = $curLine[1];
                        }
                        
                        #Get Current Test Case Version
                        if ( $test_case_arrayInternal[1] -like 'Version*' ) {
                                $curLine = $test_case_arrayInternal[$hh].Split("=");
                                $currentTestCaseVer = $curLine[1];
                        }
                           
                        #Get Current Test Case Blob
                        Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\preparedX.txt -Value $test_case_arrayInternal[$hh]
                } #END NESTED FOR
                
                U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\POPULATE_GIMPY_TEMPLATE.bat
                Start-Sleep -s 1
                $gimpyReady = (Get-Content U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\Insert_Gimpy_TC_ready.txt) | Out-String
                $gimpyReady | Out-File ($currentTestCaseNum + "v" + $currentTestCaseVer.txt)
        } #END IF
} #END FOR