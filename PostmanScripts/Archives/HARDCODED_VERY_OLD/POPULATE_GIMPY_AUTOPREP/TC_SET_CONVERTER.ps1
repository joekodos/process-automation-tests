function touch {set-content -Path ($args[0]) -Value ($null)}
function GetLineAt([String] $path, [Int32] $index)
{
    [System.IO.FileMode] $mode = [System.IO.FileMode]::Open;
    [System.IO.FileAccess] $access = [System.IO.FileAccess]::Read;
    [System.IO.FileShare] $share = [System.IO.FileShare]::Read;
    [Int32] $bufferSize = 16 * 1024;
    [System.IO.FileOptions] $options = [System.IO.FileOptions]::SequentialScan;
    [System.Text.Encoding] $defaultEncoding = [System.Text.Encoding]::UTF8;
    # FileStream(String, FileMode, FileAccess, FileShare, Int32, FileOptions) constructor
    # http://msdn.microsoft.com/library/d0y914c5.aspx
    [System.IO.FileStream] $input = New-Object `
        -TypeName 'System.IO.FileStream' `
        -ArgumentList ($path, $mode, $access, $share, $bufferSize, $options);
    # StreamReader(Stream, Encoding, Boolean, Int32) constructor
    # http://msdn.microsoft.com/library/ms143458.aspx
    [System.IO.StreamReader] $reader = New-Object `
        -TypeName 'System.IO.StreamReader' `
        -ArgumentList ($input, $defaultEncoding, $true, $bufferSize);
    [String] $line = $null;
    [Int32] $currentIndex = 0;

    try
    {
        while (($line = $reader.ReadLine()) -ne $null)
        {
            if ($currentIndex++ -eq $index)
            {
                return $line;
            }
        }
    }
    finally
    {
        # Close $reader and $input
        $reader.Close();
    }

    # There are less than ($index + 1) lines in the file
    return $null;
}
$setName = Read-Host 'Enter set name:'
$test_case_prepared_set = (Get-Content U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\TC_current_prepared_set.txt) | Out-String
$parts = $test_case_prepared_set -split "::----------------------------------------------------------------------------------------------------------------------------------------::"
#$test_case_arrayInternal = @()
$title = @()
$description = @()
$priority = @()
$assumptions = @()
$step00action = @()
$step00result = @()
$tcNumberList = @()
for($i=0; $i -le $parts.Count; $i++){
        touch prepared.txt
        $test_case_arrayInternal = $parts[$i] -split "`r`n"
        for($jj=0; $jj -le $test_case_arrayInternal.Count; $jj++){
                #Get Current Test Case Number
                if ( $test_case_arrayInternal[$jj] -like 'TC#*' ) {
                        $curLine = $test_case_arrayInternal[$jj].Split("=");
                        $currentTestCaseNum = $curLine[1];
                        $tcNumberList += $curLine[1];
                }
                
                #Get Current Test Case Version
                if ( $test_case_arrayInternal[$jj] -like 'Version*' ) {
                        $curLine = $test_case_arrayInternal[$jj].Split("=");
                        $currentTestCaseVer = $curLine[1];
                }
                
                #Get Current Test Case Contents
                if ( $test_case_arrayInternal[$jj] -like '*|*' ) {
                        #Get Current Test Case Blob
                        $curField = $test_case_arrayInternal[$jj] -split "`r`n"
                        
                        $curFieldEntry = $curField[0] -split "="
                        $curFieldValue = $curField[0] -split "\|"

                        if ( $curFieldEntry[0] -eq "Title" ) {
                                $title += $curFieldValue[1]
                        }
                        
                        if ( $curFieldEntry[0] -eq "Description" ) {
                                $description += $curFieldValue[1]
                        }

                        if ( $curFieldEntry[0] -eq "Priority" ) {
                                $priority += $curFieldValue[1]
                        }
                        if ( $curFieldEntry[0] -eq "Assumptions" ) {
                                $assumptions += $curFieldValue[1]
                        }

                        if ( $curFieldEntry[0] -eq "Step00Action" ) {
                                $step00action += $curFieldValue[1]
                        }
                        if ( $curFieldEntry[0] -eq "Step00Result" ) {
                                $step00result += $curFieldValue[1]
                        }
                }
                
                
        }
        
}
$header1 = ":###########################################################################################################################################################################################"
$header2 = "put each one of these in a file"
$header3 = "parse it?"
$header4 = ":BEGIN   |       $setName"
$header5 = ":step00action"
$header6 = ":step00result"
$header7 = ":tc_title"
$header8 = ":tc_description"
$header9 = ":tc_priority"
$header10 = ":tc_assumption"
$header11 = ":tc_note"
$header12 = ":tc_limitation"
$header13 = ":tc_data"
$header14 = ":fs_number"
$header15 = ":tc_comment"
$header16 = ":tc_number"
$header17 = ":END   |       $setName"
$header18 = ":###########################################################################################################################################################################################"

New-Item U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -type file

Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header1
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header2
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header3
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header4
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header5
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $step00action
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header6
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $step00result
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header7
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $title
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header8
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $description
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header9
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $priority
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header10
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $assumptions
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header11
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header12
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header13
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header14
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header15
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header16
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $tcNumberList
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header17
Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\$setName.txt -Value $header18