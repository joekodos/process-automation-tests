@ECHO OFF
FOR /F "delims=| tokens=2" %%a in ('findstr "Title" U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\prepared.txt') do set title=%%a
FOR /F "delims=| tokens=2" %%a in ('findstr "Description" U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\prepared.txt') do set desc=%%a
FOR /F "delims=| tokens=2" %%a in ('findstr "Priority" U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\prepared.txt') do set pri=%%a
FOR /F "delims=| tokens=2" %%a in ('findstr "Assumptions" U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\prepared.txt') do set assume=%%a
FOR /F "delims=| tokens=2" %%a in ('findstr "Step00Action" U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\prepared.txt') do set step00action=%%a
FOR /F "delims=| tokens=2" %%a in ('findstr "Step00Result" U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\prepared.txt') do set step00result=%%a
C:\GnuWin32\bin\sed.exe -e "s/tcName/%title%/" -e "s/tcDesc/%desc%/" -e "s/tcPri/%pri%/" -e "s/tcAssume/%assume%/" -e "s/tcStep00action/%step00action%/" -e "s/tcStep00result/%step00result%/" Insert_Gimpy_TC_template.txt >> Insert_Gimpy_TC_ready.txt