<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="API - Get List of Unowned Work Items and Sort Ascending by Creation Date.Time" priority="P2" state="Inactive">
        <description>Public API test to verify the correct behavior upon an attempt of retrieving a list of unowned Work Items and sorting ascending by creation Date.Time</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>The user has permission to view a List of Work Items.
 More than one unowned Work Item exists.</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Call an endpoint to get a list of unowned Work Items and sort ascending by creation Date.Time" expectedresult="A HTTP response is returned with a body that contains a list of unowned Work Items and is sorted ascending by creation Date.Time                       "/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>