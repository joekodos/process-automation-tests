<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="API - Get List of NOT Suspended Owned Work Items and Sort Ascending by Work Item ID" priority="P1" state="Inactive">
        <description>Public API test to verify the correct behavior upon retrieving a list of owned Work Items that are NOT suspended and sorting ascending by Work Item ID</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>The user has permission to view a list of Work Items.
 More than one NOT suspended Work Item is owned by a user.</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Call an endpoint to get a list of owned Work Items that are NOT suspended and sort ascending by Work Item ID" expectedresult="A HTTP response is returned with a body that contains all owned Work Items that are NOT suspended and is sorted ascending by Work Item ID"/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>