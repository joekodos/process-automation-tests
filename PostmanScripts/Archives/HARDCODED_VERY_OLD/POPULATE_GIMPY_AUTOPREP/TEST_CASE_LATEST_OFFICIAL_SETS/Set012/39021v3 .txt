<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="ClientUI - Work Item Edit Mode - Drop List Collection - Boolean " priority="P3" state="Inactive">
        <description>Verify the correct items displayed in a drop list bound to an boolean collection</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>A Work Item exists in the queue.
 The current user owns the Work Item.
 A drop list bound to an boolean collection exists on the Work Item</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Verify a drop list bound to an boolean collection contains only the items specified in a collection" expectedresult="The drop list contains only the items that exist in the boolean collection variable the drop list is bound to"/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>