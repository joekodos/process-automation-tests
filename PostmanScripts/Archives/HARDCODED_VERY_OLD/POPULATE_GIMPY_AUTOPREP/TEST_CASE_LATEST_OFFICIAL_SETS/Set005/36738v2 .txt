<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="API - Get Subset of Running Flows and Sort Ascending by flow launch Date.Time" priority="P1" state="Inactive">
        <description>Public API test to verify the correct behavior upon an attempt of retrieving a subset of running flows and sorting ascending by flow launch Date.Time</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>The user has permission to view a flow.
 More than five running flows exist.</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Call an endpoint to get all running flows limit 5 per page, sort by flow launch Date.Time ascending " expectedresult="A HTTP response is returned with a body that contains all running flows, 5 per page, sorted ascending by Date.Time"/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>