<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="API - Get Subset of Completed Flows and Sort Ascending by Launching User" priority="P3" state="Inactive">
        <description>Public API test to verify the correct behavior upon an attempt of retrieving a subset of completed flows and sorting ascending by user who launched the flow</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>The user has permission to view a flow.
 More than five completed flows exists.
 More than one user who launched a flow exists.</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Call an endpoint to get all completed flows limit 5 per page, sort by user who launched the flow ascending" expectedresult="A HTTP response is returned with a body that contains all completed flows, 5 per page, sorted ascending by the launching user"/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>