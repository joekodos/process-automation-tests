<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="ClientUI - Work Item Read Only Mode - Text Edit Box - String" priority="P1" state="Inactive">
        <description>Verify the correct behavior upon displaying a text edit box bound to a string variable in read only mode</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>A Work Item exists in the queue.
 A text edit box bound to a String variable exists on the Work Item.
 Letters, numbers, symbols, and umlauts have been entered into a text edit box bound to a string variable while a Work Item is in Edit Mode.</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Verify a previously set text edit box bound to a String variable is set correctly in Read Only mode" expectedresult="The previously set data is displayed correctly"/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>