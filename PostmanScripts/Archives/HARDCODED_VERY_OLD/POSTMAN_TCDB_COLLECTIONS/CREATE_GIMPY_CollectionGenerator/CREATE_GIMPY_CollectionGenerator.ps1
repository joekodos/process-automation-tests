$fso = new-object -com scripting.filesystemobject
$folder = $fso.getfolder("U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\CREATE_GIMPY_CollectionGenerator\TC_TO_INSERT")
$idCounter = 0
foreach ($f in $folder.files) {
        $idCounter = $idCounter + 1
        $f.path >> filesvisited.txt
        $c = (Get-Content $f.path) | Out-String
        $d = $c -replace "`r`n","\r\n"
        $e = $d -replace '"','\"'
        $f = $e -replace "`t","\t"
        $g = $f -replace "`r","\r"
        $h = $g -replace "`n","\n"
        $collection1 = (Get-Content "U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\CREATE_GIMPY_CollectionGenerator\GimpyPOSTMAN_TEMPLATE.postman_collection") | Out-String
        $collect1a = $collection1 -replace "`r`n","`n"
        $collect2b = $collect1a -replace "requestVar",$h
        $idStringA = "00000000-0000-0000-0000-0000000000"
        If ( $idCounter -lt 10 ){
        $idStringB = "0" + $idCounter
        }
        If ( $idCounter -ge 10 ){
        $idStringB = $idCounter
        }
        $idStringAll = $idStringA + $idStringB
        $collect3c = $collect2b -replace "curID",$idStringAll
        $collect3c -replace "curName",$idCounter | Out-File collection3.txt
        Copy-Item U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\CREATE_GIMPY_CollectionGenerator\collection3.txt U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\CREATE_GIMPY_CollectionGenerator\TEMP_COLLECT_APPENDER\collection$idCounter.txt
        $appended = (Get-Content "U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\CREATE_GIMPY_CollectionGenerator\TEMP_COLLECT_APPENDER\collection$idCounter.txt") | Out-String
        Add-Content -Path U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\CREATE_GIMPY_CollectionGenerator\appended.txt -Value $appended
}
        C:\Windows\System32\cmd.exe /c TYPE U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\CREATE_GIMPY_CollectionGenerator\appended.txt | findstr /v TEMP_COLLECT_APPENDER > finalAppended.txt
        $finalAppended = (Get-Content "U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\CREATE_GIMPY_CollectionGenerator\finalAppended.txt") | Out-String
        $createFinalCollectionA = (Get-Content "U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\CREATE_GIMPY_CollectionGenerator\GimpyPostmanCollection_HEADER.txt") | Out-String
        $createFinalCollectionB = (Get-Content "U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\CREATE_GIMPY_CollectionGenerator\GimpyPostmanCollection_FOOTER.txt") | Out-String
        Add-Content -Path U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\CREATE_GIMPY_CollectionGenerator\finalCollection.postman_collection -Value $createFinalCollectionA
        Add-Content -Path U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\CREATE_GIMPY_CollectionGenerator\finalCollection.postman_collection -Value $finalAppended
        Add-Content -Path U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\CREATE_GIMPY_CollectionGenerator\finalCollection.postman_collection -Value $createFinalCollectionB