param([string]$setFolderName='Set999')
$fso = new-object -com scripting.filesystemobject
$folder = $fso.getfolder("U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\UPDATE_TC_TO_INSERT")
$idCounter = 0
#$setFolderName = Read-Host "Enter Set Folder (e.g. Set001):"
# For Each POPULATE_GIMPY_TEMPLATE.bat output file in OutputFolder (001.txt, 002.txt...)
foreach ($f in $folder.files) {
        $idCounter = $idCounter + 1
        #Make sure it read all the files
        $f.path >> filesvisited.txt
        #Store text content from current file into variable
        $c = (Get-Content $f.path) | Out-String
        #Change POST call type
        $d1 = $c -replace "CreateInactiveTestCase","UpdateTestCase"
        #Mark Active
        $d2 = $d1 -replace "Inactive","Active"
        #Replace stuff for parsing
        $aaa = $d2 -replace "`r`n","\r\n"
        $bbb = $aaa -replace '"','\"'
        $ccc = $bbb -replace "`t","\t"
        $ddd = $ccc -replace "`r","\r"
        $e = $ddd -replace "`n","\n"
        #Read test case IDs 1 per line from file BatchTestCaseNumbers.txt 
        #       !!! Make sure to add 1 empty line before first number !!!
        $testCaseID = (Get-Content U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\BatchTestCaseNumbers.txt)[$idCounter]
        $f = $e -replace "{{testCaseId}}",$testCaseID
        $collection1 = (Get-Content "U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\GimpyPOSTMAN_UPDATE_TEMPLATE.postman_collection") | Out-String
        $curMain1 = $collection1 -replace "requestVar",$f
        #Update Postman Collection ID
        $idStringA = "00000000-0000-0000-0000-0000000000"
        If ( $idCounter -lt 10 ){
                $idStringB = "0" + $idCounter
        }
        If ( $idCounter -ge 10 ){
                $idStringB = $idCounter
        }
        $idStringAll = $idStringA + $idStringB
        $curID1 = $curMain1 -replace "curID",$idStringAll
        $curID1 -replace "curName",$idCounter | Out-File collection3.txt
        #Copy current piece of collection to different folder
        Copy-Item U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\collection3.txt U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\TEMP_UPDATE_COLLECT_APPENDER\collection$idCounter.txt
        $appended = (Get-Content "U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\TEMP_UPDATE_COLLECT_APPENDER\collection$idCounter.txt") | Out-String
        #Append current piece of collection to big file
        Add-Content -Path U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\appended.txt -Value $appended
}
#Remove not needed lines from file
C:\Windows\System32\cmd.exe /c TYPE U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\appended.txt | findstr /v TEMP_UPDATE_COLLECT_APPENDER > finalAppended.txt
#Set Final Collection Body, Header, Footer variables
$finalAppended = (Get-Content "U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\finalAppended.txt") | Out-String
$createFinalCollectionA = (Get-Content "U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\GimpyPostmanCollection_UPDATE_HEADER.txt") | Out-String
$createFinalCollectionB = (Get-Content "U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\GimpyPostmanCollection_FOOTER.txt") | Out-String
#Piece Header, Body, Footer together
Add-Content -Path U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\finalCollectionUPDATE.postman_collection -Value $createFinalCollectionA
Add-Content -Path U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\finalCollectionUPDATE.postman_collection -Value $finalAppended
Add-Content -Path U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\finalCollectionUPDATE.postman_collection -Value $createFinalCollectionB
#Don't forget to manually remove the last comma (,) from finalCollectionUPDATE.postman_collection

If ( Test-Path U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\Runs\$setFolderName) {
       Remove-Item U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\Runs\$setFolderName\* -force
}
If (-Not (Test-Path U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\Runs\$setFolderName)) {
       New-Item -type directory U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\Runs\$setFolderName
}

Move-Item U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\collection3.txt U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\Runs\$setFolderName
Move-Item U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\appended.txt U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\Runs\$setFolderName
Move-Item U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\BatchTestCaseNumbers.txt U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\Runs\$setFolderName
Move-Item U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\filesvisited.txt U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\Runs\$setFolderName
Move-Item U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\finalAppended.txt U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\Runs\$setFolderName
Move-Item U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\finalCollectionUPDATE.postman_collection U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\Runs\$setFolderName

Remove-Item U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\TEMP_UPDATE_COLLECT_APPENDER\* -force
Remove-Item U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator\UPDATE_TC_TO_INSERT\* -force
        
        
        
        
        