		{
			"id": "itemIDnum",
			"name": "COPY_FLOW",
			"dataMode": "params",
			"data": [],
			"rawModeData": null,
			"descriptionFormat": null,
			"description": "",
			"headers": "Authorization: IPC {{auth-dca}}\nININ-Session: {{sess-id}}\n",
			"method": "GET",
			"pathVariables": {},
			"url": "https://public-api.us-east-1.inindca.com/api/v1/processautomation/flows/definitions/{{flow-id}}",
			"preRequestScript": "",
			"tests": "",
			"version": 2,
			"currentHelper": "normal",
			"helperAttributes": "{}",
			"folder": null,
			"collectionId": "00000000-aaaa-0000-0000-0000000000collectionNumber"
		},