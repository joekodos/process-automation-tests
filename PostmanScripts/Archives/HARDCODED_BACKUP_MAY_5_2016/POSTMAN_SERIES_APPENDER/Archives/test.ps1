#Acquired Function From http://stackoverflow.com/questions/14759649/how-to-print-a-certain-line-of-a-file-with-powershell
function GetLineAt([String] $path, [Int32] $index)
{
    [System.IO.FileMode] $mode = [System.IO.FileMode]::Open;
    [System.IO.FileAccess] $access = [System.IO.FileAccess]::Read;
    [System.IO.FileShare] $share = [System.IO.FileShare]::Read;
    [Int32] $bufferSize = 16 * 1024;
    [System.IO.FileOptions] $options = [System.IO.FileOptions]::SequentialScan;
    [System.Text.Encoding] $defaultEncoding = [System.Text.Encoding]::UTF8;
    # FileStream(String, FileMode, FileAccess, FileShare, Int32, FileOptions) constructor
    # http://msdn.microsoft.com/library/d0y914c5.aspx
    [System.IO.FileStream] $input = New-Object `
        -TypeName 'System.IO.FileStream' `
        -ArgumentList ($path, $mode, $access, $share, $bufferSize, $options);
    # StreamReader(Stream, Encoding, Boolean, Int32) constructor
    # http://msdn.microsoft.com/library/ms143458.aspx
    [System.IO.StreamReader] $reader = New-Object `
        -TypeName 'System.IO.StreamReader' `
        -ArgumentList ($input, $defaultEncoding, $true, $bufferSize);
    [String] $line = $null;
    [Int32] $currentIndex = 0;

    try
    {
        while (($line = $reader.ReadLine()) -ne $null)
        {
            if ($currentIndex++ -eq $index)
            {
                return $line;
            }
        }
    }
    finally
    {
        # Close $reader and $input
        $reader.Close();
    }

    # There are less than ($index + 1) lines in the file
    return $null;
}
$fso = new-object -com scripting.filesystemobject
$folder = $fso.getfolder("U:\PostmanScripts\POSTMAN_SERIES_APPENDER\APICalls")
$idCounter = 0
#BEGIN GET NUMBER OF ENTRIES
$test_series_length = (Get-Content U:\PostmanScripts\POSTMAN_SERIES_APPENDER\test_series.txt)
$test_series_array = @()
for($hh=0; $hh -le $test_series_length.Length; $hh++){
        $test_series_array  += GetLineAt 'U:\PostmanScripts\POSTMAN_SERIES_APPENDER\test_series.txt' $hh;
        #$test_series_array[0] will contain 1st line of file
}
Write-Host $test_series_array[0]
Write-Host $test_series_array[2]
Write-Host $test_series_array[1]
Write-Host $test_series_array[3]








        #$test_series_array = (Get-Content U:\PostmanScripts\POSTMAN_SERIES_APPENDER\test_series.txt)[$hh]
        
        #Write-Host $test_series_array[0]
        #Write-Host $test_series_array[0]
#Write-Host $test_series_length.Length
#$test_series_array  += (Get-Content U:\PostmanScripts\POSTMAN_SERIES_APPENDER\test_series.txt)[$hh]
