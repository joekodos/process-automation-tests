$apiDir = "U:\PostmanScripts\POSTMAN_SERIES_APPENDER\APICalls"
$apiPoolCount = (get-childitem $apiDir).Count
#Acquired Function From http://superuser.com/questions/502374/equivalent-of-linux-touch-to-create-an-empty-file-with-powershell
function touch {set-content -Path ($args[0]) -Value ($null)} 
#Create empty files
touch itemOrderAppended.txt
touch appendedBody.txt
#Acquired Function From http://stackoverflow.com/questions/14759649/how-to-print-a-certain-line-of-a-file-with-powershell
function GetLineAt([String] $path, [Int32] $index)
{
    [System.IO.FileMode] $mode = [System.IO.FileMode]::Open;
    [System.IO.FileAccess] $access = [System.IO.FileAccess]::Read;
    [System.IO.FileShare] $share = [System.IO.FileShare]::Read;
    [Int32] $bufferSize = 16 * 1024;
    [System.IO.FileOptions] $options = [System.IO.FileOptions]::SequentialScan;
    [System.Text.Encoding] $defaultEncoding = [System.Text.Encoding]::UTF8;
    # FileStream(String, FileMode, FileAccess, FileShare, Int32, FileOptions) constructor
    # http://msdn.microsoft.com/library/d0y914c5.aspx
    [System.IO.FileStream] $input = New-Object `
        -TypeName 'System.IO.FileStream' `
        -ArgumentList ($path, $mode, $access, $share, $bufferSize, $options);
    # StreamReader(Stream, Encoding, Boolean, Int32) constructor
    # http://msdn.microsoft.com/library/ms143458.aspx
    [System.IO.StreamReader] $reader = New-Object `
        -TypeName 'System.IO.StreamReader' `
        -ArgumentList ($input, $defaultEncoding, $true, $bufferSize);
    [String] $line = $null;
    [Int32] $currentIndex = 0;

    try
    {
        while (($line = $reader.ReadLine()) -ne $null)
        {
            if ($currentIndex++ -eq $index)
            {
                return $line;
            }
        }
    }
    finally
    {
        # Close $reader and $input
        $reader.Close();
    }

    # There are less than ($index + 1) lines in the file
    return $null;
}
$collectionName = Read-Host 'Enter name of collection:'
$collectionNumber = Read-Host 'Enter number of collection (must be two digits):'
$fso = new-object -com scripting.filesystemobject
$folder = $fso.getfolder("U:\PostmanScripts\POSTMAN_SERIES_APPENDER\APICalls")
$idCounter = 0
#BEGIN GET NUMBER OF ENTRIES
$test_series_length = (Get-Content U:\PostmanScripts\POSTMAN_SERIES_APPENDER\test_series.txt)
$test_series_array = @()
for($hh=0; $hh -le $test_series_length.Length; $hh++){
        $test_series_array  += GetLineAt 'U:\PostmanScripts\POSTMAN_SERIES_APPENDER\test_series.txt' $hh;
        #$test_series_array[0] will contain 1st line of file
}
#Write-Host $test_series_array[0]
#Write-Host $test_series_array[1]
#Write-Host $test_series_array[2]
# END GET NUMBER OF ENTRIES
#BEGIN ASSEMBLE HEADER
$header1 = (Get-Content U:\PostmanScripts\POSTMAN_SERIES_APPENDER\headerA.txt) | Out-String
$header2 = $header1 -replace "collectionName",$collectionName
#create a main file with first part of header
$header2 -replace "collectionNumber",$collectionNumber | Out-File componentAssembler.txt
#BEGIN ASSEMBLE COLLECTION ITEM ID#
$idStringA = "00000000-a"
$idStringC = "a-0000-0000-0000000000"
$idStringD = ","
#$idStringE = "`n"
$idStringF = "                "
$idStringG = '"'
#foreach ($item in $test_series_array) {
#Write-Host $test_series_array[$item]
#foreach ($item in $test_series_array) {
for($aaa=0; $aaa -lt $test_series_array.Length; $aaa++){
        Write-Host $test_series_array[$aaa]
        $idCounter = $idCounter + 1
        If ( $idCounter -lt 10 ){
                $idStringB = "0" + $idCounter
        }
        If ( $idCounter -ge 10 ){
                $idStringB = $idCounter
        }
        #if counter reaches end, don't put a comma there
        If ( $idCounter -eq $test_series_length.Length ){
                #$idStringAll = $idStringF + $idStringG + $idStringA + $collectionNumber + $idStringC + $idStringB + $idStringG + $idStringE
                $idStringAll = $idStringF + $idStringG + $idStringA + $collectionNumber + $idStringC + $idStringB + $idStringG
        }
        #else, put a comma there
        If ( $idCounter -ne $test_series_length.Length ){
                #$idStringAll = $idStringF + $idStringG + $idStringA + $collectionNumber + $idStringC + $idStringB + $idStringG + $idStringD + $idStringE
                $idStringAll = $idStringF + $idStringG + $idStringA + $collectionNumber + $idStringC + $idStringB + $idStringG + $idStringD
        }
        $idCounter = $idCounter - 1
        #idStringB is the sequence number
        #idStringAll = "00000000-a" + 15 + a-0000-0000-0000000000 + 05
        #END ASSEMBLE COLLECTION ITEM ID#
        #If ( $idCounter -le $test_series_array.Length){
        #[System.IO.File]::AppendAllText("U:\PostmanScripts\POSTMAN_SERIES_APPENDER\itemOrderAppended.txt", $idStringAll, [System.Text.Encoding]::Unicode)
        Add-Content -Path U:\PostmanScripts\POSTMAN_SERIES_APPENDER\itemOrderAppended.txt -Value $idStringAll
        #}
        #END ASSEMBLE HEADER
        #BEGIN ASSEMBLE BODY
        # Find matching file (i.e. "generic.publish" in the file --> grab content of generic.publish.txt )
        #foreach ($f in $folder.files) {
        for($bbb=0; $bbb -lt $apiPoolCount; $bbb++) {
                $curFile = (Get-ChildItem $apiDir)[$bbb]
                #Write-Host $curFile
                $currentCallFileName = [System.IO.Path]::GetFileNameWithoutExtension($curFile.name)
                If ($currentCallFileName -eq $test_series_array[$idCounter]){
                        # do stuff here that grabs that matching file and copies its content to a body file
                        $currentCallData = (Get-Content U:\PostmanScripts\POSTMAN_SERIES_APPENDER\APICalls\$currentCallFileName.txt) | Out-String
                        #Write-Host $currentCallData
                        $idStringCurrentItem = $idStringA + $collectionNumber + $idStringC + $idStringB
                        #update current API item ID and collection ID fields
                        $currentUpdatedCallData = $currentCallData -replace "itemIDnum",$idStringCurrentItem
                        $currentFinishedCallData = $currentUpdatedCallData -replace "collectionNumber",$collectionNumber
                        #[System.IO.File]::AppendAllText("$env:temp\file.txt", 'New Word 2', [System.Text.Encoding]::Unicode)
                        [System.IO.File]::AppendAllText("U:\PostmanScripts\POSTMAN_SERIES_APPENDER\appendedBody.txt", $currentFinishedCallData, [System.Text.Encoding]::Unicode)
                        #Add-Content -Path U:\PostmanScripts\POSTMAN_SERIES_APPENDER\appendedBody.txt -Value $currentFinishedCallData
                }
        }
        #END ASSEMBLE BODY
        $idCounter = $idCounter + 1
}
#append item order to main file
$itemOrderingCompleted = (Get-Content U:\PostmanScripts\POSTMAN_SERIES_APPENDER\itemOrderAppended.txt) | Out-String
#Add-Content -Path U:\PostmanScripts\POSTMAN_SERIES_APPENDER\componentAssembler.txt -Value $itemOrderingCompleted
[System.IO.File]::AppendAllText("U:\PostmanScripts\POSTMAN_SERIES_APPENDER\componentAssembler.txt", $itemOrderingCompleted, [System.Text.Encoding]::Unicode)
#append rest of the header (that follows the item order) to main file
$headerClose = (Get-Content U:\PostmanScripts\POSTMAN_SERIES_APPENDER\headerB.txt) | Out-String
#Add-Content -Path U:\PostmanScripts\POSTMAN_SERIES_APPENDER\componentAssembler.txt -Value $headerClose
[System.IO.File]::AppendAllText("U:\PostmanScripts\POSTMAN_SERIES_APPENDER\componentAssembler.txt", $headerClose, [System.Text.Encoding]::Unicode)
#append assembled body to main file
$completeBody = (Get-Content U:\PostmanScripts\POSTMAN_SERIES_APPENDER\appendedBody.txt) | Out-String
#Add-Content -Path U:\PostmanScripts\POSTMAN_SERIES_APPENDER\componentAssembler.txt -Value $completeBody
[System.IO.File]::AppendAllText("U:\PostmanScripts\POSTMAN_SERIES_APPENDER\componentAssembler.txt", $completeBody, [System.Text.Encoding]::Unicode)
#append footer to main file
$footer = (Get-Content U:\PostmanScripts\POSTMAN_SERIES_APPENDER\footerA.txt) | Out-String
#Add-Content -Path U:\PostmanScripts\POSTMAN_SERIES_APPENDER\componentAssembler.txt -Value $footer
[System.IO.File]::AppendAllText("U:\PostmanScripts\POSTMAN_SERIES_APPENDER\componentAssembler.txt", $footer, [System.Text.Encoding]::Unicode)
#
#       DO NOT FORGET TO REMOVE COMMA FROM LAST ENTRY MANUALLY
#       DO NOT FORGET TO REMOVE COMMA FROM LAST ENTRY MANUALLY
#       DO NOT FORGET TO REMOVE COMMA FROM LAST ENTRY MANUALLY
#
remove-item -path U:\PostmanScripts\POSTMAN_SERIES_APPENDER\appendedBody.txt -force
remove-item -path U:\PostmanScripts\POSTMAN_SERIES_APPENDER\itemOrderAppended.txt -force