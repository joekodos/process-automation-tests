		{
			"id": "00000000-0000-0000-0000-000000000056",
			"headers": "Content-Type: text/xml\nSOAPAction: http://gimpy.i3domain.inin.com/TCDBWebService/CreateInactiveTestCase\n",
			"url": "http://gimpy.i3domain.inin.com/TCDBWebService/TCDBWebService.asmx",
			"pathVariables": {},
			"preRequestScript": "",
			"method": "POST",
			"collectionId": "00000000-0000-0000-0000-000000000007",
			"data": [],
			"dataMode": "raw",
			"name": "56",
			"description": "",
			"descriptionFormat": "html",
			"time": 1428691633073,
			"version": 2,
			"responses": [],
			"tests": "var jsonObject = xmlToJson(responseBody);\nvar importResult = JSON.stringify(jsonObject);\nvar match = importResult.match(/testcaseid.+\\:\\\\\\\"(\\d+).+imported/);\npostman.setGlobalVariable( \"testCaseId\", match[1]);\nconsole.log(\"Test Case Number Imported \" + match[1]);",
			"currentHelper": "normal",
			"helperAttributes": {},
			"collectionOwner": 0,
			"write": true,
			"synced": false,
			"owner": "39537",
			"collection": "00000000-0000-0000-0000-000000000007",
			"rawModeData": "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:gim=\"http://gimpy.i3domain.inin.com\">\r\n  <soapenv:Header/>\r\n  <soapenv:Body>\r\n    <gim:CreateInactiveTestCase>\r\n      <!--Optional:-->\r\n      <gim:testCaseXml>       \r\n                <![CDATA[         \r\n    <testcases username=\"{{gimpyUsername}}\">\r\n    <testcase codebase=\"PureCloud\" id=\"{{testCaseId}}\" name=\"ClientUI - Work Item Edit Mode - Button - Close\" priority=\"P1\" state=\"Inactive\">\r\n        <description>Verify the correct behavior upon pressing the Close button on a Work Item</description>\r\n        <estimatedruntime>0.20</estimatedruntime>\r\n        <assumptions>A Work Item exists in the queue.\r\n The current user owns the Work Item.</assumptions>\r\n        <limitations> </limitations>\r\n        <notes> </notes>\r\n        <data> </data>\r\n        <type>Public API Endpoint Only</type>\r\n        <howfound>Tester Input</howfound>\r\n        <targetrelease>PureCloud 1.0GA</targetrelease>\r\n        <pscim>\r\n            <product>PureCloud</product>\r\n        </pscim>\r\n        <teststeps>\r\n            <teststep action=\"Press the Close button and verify the Work Item successfully closes\" expectedresult=\"The Work Item closes successfully\"/>\r\n        </teststeps>\r\n    </testcase>\r\n    </testcases>\r\n\t\t]]>\r\n      </gim:testCaseXml>\r\n    </gim:CreateInactiveTestCase>\r\n  </soapenv:Body>\r\n</soapenv:Envelope>\r\n"
		},

