<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="ClientUI - Work Item Read Only Mode - Label - Display" priority="P1" state="Inactive">
        <description>Verify the correct behavior upon displaying a previously set label in read only mode</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>A Work Item exists in the queue.
 A radio button exists on the Work Item.</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Verify a label set to a string containing alphanumeric, symbol, and umlaut characters displays correctly " expectedresult="The label displays correctly"/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>