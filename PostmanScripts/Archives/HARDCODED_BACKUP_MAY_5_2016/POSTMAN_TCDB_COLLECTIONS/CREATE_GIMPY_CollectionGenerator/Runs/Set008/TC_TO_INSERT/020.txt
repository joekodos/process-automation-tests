<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="Expression Arithmetic - (Decimal), (Negative, Positive)" priority="P1" state="Inactive">
        <description>Verify the correct behavior upon using all binary operators where operands are all Decimal and patterned as (Negative, Positive)</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>A flow exists.</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Execute an expression that uses all binary operators (-5 + 5 - -5 * 5 / -5 ^ 5 % -500) where operands are all Decimal and patterned as (Negative, Positive)" expectedresult="The result is 499.992"/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>