<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="API - Get List of Work Items Belonging To Nonexistent Version of Flow" priority="P1" state="Inactive">
        <description>Verify the correct behavior upon retrieving a list of all Work Items belonging to a specified version that does not exist of a specified flow</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>A flow exists with multiple versions.
 A version of the flow has one or more Work Item(s) associated with it.</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Call an endpoint to get a list of Work Items belonging to a specific version of a flow that doesn't exist" expectedresult="A HTTP response is returned indicating that the specified version of the specified flow does not exist"/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>