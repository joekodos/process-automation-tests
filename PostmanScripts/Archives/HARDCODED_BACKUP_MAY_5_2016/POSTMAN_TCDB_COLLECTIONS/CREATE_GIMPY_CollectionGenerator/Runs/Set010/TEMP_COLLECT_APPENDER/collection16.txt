		{
			"id": "00000000-0000-0000-0000-000000000016",
			"headers": "Content-Type: text/xml\nSOAPAction: http://gimpy.i3domain.inin.com/TCDBWebService/CreateInactiveTestCase\n",
			"url": "http://gimpy.i3domain.inin.com/TCDBWebService/TCDBWebService.asmx",
			"pathVariables": {},
			"preRequestScript": "",
			"method": "POST",
			"collectionId": "00000000-0000-0000-0000-000000000007",
			"data": [],
			"dataMode": "raw",
			"name": "16",
			"description": "",
			"descriptionFormat": "html",
			"time": 1428691633073,
			"version": 2,
			"responses": [],
			"tests": "var jsonObject = xmlToJson(responseBody);\nvar importResult = JSON.stringify(jsonObject);\nvar match = importResult.match(/testcaseid.+\\:\\\\\\\"(\\d+).+imported/);\npostman.setGlobalVariable( \"testCaseId\", match[1]);\nconsole.log(\"Test Case Number Imported \" + match[1]);",
			"currentHelper": "normal",
			"helperAttributes": {},
			"collectionOwner": 0,
			"write": true,
			"synced": false,
			"owner": "39537",
			"collection": "00000000-0000-0000-0000-000000000007",
			"rawModeData": "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:gim=\"http://gimpy.i3domain.inin.com\">\r\n  <soapenv:Header/>\r\n  <soapenv:Body>\r\n    <gim:CreateInactiveTestCase>\r\n      <!--Optional:-->\r\n      <gim:testCaseXml>       \r\n                <![CDATA[         \r\n    <testcases username=\"{{gimpyUsername}}\">\r\n    <testcase codebase=\"PureCloud\" id=\"{{testCaseId}}\" name=\"API - Suspend Unsuspended Work Item With Permission Owned By Another User\" priority=\"P1\" state=\"Inactive\">\r\n        <description>Verify the correct behavior upon suspending a NOT suspended Work Item with permission owned by another user</description>\r\n        <estimatedruntime>0.20</estimatedruntime>\r\n        <assumptions>Permission to suspend a Work Item has been granted.\r\n A NOT suspended Work Item exists.\r\n Another user exists that owns the Work Item.</assumptions>\r\n        <limitations> </limitations>\r\n        <notes> </notes>\r\n        <data> </data>\r\n        <type>Public API Endpoint Only</type>\r\n        <howfound>Tester Input</howfound>\r\n        <targetrelease>PureCloud 1.0GA</targetrelease>\r\n        <pscim>\r\n            <product>PureCloud</product>\r\n        </pscim>\r\n        <teststeps>\r\n            <teststep action=\"Call an endpoint to Suspend a Work Item with permission that is unsuspended and owned by another user   \" expectedresult=\"A HTTP response is returned indicating the operation was a success\"/>\r\n        </teststeps>\r\n    </testcase>\r\n    </testcases>\r\n\t\t]]>\r\n      </gim:testCaseXml>\r\n    </gim:CreateInactiveTestCase>\r\n  </soapenv:Body>\r\n</soapenv:Envelope>\r\n"
		},

