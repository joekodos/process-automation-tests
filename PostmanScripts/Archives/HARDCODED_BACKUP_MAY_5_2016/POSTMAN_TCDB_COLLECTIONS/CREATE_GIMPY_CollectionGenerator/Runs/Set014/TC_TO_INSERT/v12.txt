<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="API - Verify Doc History Item Count" priority="P1" state="Inactive">
        <description>Verify the correct number of document workflow history items are present for a given flow</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>A flow exists that has been launched.</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Call an endpoint to retrieve all document workflow history for a given flow and verify the correct number of history items are present " expectedresult="A HTTP response is returned that contains all history items belonging to the given flow"/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>