import ConfigParser
import os

def checkMacConfig():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()

    config.read("config.ini")
    macPath = config.get("Paths", "directoryPath")

    if macPath == "":
        askDirPath = raw_input("Set a path to store your test case set. (Ex: /Users/alex.hernandez/TestCases/): ")
        print "\n"
        askConfigPath = raw_input("Set a path to your config file. (Ex: /Users/alex.hernandez/config.cfg) ")
        print "\n"
        askExcelPath = raw_input("Set a path to your excel file. (Ex:/Users/alex.hernandez/hi.xlsx) ")
        print "\n"
        file1 = open("config.ini", "w")
        config.set("Paths", "directorypath", askDirPath)
        config.set("Paths", "configPath", askConfigPath)
        config.set("Paths", "excelpath", askExcelPath)
        config.write(file1)
        file1.close()

    config.read("config.ini")

    dirPath = config.get("Paths", "directorypath")
    configPath = config.get("Paths", "configPath")
    excelPath = config.get("Paths", "excelpath")

    print "Your test case sets will be stored in this path: \n" + dirPath
    print "\n"
    print "Your config file location is: \n" + configPath
    print "\n"
    print "Your Excel file location is: \n" + excelPath
    print "\n"

    pathQuestion = raw_input("Is this correct: (Y/N) ").lower()
    while pathQuestion == "n":
        askDirPath = raw_input("Set a path to store your test case set. (Ex: /Users/alex.hernandez/TestCases/): ")
        print "\n"
        askConfigPath = raw_input("Set a path to your config file. (Ex: /Users/alex.hernandez/config.cfg) ")
        print "\n"
        askExcelPath = raw_input("Set a path to your excel file. (Ex:/Users/alex.hernandez/hi.xlsx) ")
        print "\n"
        file1 = open("config.ini", "w")
        config.set("Paths", "directorypath", askDirPath)
        config.set("Paths", "configPath", askConfigPath)
        config.set("Paths", "excelpath", askExcelPath)
        config.write(file1)
        file1.close()

        dirPath = config.get("Paths", "directorypath")
        configPath = config.get("Paths", "configPath")
        excelPath = config.get("Paths", "excelpath")

        print "Your test case set will be stored in this path: \n" + dirPath
        print "\n"
        print "Your config file location is: \n" + configPath
        print "\n"
        print "Your Excel file location is: \n" + excelPath
        print "\n"

        pathQuestion = raw_input("Is this correct?: (Y/N) ")
        if not pathQuestion == "n":
            break
        return

def checkWindowsConfig():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()

    config.read("config.ini")
    windowsPath = config.get("Paths", "directoryPath")

    if windowsPath == "":
        askDirPath = raw_input("Set a path to store your test case set. (Ex: C:\TCDB_PULL_DATA\): ")
        print "\n"
        askConfigPath = raw_input("Set a path to your config file. (C:\TCDB_PULL_DATA\config) ")
        print "\n"
        askExcelPath = raw_input("Set a path to your excel file. (C:\TCDB_PULL_DATA\excelfile.xlsx) ")
        print "\n"
        file1 = open("config.ini", "w")
        config.set("Paths", "directorypath", askDirPath)
        config.set("Paths", "configpath", askConfigPath)
        config.set("Paths", "excelpath", askExcelPath)
        config.write(file1)
        file1.close()

    config.read("config.ini")

    dirPath = config.get("Paths", "directorypath")
    configPath = config.get("Paths", "configPath")
    excelPath = config.get("Paths", "excelpath")

    print "Your test cases will be stored in this path: \n\n" + dirPath
    print "Your config file is: \n\n" + configPath
    print "Your Excel file location is: \n\n" + excelPath



    pathQuestion = raw_input("Is this correct: (Y/N) \n").lower()
    while pathQuestion == "n":
        askDirPath = raw_input("Set a path to store your test cases. (Ex: C:\GIMPY_EXPORTER\): ")
        print "\n"
        askConfigPath = raw_input("Set a path to your pass template file. (C:\GIMPY_EXPORTER\execute_pass_a_test_case.json) ")
        print "\n"
        askConfigPath2 = raw_input("Set a path to your fail template file. (C:\GIMPY_EXPORTER\execute_fail_a_test_case.json) ")
        print "\n"
        askExcelPath = raw_input("Set a path to your excel file. (C:\GIMPY_EXPORTER\excelfile.xlsx) ")
        print "\n"
        file1 = open("config.ini", "w")
        config.set("Paths", "directorypath", askDirPath)
        config.set("Paths", "configPath", askConfigPath)
        config.set("Paths", "excelpath", askExcelPath)
        config.write(file1)
        file1.close()

        dirPath = config.get("Paths", "directorypath")
        configPath = config.get("Paths", "configPath")
        excelPath = config.get("Paths", "excelpath")

        print "Your test case sets will be stored in this path: \n\n" + dirPath
        print "Your config file location is: \n\n" + configPath
        print "Your Excel file location is: \n\n" + excelPath

        pathQuestion = raw_input("Is this correct?: (Y/N) ")
        if not pathQuestion == "n":
            break
    return

def getDirPath():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "directoryPath")
    return path

def getConfigPath():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "configPath")
    return path

def getExcelPath():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "excelpath")
    return path

def createTCSet(name):
    os.makedirs(name)
