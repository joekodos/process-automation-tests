README - GIMPY EXPORTER V1.0
1| pip install xlrd
2| pip install simplejson
3| Change Directory paths in python script


Example Windows templatePath = {
    templatePath = "U:/PostmanScripts/GIMPY_EXPORTER_NEWEST/GIMPY EXPORTER/add_a_test_case.json"
}

Example Windows dirPath = {
    dirPath = "U:\PostmanScripts\GIMPY_EXPORTER_NEWEST\GIMPY EXPORTER\%s" % TestCaseSet
}

4| Execute gimpyexporter.py


**Important** - 
1) Make sure you test your first round of test cases in the TEST GIMPY DB. I have a feature suite already set up: its feature suite 3136, or you can create one if you want.
2) Use the new excel sheet provided – ExampleTestCases.xlsx
3) Use the new add a test case template – add-a-test-case.json
4) The URLs are already in the script but here it is just in case:

This is the gimpy URL used to login to the database  
http://qf-kentucky/tcdbv2/login  <---- Use this Testing Env URL first before using Prod env
                                                    					 
Prod env url: http://tcdb.inin.com/tcdbv2/login



This is the gimpy URL used to add a test case to the DB 
http://qf-kentucky.qfun.com/tcdbv2/api/testcases  <--- Use this test env url first then Prod
 
Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases