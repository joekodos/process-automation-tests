#!/usr/bin/python


# Written by Alex Hernandez
# 09/10/2015
""" This script will read an excel file and will automatically
    upload the test cases to GIMPY and update the feature suite.
"""
# Import all necessary modules.
import xlrd
from collections import OrderedDict
import simplejson as json
import shutil
import os
import requests
from openpyxl import load_workbook

# Title
print "\n"
print "                  |---- GIMPY EXPORTER v1.1 ----|"
print "\n"
print "\n"

"""Login to GIMPY"""

print "*** Login into GIMPY ***"
print "\n"

# This is the gimpy URL used to login to the database       *****IMPORTANT*****
gimpyUrl = "http://qf-kentucky/tcdbv2/login"         # <---- Use this Testing Env URL first before using Prod env
                                                    # Prod env url: "http://tcdb.inin.com/tcdbv2/login"

# username variable will store the input of the user
username = str(raw_input(" > USERNAME: "))


# Method body:
loginPayload = {
        "UserName": username,
        "Password": ""

}

# Method Headers:
loginHeaders = {'content-type': 'application/json'}


# Send the request with parameters and store it in login_response
login_response = requests.post(gimpyUrl, data=json.dumps(loginPayload), headers=loginHeaders)

# Use the .json function() to get the data in json format and then we store it in login_response_json variable
login_response_json = login_response.json()


# Store the token
token = login_response_json["Token"]

print "\n"
print "\n"

print "***Logged into GIMPY: SUCCESS***"



# Ask the user a name for the folder that will be created to hold the test cases.
TestCaseSet = ""

print "\n"

# This is the path of the directory that will be created that will hold the test cases
while True:
    TestCaseSet = raw_input(" > Please enter the name for a new directory that doesn't already exist: ")
    dirPath = "U:\PostmanScripts\GIMPY_EXPORTER_NEWEST\GIMPY EXPORTER\%s" % TestCaseSet     # <--- Edit this path
    if not os.path.exists(dirPath):
        break

os.makedirs(dirPath)

print "\n"

# Declare the starting position of the test cases that have been created.
start_row = int(raw_input(" > What row does your first test case start at: ")) - 1
while start_row <= 1:
    start_row = int(raw_input("     > *** Please enter a number higher than 1 or 2: *** ")) - 1

print "\n"
print "\n"

print "   ---------------------------- TEST CASE RESULTS---------------------------------"
print "\n"
# This is the path where the template is located
templatePath = "U:/PostmanScripts/GIMPY_EXPORTER_NEWEST/GIMPY EXPORTER/add_a_test_case.json" # <--- Edit this path

# This is the path where the excel file is located
excelFilePath = "U:\PostmanScripts\GIMPY_EXPORTER_NEWEST\GIMPY EXPORTER\hi1.xlsx" # <--- Edit this path

# Open the workbook and select the first worksheet
wb = xlrd.open_workbook(excelFilePath)
sh = wb.sheet_by_index(0)


# List to hold dictionaries
test_cases_list = []

# List to hold feature suite id's
fs_list = []


""" Iterate through each row in worksheet and fetch values into dict  """

for rownum in range(start_row, sh.nrows):
    test_case = OrderedDict()
    row_values = sh.row_values(rownum)
    test_case['Name'] = row_values[0]
    test_case['Description'] = row_values[1]
    test_case['Assumption'] = row_values[2]
    test_case['Limitation'] = row_values[3]
    test_case['Note'] = row_values[4]
    test_case['EstimatedTimeToRun'] = row_values[5]
    test_case['Data'] = row_values[6]
    test_case['State ID'] = int(row_values[7])
    test_case['Product ID'] = int(row_values[8])
    test_case['Priority ID'] = int(row_values[9])
    test_case['TestType ID'] = int(row_values[10])
    test_case['HowFound ID'] = int(row_values[11])
    test_case['CodeBase ID'] = int(row_values[12])
    test_case['Release ID'] = int(row_values[13])
    test_case['Action'] = row_values[14]
    test_case['Result'] = row_values[15]
    test_case['Comment'] = row_values[16]

 
    # Append the values to the test_cases_list
    test_cases_list.append(test_case)
    fs_list.append(int(row_values[17]))

"""
    Iterate through every item on the list, create a file for it
    and copy the information from the excel information to the template file copied.
"""

test_case_id = []

for i in range(len(test_cases_list)):
    shutil.copyfile(templatePath, dirPath + '/%s%d.json' % (TestCaseSet,i))
    # Opens, loads file we want to be written and stores it in the "data" variable
    jsonFile = open(dirPath + '/%s%d.json' % (TestCaseSet,i), "r")
    data = json.load(jsonFile, object_pairs_hook=OrderedDict)
    jsonFile.close()
        
    # Fetches each entity in JSON file and replaces element with new imported information
    data["Name"] = test_cases_list[i]['Name']
    data['Description'] = test_cases_list[i]['Description']
    data['Assumption'] = test_cases_list[i]['Assumption']
    data['Limitation'] =  test_cases_list[i]['Limitation']
    data['Note'] = test_cases_list[i]['Note']
    data['EstimatedTimeToRun'] = test_cases_list[i]['EstimatedTimeToRun']
    data['Data'] = test_cases_list[i]['Data']
    data['State']['Id'] = test_cases_list[i]['State ID']
    data['Product']['Id'] = test_cases_list[i]['Product ID']
    data['Priority']['Id'] = test_cases_list[i]['Priority ID']
    data['TestType']['Id'] = test_cases_list[i]['TestType ID']
    data['HowFound']['Id'] = test_cases_list[i]['HowFound ID']
    data['CodeBase']['Id'] = test_cases_list[i]['CodeBase ID']
    data['Release']['Id'] = test_cases_list[i]['Release ID']
    data['Steps'][0]['Action'] = test_cases_list[i]['Action']
    data['Steps'][0]['ExpectedResult'] = test_cases_list[i]['Result']
    data['Steps'][0]['Comment'] = test_cases_list[i]['Comment']


    """Create the Test Case"""

    # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
    addTestCaseURL = "http://qf-kentucky.qfun.com/tcdbv2/api/testcases" # <--- Use this test env url first then Prod
                                                            # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases

    # JSON body
    addTestCase_PayLoad = data

    # JSON Headers
    addTestCase_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    addTestCase_response = requests.post(addTestCaseURL, data=json.dumps(addTestCase_PayLoad), headers=addTestCase_headers)


    """Store the addTestCase response"""

    # Use the .json function() to get the data in json format and then we store it in addTestCase_response variable
    addTestCase_response = addTestCase_response.json()


    """Update the Feature suite"""

    fs_id = str(fs_list[i])

    # This is the gimpy URL used to add a test case to the DB
    updateFsURL = "http://qf-kentucky.qfun.com/tcdbv2/api/testsuites/%s/testcases" % fs_id

    # JSON body
    updateFsPayload = [
    {
        "Id": str(addTestCase_response['Id'])
    }
    ]

    # JSON Headers
    updateFsHeaders = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    updateFsResponse = requests.put(updateFsURL, data=json.dumps(updateFsPayload), headers=updateFsHeaders)


    # Print the results of test cases of each test case that has been created
    print "              Test Case"
    print "              ---------"
    print "      " + str(data['Name'])
    print "\n"
    print "   Status Code: " + str(updateFsResponse)
    print "   Test Case ID: " + str(addTestCase_response['Id'])
    # Print the confirmation of feature suite being updated
    print "   Feature Suite " + fs_id + ": Updated successfully with TC" + str(addTestCase_response['Id'])


    # Append the responses to test_case_id list to write to exe file
    test_case_id.append(addTestCase_response['Id'])


    """Writes new information to new file and closes it"""

    jsonFile = open(dirPath + "/%s%d.json" % (TestCaseSet,i), "w+")
    jsonFile.write(json.dumps(data['Name'], indent=4 * ' '))
    jsonFile.write(json.dumps(addTestCase_response, indent=4 * ' '))
    jsonFile.close()

    print "\n"


"""Writes the ID back to the excel file"""
# Load the excel file
wb = load_workbook(filename=excelFilePath)
# Get the sheet
ws = wb.get_sheet_by_name("Sheet1")
row1 = start_row + 1
for item in range(len(test_case_id)):
    # Write the test case id to the excel file
    ws['S'+str(row1)] = test_case_id[item]
    row1 = row1 + 1

# save the excel file
wb.save(excelFilePath)















