from cx_Freeze import setup, Executable

setup(name='GimpyExporterv1.1',
      version='1.1',
      description='Reads Excel File and Exports to GIMPY',
      executables=[Executable("gimpyexporterv1_1.py")])
