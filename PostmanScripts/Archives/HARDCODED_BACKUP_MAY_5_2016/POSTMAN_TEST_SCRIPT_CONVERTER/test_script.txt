var curEntity = -1;
var counterX = -1;
tests[environment.associatedDocuments_assocType + "~ENV associatedDocuments_assocType"]=true;
if (responseCode.code === 200) {
    if (responseBody.length !== 0){
        counterX = counterX + 1;
        var data = JSON.parse(responseBody);
        
        if(data.searchResults.total !== 0){
            //Get current entity
            for(t=0; t < data.searchResults.entities.length; t++){
                var tempT = data.searchResults.entities[t].workItem.flowExecId.id;
                if (tempT === environment.instance_id){
                    curEntity = t;
                }
            }
            if (data.searchResults.entities[curEntity].workItem.id === environment.workitemInstanceId ){
                tests["43145 API - Work Item Instance Search - Filters - Advanced - OR AND NOT OR"] = true;
                postman.setEnvironmentVariable("test_result", "Passed");
                postman.setEnvironmentVariable("test_step00_result", "Passed");
                postman.setEnvironmentVariable("tc_executor", environment.tcdbv2_generic_TC_pass_1step);
            }
            else {
                tests["43145 API - Work Item Instance Search - Filters - Advanced - OR AND NOT OR"] = false;
                postman.setEnvironmentVariable("test_result", "Failed");
                postman.setEnvironmentVariable("test_step00_result", "Failed");
                postman.setEnvironmentVariable("tc_executor", environment.tcdbv2_generic_TC_fail_1step);
                postman.setEnvironmentVariable("tcdbv2_failure_reason_id", 1);
                postman.setEnvironmentVariable("tcdbv2_failure_reason_name", "testFail_Name1");
                postman.setEnvironmentVariable("tcdbv2_failure_reason_comment", "fail_comment_SCR999");
                postman.setEnvironmentVariable("tcdbv2_issueLabel", "CW-9999");
                postman.setEnvironmentVariable("tcdbv2_issueLabel_state_id", 1);
                postman.setEnvironmentVariable("tcdbv2_issueLabel_state_name", "Opened");
            }
        }
    }
}