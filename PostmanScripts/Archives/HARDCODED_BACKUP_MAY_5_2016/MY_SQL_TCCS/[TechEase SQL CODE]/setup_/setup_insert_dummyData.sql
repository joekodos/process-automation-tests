\! echo ..........................................       
\! echo ..........................................        
\! echo Adding dummy data...  
\! echo ..........................................
\! echo Adding dummy customer...
USE store;

INSERT INTO customer ( cust_firstName, cust_lastName, cust_shipAddr, cust_city, cust_zip, cust_country, cust_paypal_name, cust_email ) 
VALUES ( 'cust0firstName', 
         'cust0lastName',
         'cust0shippingAddress',
         'cust0city', 
         'cust0zip',          
         'cust0country',
         'cust0paypalName',
         'cust0email' );
    

INSERT INTO customer ( cust_firstName, cust_lastName, cust_shipAddr, cust_city, cust_zip, cust_country, cust_paypal_name, cust_email ) 
VALUES ( 'cust1firstName', 
         'cust1lastName',
         'cust1shippingAddress',
         'cust1city', 
         'cust1zip',          
         'cust1country',
         'cust1paypalName',
         'cust1email' );


INSERT INTO customer ( cust_firstName, cust_lastName, cust_shipAddr, cust_city, cust_zip, cust_country, cust_paypal_name, cust_email ) 
VALUES ( 'cust2firstName', 
         'cust2lastName',
         'cust2shippingAddress',
         'cust2city', 
         'cust2zip',          
         'cust2country',
         'cust2paypalName',
         'cust2email' );


\! echo Adding dummy orders...

INSERT INTO orders ( order_cust_id, order_item, order_item_count, order_total ) 
VALUES ( 00001,
        'RAM_stick: 512MB, 240-pin, 533 MHz', 
         2,
         8 );
         
INSERT INTO orders ( order_cust_id, order_item, order_item_count, order_total ) 
VALUES ( 00002,
         'RAM_stick: 1024MB, 240-pin, 533 MHz', 
         2,
         16 );
                  

\! echo Adding dummy blog...
USE content;

INSERT INTO blog ( blog_author, blog_title, blog_descript ) 
VALUES ( 'SYSTEM',
        'DUMMY BLOG 0', 
         'Testing a blog 0 entry...' );
         
INSERT INTO blog ( blog_author, blog_title, blog_descript ) 
VALUES ( 'SYSTEM',
        'DUMMY BLOG 1', 
         'Testing a blog 1 entry...' );
         
         
INSERT INTO blog ( blog_author, blog_title, blog_descript ) 
VALUES ( 'SYSTEM',
        'DUMMY BLOG 2', 
         'Testing a blog 2 entry...' );
         
          
\! echo Adding dummy events...

INSERT INTO event ( event_type, event_title, event_loc, event_descript, event_cost ) 
VALUES ( 'Donation',
         'DUMMY EVENT 0 Donations',
         'Nowhere0, USA',        
         'Testing an event 0 Donations entry...',
          0 );         
  
INSERT INTO event ( event_type, event_title, event_loc, event_descript, event_cost ) 
VALUES ( 'Gathering',
         'DUMMY EVENT 1 Gathering / LAN',
         'Nowhere1, USA',        
         'Testing an event 1 Gathering / LAN entry...',
          0 );         

INSERT INTO event ( event_type, event_title, event_loc, event_descript, event_cost ) 
VALUES ( 'Discount',
        'DUMMY EVENT 2 Discount',
        'Nowhere2, USA',
        'Testing an event 2 Discount entry...',
         0 );           
          
          
\! echo Adding dummy logging...

INSERT INTO logging ( log_type, log_category, log_title, log_descript ) 
VALUES ( 'Apache',
         'System',
         'SysHealth',        
         'This log provides system health information...' );         

INSERT INTO logging ( log_type, log_category, log_title, log_descript ) 
VALUES ( 'Apache',
         'Network',
         'TrafficThroughput',        
         'This log provides network traffic information...' );      
            
\! echo Adding dummy calendar...

INSERT INTO calendar ( cal_title, cal_descript, cal_type, cal_category, cal_months, cal_days ) 
VALUES ( 'Default Calendar 2013',
         'Standard Calendar 2013',
         'AllTopics',        
         'Generic',
         'January',
         'Sunday' );                 


INSERT INTO calendar ( cal_title, cal_descript, cal_type, cal_category, cal_months, cal_days ) 
VALUES ( 'Default Calendar 2012',
         'Standard Calendar 2012',
         'AllTopics',        
         'Generic',
         'January',
         'Sunday' );            
         
            
\! echo Adding dummy doc...

INSERT INTO doc ( doc_type, doc_category, doc_title, doc_descript ) 
VALUES ( 'Tutorial',
         'BasicInfo',
         'Generic Help',        
         'Coming Soon! Dummy Tutorial 0' );           

INSERT INTO doc ( doc_type, doc_category, doc_title, doc_descript ) 
VALUES ( 'Tutorial II',
         'BasicInfo II',
         'Generic Help II',        
         'Coming Soon! Dummy Tutorial II' ); 
  
             
\! echo Adding dummy tterm...

 
INSERT INTO tterm ( tterm_type, tterm_category, tterm_title, tterm_descript ) 
VALUES ( 'Basic',
         'Hardware',
         'BasicVocab0',        
         'Coming Soon! Technicial Terms - Hardware' );        
            
INSERT INTO tterm ( tterm_type, tterm_category, tterm_title, tterm_descript ) 
VALUES ( 'Advanced',
         'Hardware',
         'BasicVocab1',        
         'Coming Soon! Technicial Terms - Hardware' ); 
         
INSERT INTO tterm ( tterm_type, tterm_category, tterm_title, tterm_descript ) 
VALUES ( 'Expert',
         'Hardware',
         'BasicVocab2',        
         'Coming Soon! Technicial Terms - Hardware' ); 

INSERT INTO tterm ( tterm_type, tterm_category, tterm_title, tterm_descript ) 
VALUES ( 'Basic',
         'Software',
         'BasicVocab0',        
         'Coming Soon! Technicial Terms - Software' );

INSERT INTO tterm ( tterm_type, tterm_category, tterm_title, tterm_descript ) 
VALUES ( 'Advanced',
         'Networking',
         'BasicVocab0',        
         'Coming Soon! Technicial Terms - Networking' );   


\! echo Adding dummy guests...
 USE users;
 
INSERT INTO guest ( guest_user, guest_pass ) 
VALUES ( 'tempUser000',
         'password' );  

INSERT INTO guest ( guest_user, guest_pass ) 
VALUES ( 'tempUser001',
         'password' );  

INSERT INTO guest ( guest_user, guest_pass ) 
VALUES ( 'tempUser002',
         'password' );  

\! echo Adding dummy members...
 USE users;
 
INSERT INTO member ( mem_user, mem_pass, mem_role, mem_email ) 
VALUES ( 'member000',
         'password',         
         'basic',
         'testmember00@a.com'); 

INSERT INTO member ( mem_user, mem_pass, mem_role, mem_email ) 
VALUES ( 'member001',
         'password',         
         'advanced',
         'testmember01@a.com' );  

INSERT INTO member ( mem_user, mem_pass, mem_role, mem_email  ) 
VALUES ( 'member002',
         'password',
         'expert',
         'testmember02@a.com' );  

         
    /* DONE: news, employee, item, company, itemHDD, itemPCI, 
             itemRAM, customer, orders, blog, event, log,
             calendar, doc, tterm, member, guest 
       TODO:  more coming soon! */










       
         
         