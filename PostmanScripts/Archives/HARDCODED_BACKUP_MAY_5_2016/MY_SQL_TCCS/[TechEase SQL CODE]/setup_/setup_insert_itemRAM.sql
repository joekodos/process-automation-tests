USE item;
\! echo Inserting itemRAM data...

INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         2, 
         256,
         'ADATA',
         'PC2100S', 
         5 );

INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         3, 
         256,
         'Infinion',
         'PC2100S', 
         5 );

INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         3, 
         256,
         'Samsung',
         'PC2100S', 
         5 );


INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         2, 
         256,
         'Random',
         'PC2100S', 
         5 );

INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         4, 
         256,
         'Infinion',
         'PC2700S', 
         7 );

INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         4, 
         256,
         'Promos',
         'PC2700S', 
         7 );         
         
         
INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         2, 
         256,
         'Random',
         'PC2700S', 
         7 );

         
INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         2, 
         1024,
         'Kingston',
         'PC2700S', 
         14 );


INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         4, 
         512,
         'Hynix',
         'PC4200S', 
         8 );

         
INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         2, 
         256,
         'Nanya',
         'PC4200S', 
         10 );
  

INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         11, 
         512,
         'Samsung HP Replacement',
         'PC4200S', 
         8 );
         
         
INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         5, 
         512,
         'Infinion',
         'PC4200S', 
         10 );        
         
         
INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         2, 
         256,
         'Hynix',
         'PC4200S', 
         8 );          
         
         
INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         3, 
         512,
         'Memorex',
         'PC4200S', 
         10 );          
         
         
INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         2, 
         1024,
         'Lenovo',
         'PC5300S', 
         19 );            
         
         
INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         1, 
         1024,
         'Hynix',
         'PC5300S', 
         19 );  

INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         1, 
         1024,
         'Samsung',
         'PC5300S', 
         19 ); 

         
INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         2, 
         512,
         'Hynix',
         'PC5300S', 
         14 ); 


INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR1 - Laptop',
         2, 
         512,
         'Samsung',
         'PC5300S', 
         14 );

         
INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR2 - Laptop',
         1, 
         1024,
         'Kingston',
         'PC2-3200', 
         17 );


INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR2 - Laptop',
         1, 
         512,
         'Samsung',
         'PC2-3200', 
         12 );


INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR2 - Laptop',
         1, 
         512,
         'Nanya',
         'PC2-3200', 
         12 );


INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR2 - Laptop',
         1, 
         512,
         'Kingston',
         'PC2-4200', 
         14 );         


INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR2 - Laptop',
         1, 
         512,
         'Nanya',
         'PC2-4200', 
         14 ); 


INSERT INTO itemRAM ( itemRAM_type, itemRAM_quantity, itemRAM_size, itemRAM_brand, itemRAM_speed, itemRAM_cost ) 
VALUES ( 'DDR2 - Laptop',
         1, 
         512,
         'Unknown',
         'PC2-5200', 
         16 );