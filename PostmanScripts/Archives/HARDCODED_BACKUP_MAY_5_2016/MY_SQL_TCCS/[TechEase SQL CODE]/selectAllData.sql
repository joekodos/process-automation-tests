SELECT * FROM item.itemMain;
SELECT * FROM item.itemPCI;
SELECT * FROM item.itemHDD;
SELECT * FROM item.itemRAM;

SELECT * FROM users.guest;
SELECT * FROM users.member;
SELECT * FROM users.employee;

SELECT * FROM store.customer;
SELECT * FROM store.orders;

SELECT * FROM content.blog;
SELECT * FROM content.calEntry;
SELECT * FROM content.calendar;
SELECT * FROM content.company;
SELECT * FROM content.doc;
SELECT * FROM content.event;
SELECT * FROM content.logAuth;
SELECT * FROM content.logging;
SELECT * FROM content.news;
SELECT * FROM content.service;
SELECT * FROM content.tterm;

SHOW DATABASES;


USE item;
SHOW TABLES;
DESCRIBE itemMain;
DESCRIBE itemRAM;
DESCRIBE itemHDD;
DESCRIBE itemPCI;


USE store;
SHOW TABLES;
DESCRIBE customer;
DESCRIBE orders;



USE users;
SHOW TABLES;
DESCRIBE guest;
DESCRIBE member;
DESCRIBE employee;


USE content;
SHOW TABLES;
DESCRIBE blog;
DESCRIBE calEntry;
DESCRIBE calendar;
DESCRIBE company;
DESCRIBE doc;
DESCRIBE event;
DESCRIBE logAuth;
DESCRIBE logging;
DESCRIBE news;
DESCRIBE service;
DESCRIBE tterm;