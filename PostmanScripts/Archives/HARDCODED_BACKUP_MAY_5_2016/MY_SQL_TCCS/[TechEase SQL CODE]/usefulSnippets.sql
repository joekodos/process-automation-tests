/* If fail because of INNODB, use INDEX on primary key column: */
                         INDEX(order_id) );
                         
/* Show Foreign Key Errors */
SHOW ENGINE INNODB STATUS


/*
Meanwhile, in 4.0.18 you can use

SET FOREIGN_KEY_CHECKS=0;
DROP DATABASE ...
SET FOREIGN_KEY_CHECKS=1;

to drop a database.

*/