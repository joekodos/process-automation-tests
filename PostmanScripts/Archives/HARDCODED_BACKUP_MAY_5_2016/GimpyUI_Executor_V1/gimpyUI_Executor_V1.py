#!/usr/bin/python



# Core Written by Alex Hernandez
# Updater Created by Joseph Kodos
# 10/03/2015
""" This script will read an excel file and will automatically
    update the test cases in GIMPY.
"""
# Import all necessary modules.
import xlrd
from collections import OrderedDict
import simplejson as json
import shutil
from openpyxl import load_workbook
import bll
import sys
# Title
print "\n"
print "                                             |---- GIMPY EXPORTER v1.2 ----|"
print "\n"
print "\n"

"""Login to GIMPY"""

print "   -Login into GIMPY-"

print "\n"

# Stores the username in username variable
username = raw_input(" > What is your username: ")

# Logs user in and stores the token
loginToken = bll.db.login_gimpy(username)

print "\n"

print "  -Login Successful-"

print "\n"

"""Check Paths"""

# Check path config
bll.db.checkPathConfig()

# Set directory path
dirPath = bll.db.setDirPath()

# Set template path
templatePath = bll.db.setTemplatePath()
templatePath2 = bll.db.setTemplatePath2()


# Set excel path
excelPath = bll.db.setExcelPath()

print "\n"

# Set Test Case set

name = raw_input("What do you want to name this set of Test Cases?: ")

# Set Test Case Set name

testCaseSet = dirPath + name
print "\n"
print "Your test cases will be stored here: " + testCaseSet

# Create testCaseSet

bll.db.setTSname(testCaseSet)

print "\n"
print "\n"
print "\n"

# Declare the starting position of the test cases that have been created.
print " /!\ " 
print " The Excel document may be rendered unreadable if this process is killed! "
print "\n"
print "Do NOT CTRL+C or exit this script after this point! "
print " /!\ "
print "\n"
print "\n"
ExcelQuestion = raw_input("*** Please save and close the Excel file *** \n\n"
                          "   Continue?: (Y/N) " ).lower()
if ExcelQuestion == "y":
    print "\n"
    # Declare the starting position of the test cases that have been created.
    start_row = int(raw_input(" > What row does your first test case start at: ")) - 1
    while start_row <= 1:
        start_row = int(raw_input("     > *** Please enter a number higher than 1 or 2: *** ")) - 1
else:
    sys.exit(0)

print "\n"

print "\n"

print "---------------------------- TEST CASE RESULTS---------------------------------"

print "\n"

"""Open Excel File"""

# Open the workbook and select the first worksheet
wb = bll.db.openWB(excelPath)
sh = bll.db.openWS(wb)

# List to hold dictionaries
test_cases_list = []

# List to hold feature suite id's
fs_list = []

# List to hold test Case id's
test_case_id = []


# List to hold test Case Execution Ids
test_case_execution_id = []


""" Iterate through each row in worksheet and fetch values into dict  """
for rownum in range(start_row, sh.nrows):
        test_case = OrderedDict()
        row_values = sh.row_values(rownum)
        if row_values[0] != "":
                test_case['Name'] = row_values[0]
                test_case['Description'] = row_values[1]
                test_case['Assumption'] = row_values[2]
                test_case['Limitation'] = row_values[3]
                test_case['Note'] = row_values[4]
                test_case['EstimatedTimeToRun'] = row_values[5]
                test_case['Data'] = row_values[6]
                test_case['State ID'] = int(row_values[7])
                test_case['Product ID'] = int(row_values[8])
                test_case['Priority ID'] = int(row_values[9])
                test_case['TestType ID'] = int(row_values[10])
                test_case['HowFound ID'] = int(row_values[11])
                test_case['CodeBase ID'] = int(row_values[12])
                test_case['Release ID'] = int(row_values[13])
                test_case['Action'] = row_values[14]
                test_case['Result'] = row_values[15]
                test_case['Comment'] = row_values[16]
                test_case['Test Case #'] = row_values[18]
                test_case['TC Version'] = row_values[19]
                test_case['Exec Result'] = row_values[20]
                test_case['Exec Step Result'] = row_values[21]
                test_case['Exec Step Comment'] = row_values[22]
                test_case['Exec Step Create DateTime'] = row_values[23]
                test_case['Exec Architecture Id'] = row_values[24]
                test_case['Exec Build Id'] = row_values[25]
                test_case['Exec Note'] = row_values[26]
                test_case['Exec Fail Reason Id'] = row_values[27]
                test_case['Exec Fail Reason Name'] = row_values[28]
                test_case['Exec Fail Reason Comment'] = row_values[29]
                test_case['Exec Fail Create DateTime'] = row_values[30]
                test_case['Exec Issue Label'] = row_values[31]
                test_case['Exec Issue Label State Id'] = row_values[32]
                test_case['Exec Issue Label State Name'] = row_values[33]


                # Append the values to the test_cases_list
                test_cases_list.append(test_case)

                # Append the values to the fs_list
                fs_list.append(int(row_values[17]))
        


"""
    Iterate through every item on the list, create a file for it
    and copy the information from the excel information to the template file copied.
"""

for i in range(len(test_cases_list)):

    if (test_cases_list[i]['Exec Result'] == "Passed" and test_cases_list[i]['Exec Step Result'] == "Passed"):
        shutil.copyfile(templatePath, testCaseSet + '/%s%d.json' % (name,i))
    
    if (test_cases_list[i]['Exec Result'] == "Failed" and test_cases_list[i]['Exec Step Result'] == "Failed"):
        shutil.copyfile(templatePath2, testCaseSet + '/%s%d.json' % (name,i))
    
    # Opens, loads file we want to be written and stores it in the "data" variable
    jsonFile = open(testCaseSet + '/%s%d.json' % (name,i), "r")
    data = json.load(jsonFile, object_pairs_hook=OrderedDict)
    jsonFile.close()

    
    if (test_cases_list[i]['Exec Result'] == "Passed" and test_cases_list[i]['Exec Step Result'] == "Passed"):
    # Fetches each entity in JSON file and replaces element with new imported information
        data["Note"] = test_cases_list[i]['Exec Note']
        trim_exec_build_id = str(test_cases_list[i]['Exec Build Id']).split('.')
        data['Build']['Id'] = trim_exec_build_id[0]
        data['Result'] = test_cases_list[i]['Exec Result']
        trim_exec_architecture_id = str(test_cases_list[i]['Exec Architecture Id']).split('.')
        data['Architecture']['Id'] = trim_exec_architecture_id[0]
        data['ExecutionSteps'][0]['Result'] = test_cases_list[i]['Exec Step Result']
        data['ExecutionSteps'][0]['Comment'] = test_cases_list[i]['Exec Step Comment']
        data['ExecutionSteps'][0]['CreatedDateTime'] = test_cases_list[i]['Exec Step Create DateTime']
    
    if (test_cases_list[i]['Exec Result'] == "Failed" and test_cases_list[i]['Exec Step Result'] == "Failed"):
        data["Note"] = test_cases_list[i]['Exec Note']
        trim_exec_build_id = str(test_cases_list[i]['Exec Build Id']).split('.')
        data['Build']['Id'] = trim_exec_build_id[0]
        data['Result'] = test_cases_list[i]['Exec Result']
        trim_exec_architecture_id = str(test_cases_list[i]['Exec Architecture Id']).split('.')
        data['Architecture']['Id'] = trim_exec_architecture_id[0]
        data['ExecutionSteps'][0]['Result'] = test_cases_list[i]['Exec Step Result']
        data['ExecutionSteps'][0]['Comment'] = test_cases_list[i]['Exec Step Comment']
        data['ExecutionSteps'][0]['CreatedDateTime'] = test_cases_list[i]['Exec Step Create DateTime']
        trim_exec_fail_reason_id = str(test_cases_list[i]['Exec Fail Reason Id']).split('.')
        data['FailureReason']['Reason']['Id'] = trim_exec_fail_reason_id[0]
        data['FailureReason']['Reason']['Name'] = test_cases_list[i]['Exec Fail Reason Name']
        data['FailureReason']['Comment'] = test_cases_list[i]['Exec Fail Reason Comment']
        data['FailureReason']['CreatedDateTime'] = test_cases_list[i]['Exec Fail Create DateTime']
        data['FailureReason']['Issues'][0]['IssueLabel'] = test_cases_list[i]['Exec Issue Label']
        trim_exec_issue_label_state_id = str(test_cases_list[i]['Exec Issue Label State Id']).split('.')
        data['FailureReason']['Issues'][0]['State']['Id'] = trim_exec_issue_label_state_id[0]
        data['FailureReason']['Issues'][0]['State']['Name'] = test_cases_list[i]['Exec Issue Label State Name']
    
    
    #Add test case number to TC ID list
    test_case_id.append(test_cases_list[i]['Test Case #'])
    
    test_case_id_str = str(test_case_id[i])
    test_case_id_trimmed = test_case_id_str.split('.')

    """Get the Test Case Version"""
    getTestCase_response = bll.db.get_a_test_case(loginToken, test_case_id_trimmed[0])
    
    
    """Execute the Test Case"""
    # tc_id will hold the value of the id in the test_case_id list
    #tc_id = str(test_case_id[i])
    executeTestCase_response = bll.db.execute_a_test_case(data, loginToken, test_case_id_trimmed[0], getTestCase_response['Version'])
        
        
    # Print the results of test cases of each test case that has been created
    print "Test Case"
    print "---------"
    print "Test Case Name: " +    str(test_cases_list[i]['Name'])
    print "Test Case Execution ID: " + str(executeTestCase_response['Id'])
    print "Test Case Execution Status: " + str(data['Result'])
    print "Executed Successfully!"


    # Append the responses to test_case_execution_id list to write to excel file
    test_case_execution_id.append(executeTestCase_response['Id'])

    """Writes new information to new file and closes it"""

    jsonFile = open(testCaseSet + "/%s%d.json" % (name,i), "w+")
    jsonFile.write(json.dumps(test_cases_list[i]['Name'], indent=4 * ' '))
    jsonFile.write(json.dumps(executeTestCase_response, indent=4 * ' '))
    jsonFile.close()

    print "\n"

    
"""Writes the Test Case Version back to the excel file"""
# Load the excel file
wb = load_workbook(filename=excelPath)
# Get the sheet
ws = wb.get_sheet_by_name("Sheet1")
row1 = start_row + 1


#Iterate through the returned set of test case id's
for item in range(len(test_case_execution_id)):

        #Check if current row is blank
        if str(ws['A'+str(row1)].value) == "None":
                print "Entering blank row!"
                #Skip the row
                row1 = row1 + 3
                # Write the test case id to the excel file
                ws['AI'+str(row1)] = test_case_execution_id[item]
                row1 = row1 + 1
        #Else, current row is not blank
        else:
                # Write the test case id to the excel file
                ws['AI'+str(row1)] = test_case_execution_id[item]
                row1 = row1 + 1

# save the excel file
wb.save(excelPath)

print "\n"

print "---------------------------- UPDATES COMPLETED---------------------------------"

print "\n"















