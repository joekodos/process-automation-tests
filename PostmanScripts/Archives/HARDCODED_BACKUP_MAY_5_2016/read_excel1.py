#!/usr/bin/python


#Written by Alex Hernandez
#09/10/2015
#This script will read an excel file and export them in JSON ready format to upload into gimpy

import xlrd
from collections import OrderedDict
import simplejson as json
import shutil

#TestCaseSuite = raw_input("What do you want to name this set of Test Cases?")

#Copy file of JSON template for new file to written
shutil.copyfile('/Users/alex.hernandez/Google Drive/Interactive Intelligence/python/add_a_test_case.json', '/Users/alex.hernandez/Google Drive/Interactive Intelligence/python/file12.json')  
 
# Open the workbook and select the first worksheet
wb = xlrd.open_workbook('TestCases1.xlsx')
sh = wb.sheet_by_index(0)
 
# List to hold dictionaries
name_data_list = []


#Iterate through each row in worksheet and fetch values into dict
for rownum in range(1, sh.nrows):
    name_data = OrderedDict()
    row_values = sh.row_values(rownum)
    name_data['Name'] = row_values[0]
    name_data['Description'] = row_values[1]
    name_data['Assumption'] = row_values[2]
    name_data['Limitation'] = row_values[3]
    name_data['Note'] = row_values[4]
    name_data['EstimatedTimeToRun'] = row_values[5]
    name_data['Data'] = row_values[6]
    name_data['State ID'] = row_values[7]
    name_data['Product ID'] = row_values[8]
    name_data['Priority ID'] = row_values[9]
    name_data['TestType ID'] = row_values[10]
    name_data['HowFound ID'] = row_values[11]
    name_data['CodeBase ID'] = row_values[12]
    name_data['Release ID'] = row_values[13]
    name_data['Action'] = row_values[14]
    name_data['Result'] = row_values[15]
    name_data['Comment'] = row_values[16]
 
    name_data_list.append(name_data)


#Opens, loads file we want to be written and stores it in the "data" variable
jsonFile = open("file12.json", "r")
data = json.load(jsonFile, object_pairs_hook=OrderedDict)
jsonFile.close()


#Fetches each entity in JSON file and replaces element with new imported information

data["Name"] = name_data['Name']
data['Description'] = name_data['Description']
data['Assumption'] = name_data['Assumption']
data['Limitation'] =  name_data['Limitation']
data['Note'] = name_data['Note']
data['EstimatedTimeToRun'] = name_data['EstimatedTimeToRun']
data['Data'] = name_data['Data']
data['State']['Id'] = name_data['State ID']
data['Product']['Id'] = name_data['Product ID']
data['Priority']['Id'] = name_data['Priority ID']
data['TestType']['Id'] = name_data['TestType ID']
data['HowFound']['Id'] = name_data['HowFound ID']
data['CodeBase']['Id'] = name_data['CodeBase ID']
data['Release']['Id'] = name_data['Release ID']
data['Steps'][0]['Action'] = name_data['Action']
data['Steps'][0]['ExpectedResult'] = name_data['Result']
data['Steps'][0]['Comment'] = name_data['Comment']


#Writes new information to new file and closes it
jsonFile = open("file12.json", "w+")
jsonFile.write(json.dumps(OrderedDict(data), indent=4 * ' '))
jsonFile.close()




















