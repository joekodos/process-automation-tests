var abc = "-j \".requests[1].id\" designerFlowDefFlag.json.postman_collection"
console.log(abc);
var spawn = require('child_process').spawn,
    jq    = spawn('jq', [abc]);

jq.stdout.on('data', function (data) {
  //grep.stdin.write(data);
  console.log(data);
});

jq.stderr.on('data', function (data) {
  console.log('jq stderr: ' + data);
});

jq.on('close', function (code) {
  if (code !== 0) {
    console.log('jq process exited with code ' + code);
  }
  jq.stdin.end();
});

// jq -j ".requests[1].id" designerFlowDefFlag.json.postman_collection