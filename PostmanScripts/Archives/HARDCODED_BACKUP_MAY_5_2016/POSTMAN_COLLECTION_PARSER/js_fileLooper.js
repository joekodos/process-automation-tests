function walk(currentDirPath, callback) {
    var fs = require('fs'), path = require('path');
    fs.readdirSync(currentDirPath).forEach(function(name) {
        var filePath = path.join(currentDirPath, name);
        var stat = fs.statSync(filePath);
        if (stat.isFile()) {
            callback(filePath, stat);
        } else if (stat.isDirectory()) {
            walk(filePath, callback);
        }
    });
}

walk('U:/PostmanScripts/POSTMAN_COLLECTION_PARSER/OfficialCurlCalls', function(filePath, stat) {
    console.log(filePath);
});


// Parser constructor.
var ReadKey = function() {

};
ReadKey.prototype.read = function(dir) {

        var fs=require('fs');

        //var dir='./CallParts/Id/';
        //var dir='U:/PostmanScripts/POSTMAN_ENVIRONMENT_PARSER/Keys/';
        var data={};

        fs.readdir(dir,function(err,files){
            if (err) throw err;
            var c=0;
            files.forEach(function(file){
                c++;
                fs.readFile(dir+file,'utf-8',function(err,html){
                    if (err) throw err;
                    data[file]=html;
                    if (0===--c) {
                    //console.log(data);
                    return data;
                        //console.log(data);  //socket.emit('init', {data: data});
                    }
                }); //end readFile
            }); // end forEach file
            console.log(data);
        }); //end readDir
        //return data;
};
module.exports = ReadKey;