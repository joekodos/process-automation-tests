function touch {set-content -Path ($args[0]) -Value ($null)}
function GetLineAt([String] $path, [Int32] $index)
{
    [System.IO.FileMode] $mode = [System.IO.FileMode]::Open;
    [System.IO.FileAccess] $access = [System.IO.FileAccess]::Read;
    [System.IO.FileShare] $share = [System.IO.FileShare]::Read;
    [Int32] $bufferSize = 16 * 1024;
    [System.IO.FileOptions] $options = [System.IO.FileOptions]::SequentialScan;
    [System.Text.Encoding] $defaultEncoding = [System.Text.Encoding]::UTF8;
    # FileStream(String, FileMode, FileAccess, FileShare, Int32, FileOptions) constructor
    # http://msdn.microsoft.com/library/d0y914c5.aspx
    [System.IO.FileStream] $input = New-Object `
        -TypeName 'System.IO.FileStream' `
        -ArgumentList ($path, $mode, $access, $share, $bufferSize, $options);
    # StreamReader(Stream, Encoding, Boolean, Int32) constructor
    # http://msdn.microsoft.com/library/ms143458.aspx
    [System.IO.StreamReader] $reader = New-Object `
        -TypeName 'System.IO.StreamReader' `
        -ArgumentList ($input, $defaultEncoding, $true, $bufferSize);
    [String] $line = $null;
    [Int32] $currentIndex = 0;

    try
    {
        while (($line = $reader.ReadLine()) -ne $null)
        {
            if ($currentIndex++ -eq $index)
            {
                return $line;
            }
        }
    }
    finally
    {
        # Close $reader and $input
        $reader.Close();
    }

    # There are less than ($index + 1) lines in the file
    return $null;
}
echo "Duplicate the last test case in TC_current_prepared_set.txt"
echo "Make the test case number invalid (e.g. AAA) before proceeding"
echo " if AAA, script will auto-delete the file (searches for AAA*)"
echo " "
echo " 'create' is for when there are no test cases"
echo " 'update' is for when test case numbers already exist"
echo "{}"
$runType = Read-Host 'Type "create" or "update" and Hit [Enter] when Ready!'
$test_case_prepared_set = (Get-Content U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\TC_current_prepared_set.txt) | Out-String
$parts = $test_case_prepared_set -split "::----------------------------------------------------------------------------------------------------------------------------------------::"
#$test_case_arrayInternal = @()
for($i=0; $i -le $parts.Count; $i++){
        touch prepared.txt
        $test_case_arrayInternal = $parts[$i] -split "`r`n"
        for($jj=0; $jj -le $test_case_arrayInternal.Count; $jj++){
                #Get Current Test Case Number
                if ( $test_case_arrayInternal[$jj] -like 'TC#*' ) {
                        $curLine = $test_case_arrayInternal[$jj].Split("=");
                        $currentTestCaseNum = $curLine[1];
                }
                
                #Get Current Test Case Version
                if ( $test_case_arrayInternal[$jj] -like 'Version*' ) {
                        $curLine = $test_case_arrayInternal[$jj].Split("=");
                        $currentTestCaseVer = $curLine[1];
                }
                
                #Get Current Test Case Contents
                if ( $test_case_arrayInternal[$jj] -like '*|*' ) {
                        #Get Current Test Case Blob
                        Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\prepared.txt -Value $test_case_arrayInternal[$jj]
                }
                
                
        }
        U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\POPULATE_GIMPY_TEMPLATE_AUTOPREP.bat
        #Sleep -m 500
        if( $runType -eq "update"){
                Copy-Item U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\Insert_Gimpy_TC_ready.txt -destination ($currentTestCaseNum + "v" + $currentTestCaseVer + ".txt")
        }
        if( $runType -eq "create"){
                Copy-Item U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\Insert_Gimpy_TC_ready.txt -destination ($currentTestCaseNum + "v" + $i + $currentTestCaseVer + ".txt")
        }
        
        Remove-Item -path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\Insert_Gimpy_TC_ready.txt -force
        
        #Copy-Item U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\prepared.txt -destination ($currentTestCaseNum + "EEEEEEE" + $currentTestCaseVer + ".txt")
        
        Remove-Item -path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\prepared.txt -force
}
if ( Test-Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\v.txt ) {
        Remove-Item -path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\v.txt -force
}
if ( Test-Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\AAA*.txt ) {
        Remove-Item -path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\AAA*.txt -force
}