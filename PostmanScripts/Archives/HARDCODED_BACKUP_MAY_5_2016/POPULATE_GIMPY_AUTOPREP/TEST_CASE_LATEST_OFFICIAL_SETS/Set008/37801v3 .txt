<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="Expression Arithmetic - Advanced, ((((00)11)00)1)" priority="P4" state="Inactive">
        <description>Verify the correct behavior upon using all arithmetic operations in pattern (((00)11)00)1 where 0 is a negative value and 1 is a positive value</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>A flow exists.</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Execute an expression ((((-5.15 + -0.005)-58668488*12.1112)/-569^-1) % 3.015) to conduct all operations in pattern (((00)11)00)1" expectedresult="The result is 0.02136230468"/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>