<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="Expression Logical - (False OR True OR False)" priority="P4" state="Inactive">
        <description>Verify the correct behavior upon computing the scenario (False OR True OR False)</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>A flow exists.</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Execute an expression ( ( -5 tcStep00actiongt;= -5 ) OR ( 5 tcStep00actionlt;tcStep00actiongt; 5 ) AND ( -5 tcStep00actiongt;= -5 ) OR ( 5 != 5 ) ) that will be reduced to (False OR True OR False) " expectedresult="The result is true"/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>