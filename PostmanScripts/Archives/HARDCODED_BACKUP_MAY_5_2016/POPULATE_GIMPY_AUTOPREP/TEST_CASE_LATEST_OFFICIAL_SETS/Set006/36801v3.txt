<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="API - Get List of NOT Suspended Work Items Owned by A User and Sort Descending by Work Item ID" priority="P3" state="Inactive">
        <description>Public API test to verify the correct behavior upon retrieving a list of Work Items owned by a user that are NOT suspended and sorting descending by Work Item ID</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>The user has permission to view a list of Work Items.
 More than one NOT suspended Work Item is owned by the specified user.</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Call an endpoint to get a list of NOT suspended Work Items owned by a specified user and sort descending by Work Item ID" expectedresult="A HTTP response is returned with a body that contains all NOT suspended Work Items owned by the specified user and are sorted descending by Work Item ID"/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>