<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="API - Submit without Sending Data NOT Suspended Work Item with Permission Owned By Another User" priority="P3" state="Inactive">
        <description>Verify the correct behavior upon submitting a Work Item without sending data a NOT suspended Work Item with permission that is owned by another user</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>Permission to interact with a Work Item has been revoked.
 A Work Item exists that is owned by another user.
 The Work Item is NOT suspended.</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Call an endpoint to submit a Work Item without sending data with permission that is owned by another user and NOT suspended" expectedresult="A HTTP response is returned indicating the operation failed               "/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>