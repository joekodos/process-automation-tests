How to use:

1| Copy and paste desired [ORGZ]TestCase_Set_XXX into TC_current_prepared_set.txt
2| Prepare TC_current_prepared_set.txt by removing all other lines except for a 
        "
        ::----------------------------------------------------------------------------------------------------------------------------------------::
        "
        before and after each test case.

3| Execute AUTO_PREPARER.PS1
4| Follow on screen instructions and hit Enter
5| Delete all test cases in TEST_CASE_LATEST_OFFICIAL_SETS\<Set###>
6| Copy all test cases manually into TEST_CASE_LATEST_OFFICIAL_SETS\<Set###>

7|      A| if Updating --> use COPY_AND_UPDATE_GIMPY_TCs.BAT
		( loc: U:\PostmanScripts\POSTMAN_TCDB_COLLECTIONS\UPDATE_GIMPY_CollectionGenerator )
        B| if Creating --> Copy test cases and paste into CREATE_GIMPY_CollectionGenerator\TC_TO_INSERT