How to use:

1| Copy and paste desired [ORGZ]TestCase_Set_XXX into TC_current_prepared_set.txt
2| Prepare TC_current_prepared_set.txt by removing all other lines except for a 
        "
        ::----------------------------------------------------------------------------------------------------------------------------------------::
        "
        before and after each test case.

3| Execute TC_SET_CONVERTER.PS1
4| Enter a name for the set and press Enter to execute the script
5| Your new file will be named what was previously specified