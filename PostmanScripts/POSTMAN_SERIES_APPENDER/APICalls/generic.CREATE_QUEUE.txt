		{
			"id": "itemIDnum",
			"headers": "Authorization: IPC {{auth-dca}}\nININ-Session: {{sess-id}}\nContent-Type: application/json\n",
			"url": "{{server_url_dca}}/api/v1/processautomation/queues",
			"preRequestScript": "",
			"pathVariables": {},
			"method": "POST",
			"data": [],
			"dataMode": "raw",
			"version": 2,
			"tests": "if (responseCode.code >= 200 && responseCode.code <= 299) {\n        var data = JSON.parse(responseBody);\n        if (data.id) {\n                postman.setEnvironmentVariable(\"current_queue_id\", data.id);\n                tests[\"Queue Successfully created!\"] = responseCode.code === 200;\n        }\n        else {\n          tests[\"Queue ID not Found\"] = false;\n        }\n}\nelse {\n        tests[responseBody]=false;\n        if (responseCode.code === 400 ) {\n                tests[\"The request could not be understood by the server due to malformed syntax.\"] = false;\n        }\n        else if (responseCode.code === 401) {\n                tests[\"No authorization token (cookie or header) was found.\"] = false;\n        }\n        else if (responseCode.code === 404) {\n                tests[\"The endpoint was not found\"] = false;\n        }\n        else if (responseCode.code === 415 )  {\n                tests[\"Unsupported Media Type - Unsupported or incorrect media type, such as an incorrect Content-Type value in the header.\"] = false;\n        }\n        else if (responseCode.code === 429 ) {\n                tests[\"Too Many Requests\"] = false;\n        }\n        else if (responseCode.code === 500) {\n                tests[\"The server encountered an unexpected condition which prevented it from fulfilling the request.\"] = false;\n        }\n        else if (responseCode.code === 503 )  {\n                tests[\"Service Unavailable - The server is currently unavailable (because it is overloaded or down for maintenance).\"] = false;\n        }\n        else if (responseCode.code === 504) {\n                tests[\"The request timed out.\"] = false;\n        }\n        else {\n                tests[\"Unknown Response Code\"]=false;\n        }\n}",
			"currentHelper": "normal",
			"helperAttributes": {},
			"time": 1450799741083,
			"name": "CREATE_QUEUE",
			"description": "",
			"collectionId": "00000000-aaaa-0000-0000-0000000000collectionNumber",
			"rawModeData": "{\r\n  \"name\": \"TEST_QUEUE_NAME\",\r\n  \"description\": \"TEST_QUEUE_DESCRIPTION\"\r\n}"
		},