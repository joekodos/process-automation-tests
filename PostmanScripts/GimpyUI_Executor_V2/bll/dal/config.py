import ConfigParser
import os

def checkMacConfig():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()

    config.read("config.ini")
    macPath = config.get("Paths", "directoryPath")

    if macPath == "":
        askDirPath = raw_input("Set a path to store your test cases. (Ex: /Users/alex.hernandez/TestCases/): ")
        print "\n"
        asktemplatepath_pass_1 = raw_input("Set a path to your passed template file. (Ex: /Users/alex.hernandez/execute_pass_a_test_case.json) ")
        print "\n"
        asktemplatepath_pass_2 = raw_input("Set a path to your passed template file. (Ex: /Users/alex.hernandez/execute_pass_a_test_case.json) ")
        print "\n"
        asktemplatepath_pass_3 = raw_input("Set a path to your passed template file. (Ex: /Users/alex.hernandez/execute_pass_a_test_case.json) ")
        print "\n"
        
        asktemplatepath_fail_1 = raw_input("Set a path to your failed template file. (Ex: /Users/alex.hernandez/execute_pass_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_2 = raw_input("Set a path to your failed template file. (Ex: /Users/alex.hernandez/execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_3 = raw_input("Set a path to your failed template file. (Ex: /Users/alex.hernandez/execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_4 = raw_input("Set a path to your failed template file. (Ex: /Users/alex.hernandez/execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_5 = raw_input("Set a path to your failed template file. (Ex: /Users/alex.hernandez/execute_fail_a_test_case.json) ")
        print "\n"
        
        askExcelPath = raw_input("Set a path to your excel file. (Ex:/Users/alex.hernandez/hi.xlsx) ")
        print "\n"
        file1 = open("config.ini", "w")
        config.set("Paths", "directorypath", askDirPath)
        config.set("Paths", "templatepath_pass_1", asktemplatepath_pass_1)
        config.set("Paths", "templatepath_pass_2", asktemplatepath_pass_2)
        config.set("Paths", "templatepath_pass_3", asktemplatepath_pass_3)
        config.set("Paths", "templatepath_fail_1", asktemplatepath_fail_1)
        config.set("Paths", "templatepath_fail_2", asktemplatepath_fail_2)
        config.set("Paths", "templatepath_fail_3", asktemplatepath_fail_3)
        config.set("Paths", "templatepath_fail_4", asktemplatepath_fail_4)
        config.set("Paths", "templatepath_fail_5", asktemplatepath_fail_5)
        config.set("Paths", "excelpath", askExcelPath)
        config.write(file1)
        file1.close()

    config.read("config.ini")

    dirPath = config.get("Paths", "directorypath")
    templatepath_pass_1 = config.get("Paths", "templatepath_pass_1")
    templatepath_pass_2 = config.get("Paths", "templatepath_pass_2")
    templatepath_pass_3 = config.get("Paths", "templatepath_pass_3")
    templatepath_fail_1 = config.get("Paths", "templatepath_fail_1")
    templatepath_fail_2 = config.get("Paths", "templatepath_fail_2")
    templatepath_fail_3 = config.get("Paths", "templatepath_fail_3")
    templatepath_fail_4 = config.get("Paths", "templatepath_fail_4")
    templatepath_fail_5 = config.get("Paths", "templatepath_fail_5")
    excelPath = config.get("Paths", "excelpath")

    print "Your test cases will be stored in this path: \n" + dirPath
    print "\n"
    print "Your template file location is: \n" + templatepath_pass_1
    print "Your template file location is: \n" + templatepath_pass_2
    print "Your template file location is: \n" + templatepath_pass_3
    print "Your template file location is: \n" + templatepath_fail_1
    print "Your template file location is: \n" + templatepath_fail_2
    print "Your template file location is: \n" + templatepath_fail_3
    print "Your template file location is: \n" + templatepath_fail_4
    print "Your template file location is: \n" + templatepath_fail_5
    print "\n"
    print "Your Excel file location is: \n" + excelPath
    print "\n"

    pathQuestion = raw_input("Is this correct: (Y/N) ").lower()
    while pathQuestion == "n":
        askDirPath = raw_input("Set a path to store your test cases. (Ex: /Users/alex.hernandez/TestCases/): ")
        print "\n"
        asktemplatepath_pass_1 = raw_input("Set a path to your template file. (Ex: /Users/alex.hernandez/execute_pass_a_test_case.json) ")
        print "\n"
        asktemplatepath_pass_2 = raw_input("Set a path to your template file. (Ex: /Users/alex.hernandez/execute_pass_a_test_case.json) ")
        print "\n"
        asktemplatepath_pass_3 = raw_input("Set a path to your template file. (Ex: /Users/alex.hernandez/execute_pass_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_1 = raw_input("Set a path to your template file. (Ex: /Users/alex.hernandez/execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_2 = raw_input("Set a path to your template file. (Ex: /Users/alex.hernandez/execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_3 = raw_input("Set a path to your template file. (Ex: /Users/alex.hernandez/execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_4 = raw_input("Set a path to your template file. (Ex: /Users/alex.hernandez/execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_5 = raw_input("Set a path to your template file. (Ex: /Users/alex.hernandez/execute_fail_a_test_case.json) ")
        print "\n"
        
        askExcelPath = raw_input("Set a path to your excel file. (Ex:/Users/alex.hernandez/hi.xlsx) ")
        print "\n"
        file1 = open("config.ini", "w")
        config.set("Paths", "directorypath", askDirPath)
        config.set("Paths", "templatepath_pass_1", asktemplatepath_pass_1)
        config.set("Paths", "templatepath_pass_2", asktemplatepath_pass_2)
        config.set("Paths", "templatepath_pass_3", asktemplatepath_pass_3)
        config.set("Paths", "templatepath_fail_1", asktemplatepath_fail_1)
        config.set("Paths", "templatepath_fail_2", asktemplatepath_fail_2)
        config.set("Paths", "templatepath_fail_3", asktemplatepath_fail_3)
        config.set("Paths", "templatepath_fail_4", asktemplatepath_fail_4)
        config.set("Paths", "templatepath_fail_5", asktemplatepath_fail_5)
        config.set("Paths", "excelpath", askExcelPath)
        config.write(file1)
        file1.close()

        dirPath = config.get("Paths", "directorypath")
        templatepath_pass_1 = config.get("Paths", "templatepath_pass_1")
        templatepath_pass_2 = config.get("Paths", "templatepath_pass_2")
        templatepath_pass_3 = config.get("Paths", "templatepath_pass_3")
        templatepath_fail_1 = config.get("Paths", "templatepath_fail_1")
        templatepath_fail_2 = config.get("Paths", "templatepath_fail_2")
        templatepath_fail_3 = config.get("Paths", "templatepath_fail_3")
        templatepath_fail_4 = config.get("Paths", "templatepath_fail_4")
        templatepath_fail_5 = config.get("Paths", "templatepath_fail_5")
        excelPath = config.get("Paths", "excelpath")

        print "Your test cases will be stored in this path: \n" + dirPath
        print "\n"
        print "Your pass template file location is: \n" + templatepath_pass_1
        print "\n"
        print "Your fail template file location is: \n" + templatepath_pass_2
        print "\n"
        print "Your fail template file location is: \n" + templatepath_pass_3
        print "\n"
        print "Your fail template file location is: \n" + templatepath_fail_1
        print "\n"
        print "Your fail template file location is: \n" + templatepath_fail_2
        print "\n"
        print "Your fail template file location is: \n" + templatepath_fail_3
        print "\n"
        print "Your fail template file location is: \n" + templatepath_fail_4
        print "\n"
        print "Your fail template file location is: \n" + templatepath_fail_5
        print "\n"
        
        print "Your Excel file location is: \n" + excelPath
        print "\n"

        pathQuestion = raw_input("Is this correct?: (Y/N) ")
        if not pathQuestion == "n":
            break
        return

def checkWindowsConfig():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()

    config.read("config.ini")
    windowsPath = config.get("Paths", "directoryPath")

    if windowsPath == "":
        askDirPath = raw_input("Set a path to store your test cases. (Ex: C:\GIMPY_EXPORTER\): ")
        print "\n"
        asktemplatepath_pass_1 = raw_input("Set a path to your template file. (C:\GIMPY_EXPORTER\execute_pass_a_test_case.json) ")
        print "\n"
        asktemplatepath_pass_2 = raw_input("Set a path to your template file. (C:\GIMPY_EXPORTER\execute_pass_a_test_case.json) ")
        print "\n"
        asktemplatepath_pass_3 = raw_input("Set a path to your template file. (C:\GIMPY_EXPORTER\execute_pass_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_1 = raw_input("Set a path to your template file. (C:\GIMPY_EXPORTER\execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_2 = raw_input("Set a path to your template file. (C:\GIMPY_EXPORTER\execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_3 = raw_input("Set a path to your template file. (C:\GIMPY_EXPORTER\execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_4 = raw_input("Set a path to your template file. (C:\GIMPY_EXPORTER\execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_5 = raw_input("Set a path to your template file. (C:\GIMPY_EXPORTER\execute_fail_a_test_case.json) ")
        print "\n"
        
        askExcelPath = raw_input("Set a path to your excel file. (C:\GIMPY_EXPORTER\excelfile.xlsx) ")
        print "\n"
        file1 = open("config.ini", "w")
        config.set("Paths", "directorypath", askDirPath)
        config.set("Paths", "templatepath_pass_1", asktemplatepath_pass_1)
        config.set("Paths", "templatepath_pass_2", asktemplatepath_pass_2)
        config.set("Paths", "templatepath_pass_3", asktemplatepath_pass_3)
        config.set("Paths", "templatepath_fail_1", asktemplatepath_fail_1)
        config.set("Paths", "templatepath_fail_2", asktemplatepath_fail_2)
        config.set("Paths", "templatepath_fail_3", asktemplatepath_fail_3)
        config.set("Paths", "templatepath_fail_4", asktemplatepath_fail_4)
        config.set("Paths", "templatepath_fail_5", asktemplatepath_fail_5)
        config.set("Paths", "excelpath", askExcelPath)
        config.write(file1)
        file1.close()

    config.read("config.ini")

    dirPath = config.get("Paths", "directorypath")
    templatepath_pass_1 = config.get("Paths", "templatepath_pass_1")
    templatepath_pass_2 = config.get("Paths", "templatepath_pass_2")
    templatepath_pass_3 = config.get("Paths", "templatepath_pass_3")
    templatepath_fail_1 = config.get("Paths", "templatepath_fail_1")
    templatepath_fail_2 = config.get("Paths", "templatepath_fail_2")
    templatepath_fail_3 = config.get("Paths", "templatepath_fail_3")
    templatepath_fail_4 = config.get("Paths", "templatepath_fail_4")
    templatepath_fail_5 = config.get("Paths", "templatepath_fail_5")
    excelPath = config.get("Paths", "excelpath")

    print "Your test cases will be stored in this path: \n\n" + dirPath
    print "Your passed template file location is: \n\n" + templatepath_pass_1
    print "Your failed template file location is: \n\n" + templatepath_pass_2
    print "Your failed template file location is: \n\n" + templatepath_pass_3
    print "Your failed template file location is: \n\n" + templatepath_fail_1
    print "Your failed template file location is: \n\n" + templatepath_fail_2
    print "Your failed template file location is: \n\n" + templatepath_fail_3
    print "Your failed template file location is: \n\n" + templatepath_fail_4
    print "Your failed template file location is: \n\n" + templatepath_fail_5
    print "Your Excel file location is: \n\n" + excelPath



    pathQuestion = raw_input("Is this correct: (Y/N) \n").lower()
    while pathQuestion == "n":
        askDirPath = raw_input("Set a path to store your test cases. (Ex: C:\GIMPY_EXPORTER\): ")
        print "\n"
        asktemplatepath_pass_1 = raw_input("Set a path to your pass template file. (C:\GIMPY_EXPORTER\execute_pass_a_test_case.json) ")
        print "\n"
        asktemplatepath_pass_2 = raw_input("Set a path to your fail template file. (C:\GIMPY_EXPORTER\execute_pass_a_test_case.json) ")
        print "\n"
        asktemplatepath_pass_3 = raw_input("Set a path to your fail template file. (C:\GIMPY_EXPORTER\execute_pass_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_1 = raw_input("Set a path to your fail template file. (C:\GIMPY_EXPORTER\execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_2 = raw_input("Set a path to your fail template file. (C:\GIMPY_EXPORTER\execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_3 = raw_input("Set a path to your fail template file. (C:\GIMPY_EXPORTER\execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_4 = raw_input("Set a path to your fail template file. (C:\GIMPY_EXPORTER\execute_fail_a_test_case.json) ")
        print "\n"
        asktemplatepath_fail_5 = raw_input("Set a path to your fail template file. (C:\GIMPY_EXPORTER\execute_fail_a_test_case.json) ")
        print "\n"
        
        askExcelPath = raw_input("Set a path to your excel file. (C:\GIMPY_EXPORTER\excelfile.xlsx) ")
        print "\n"
        file1 = open("config.ini", "w")
        config.set("Paths", "directorypath", askDirPath)
        config.set("Paths", "templatepath_pass_1", asktemplatepath_pass_1)
        config.set("Paths", "templatepath_pass_2", asktemplatepath_pass_2)
        config.set("Paths", "templatepath_pass_3", asktemplatepath_pass_3)
        config.set("Paths", "templatepath_fail_1", asktemplatepath_fail_1)
        config.set("Paths", "templatepath_fail_2", asktemplatepath_fail_2)
        config.set("Paths", "templatepath_fail_3", asktemplatepath_fail_3)
        config.set("Paths", "templatepath_fail_4", asktemplatepath_fail_4)
        config.set("Paths", "templatepath_fail_5", asktemplatepath_fail_5)
        config.set("Paths", "excelpath", askExcelPath)
        config.write(file1)
        file1.close()

        dirPath = config.get("Paths", "directorypath")
        templatepath_pass_1 = config.get("Paths", "templatepath_pass_1")
        templatepath_pass_2 = config.get("Paths", "templatepath_pass_2")
        templatepath_pass_3 = config.get("Paths", "templatepath_pass_3")
        templatepath_fail_1 = config.get("Paths", "templatepath_fail_1")
        templatepath_fail_2 = config.get("Paths", "templatepath_fail_2")
        templatepath_fail_3 = config.get("Paths", "templatepath_fail_3")
        templatepath_fail_4 = config.get("Paths", "templatepath_fail_4")
        templatepath_fail_5 = config.get("Paths", "templatepath_fail_5")
        excelPath = config.get("Paths", "excelpath")

        print "Your test cases will be stored in this path: \n\n" + dirPath
        print "Your passed template file location is: \n\n" + templatepath_pass_1
        print "Your failed template file location is: \n\n" + templatepath_pass_2
        print "Your failed template file location is: \n\n" + templatepath_pass_3
        print "Your failed template file location is: \n\n" + templatepath_fail_1
        print "Your failed template file location is: \n\n" + templatepath_fail_2
        print "Your failed template file location is: \n\n" + templatepath_fail_3
        print "Your failed template file location is: \n\n" + templatepath_fail_4
        print "Your failed template file location is: \n\n" + templatepath_fail_5
        print "Your Excel file location is: \n\n" + excelPath

        pathQuestion = raw_input("Is this correct?: (Y/N) ")
        if not pathQuestion == "n":
            break
    return

def getDirPath():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "directoryPath")
    return path

def getTemplatePath_pass_1():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "templatepath_pass_1")
    return path
    
def getTemplatePath_pass_2():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "templatepath_pass_2")
    return path
    
def getTemplatePath_pass_3():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "templatepath_pass_3")
    return path
    
def getTemplatePath_fail_1():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "templatepath_fail_1")
    return path

def getTemplatePath_fail_2():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "templatepath_fail_2")
    return path
    
def getTemplatePath_fail_3():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "templatepath_fail_3")
    return path
    
def getTemplatePath_fail_4():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "templatepath_fail_4")
    return path
    
def getTemplatePath_fail_5():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "templatepath_fail_5")
    return path
    
def getExcelPath():
    # create config file and check if it is null
    config = ConfigParser.ConfigParser()
    config.read("config.ini")
    path = config.get("Paths", "excelpath")
    return path

def createTCSet(name):
    os.makedirs(name)
