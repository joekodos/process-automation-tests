import platform
import dal

def login_gimpy(username):

    """Login to gimpy"""

    # This is the gimpy URL used to login to the database       *****IMPORTANT*****
    gimpyUrl = "http://tcdb.inin.com/tcdbv2/login"         # <---- Use this Testing Env URL first before using Prod env
                                                    # Prod env url: "http://tcdb.inin.com/tcdbv2/login"
                                                    # Test env url: "http://tcdb.inin.com/tcdbv2/login"

    # Method body:
    loginPayload = {
        "UserName": username,
        "Password": ""
    }

    # Method Headers:
    loginHeaders = {'content-type': 'application/json'}

    # Send the request with parameters and store it in login_response
    login_response = dal.tcdb.login_gimpy_call(gimpyUrl, loginPayload, loginHeaders)

    # Use the .json function() to get the data in json format and then we store it in login_response_json variable
    login_response_json = login_response.json()

    # Store the token
    token = login_response_json["Token"]
    return token

def execute_a_test_case(body, token, testCaseNumber, testCaseVersion):

    """Create the Test Case"""

    # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
    executeTestCaseURL = 'http://tcdb.inin.com/tcdbv2/api/testcases/' + str(testCaseNumber) + '/' + str(testCaseVersion) + '/Executions' # <--- Use this test env url first then Prod
                                                            # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases


    # Method body
    executeTestCase_PayLoad = body

    # Method Headers
    executeTestCase_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    executeTestCase_response = dal.tcdb.execute_a_test_case_call(executeTestCaseURL, executeTestCase_PayLoad, executeTestCase_headers)


    """Store the addTestCase response"""

    # Use the .json function() to get the data in json format and then we store it in updateTestCase_response variable
    executeTestCase_response = executeTestCase_response.json()
    return executeTestCase_response

def update_feature_suite(fsid, testcaseid, token):

    """Update Feature Suite"""

    # This is the gimpy URL used to add a test case to the DB
    updateFsURL = "http://tcdb.inin.com/tcdbv2/api/testsuites/"+fsid+"/testcases"

    # Method body
    updateFsPayload = [
    {
        "Id": testcaseid
    }
    ]

    # Method Headers
    updateFsHeaders = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    updateFsResponse = dal.tcdb.update_feature_suite_call(updateFsURL, updateFsPayload, updateFsHeaders)
    return updateFsResponse

def get_a_test_case(token, testCaseNumber):

        """Get a Test Case"""
        # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
        getTestCaseURL = "http://tcdb.inin.com/tcdbv2/api/testcases/" + str(testCaseNumber)   # <--- Use this test env url first then Prod

                                                                                                  # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases
        # Method Headers
        getTestCase_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}
        # Specify request body json data and headers
        getTestCase_response = dal.tcdb.get_a_test_case_call(getTestCaseURL, getTestCase_headers)
        
        """Store the getTestCase response"""
        # Use the .json function() to get the data in json format and then we store it in getTestCase_response variable
        getTestCase_response = getTestCase_response.json()
        return getTestCase_response




def checkPathConfig ():
    # Get os type
    os_type = platform.system()
    if os_type == "Windows":
        dal.config.checkWindowsConfig()
    else:
        dal.config.checkMacConfig()

def setDirPath():
    dirPath = dal.config.getDirPath()
    return dirPath


def setTemplatePath_pass_1():
    templatePath_pass_1 = dal.config.getTemplatePath_pass_1()
    return templatePath_pass_1

def setTemplatePath_pass_2():
    templatePath_pass_2 = dal.config.getTemplatePath_pass_2()
    return templatePath_pass_2
    
def setTemplatePath_pass_3():
    templatePath_pass_3 = dal.config.getTemplatePath_pass_3()
    return templatePath_pass_3
    
def setTemplatePath_fail_1():
    templatePath_fail_1 = dal.config.getTemplatePath_fail_1()
    return templatePath_fail_1
    
def setTemplatePath_fail_2():
    templatePath_fail_2 = dal.config.getTemplatePath_fail_2()
    return templatePath_fail_2
    
def setTemplatePath_fail_3():
    templatePath_fail_3 = dal.config.getTemplatePath_fail_3()
    return templatePath_fail_3
    
def setTemplatePath_fail_4():
    templatePath_fail_4 = dal.config.getTemplatePath_fail_4()
    return templatePath_fail_4
    
def setTemplatePath_fail_5():
    templatePath_fail_5 = dal.config.getTemplatePath_fail_5()
    return templatePath_fail_5
    
    
    


def setExcelPath():
    excelPath = dal.config.getExcelPath()
    return excelPath

def setTSname(name):
    dal.config.createTCSet(name)


def openWB(path):
    wb = dal.excel.openExcileFile(path)
    return wb

def openWS(path):
    ws = dal.excel.getSheet(path)
    return ws