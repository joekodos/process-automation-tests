#!/usr/bin/python
# BLL Designed by Alex Hernandez
# TCDB_PULL_DATA Created by Joseph Kodos
# 02/26/2016
# 03/16/2016 - Version 1.0
        #DESCRIPTION= Pull data from TCDB and insert into Excel
# 05/23/2016 - Version 2.0 
        #DESCRIPTION= Added support for multiple Steps and Tags
""" 
This script will pull test cases from TCDB using the TCDBv2 API and populate an excel file.
"""
# Import all necessary modules.
import xlrd
from collections import OrderedDict
import simplejson as json
import shutil
from openpyxl import load_workbook
import bll
import sys
# Title
print "\n"
print "                                             |---- TCDB PULL DATA v1.0 ----|"
print "\n"
print "\n"

"""Login to GIMPY"""
print "   -Login into GIMPY-"
print "\n"
# Stores the username in username variable
username = raw_input(" > What is your username: ")

# Logs user in and stores the token
loginToken = bll.db.login_gimpy(username)

print "\n"
print "  -Login Successful-"
print "\n"

"""Check Paths"""
# Check path config
bll.db.checkPathConfig()

# Set directory path
dirPath = bll.db.setDirPath()

# Set config path
configPath = bll.db.setConfigPath()

# Set excel path
excelPath = bll.db.setExcelPath()

print "\n"

# Set Test Case set

name = raw_input("What do you want to name this set of test cases ?: ")



# Set Test Case Set name

testCaseSet = dirPath + name
print "\n"
print "Your test cases will be stored here: " + testCaseSet

# Create testCaseSet

bll.db.setTSname(testCaseSet)

print "\n"
print "\n"
print "\n"

# Declare the starting position of the test cases that have been created.
print " /!\ " 
print " The Excel document may be rendered unreadable if this process is killed! "
print "\n"
print "Do NOT CTRL+C or exit this script after this point! "
print " /!\ "
print "\n"
print "\n"
ExcelQuestion = raw_input("*** Please save and close the Excel file *** \n\n"
                          "   Continue?: (Y/N) " ).lower()
if ExcelQuestion == "y":
    print "\n"
    # Declare the starting position of the test cases that have been created.
    start_row = int(raw_input(" > What row does your first test case start at: ")) - 1
    while start_row <= 1:
        start_row = int(raw_input("     > *** Please enter a number higher than 1 or 2: *** ")) - 1
else:
    sys.exit(0)

print "\n"

print "\n"

print "---------------------------- TEST CASE RESULTS---------------------------------"

print "\n"


#List of Lists to hold all test suite TC #'s
testSuite_test_case_list = []

#List to hold current test suite TC #'s
current_test_case_list = []


# List of test suites
testSuiteList = []

""" File Input Method """
#Collect test suite ids from file, one line per test suite id
input_test_suite_id = tuple(open(configPath, 'r'))

#Loop through input tuple and convert to list, then strip newlines
for object in range(len(input_test_suite_id)):
        testSuiteList.append(input_test_suite_id[object])
        testSuiteList[object] = testSuiteList[object].replace('\n','')
        testSuiteList[object] = testSuiteList[object].replace('\r','')

#Go through each test suite
for input_test_suite in range(len(testSuiteList)):
        #Get list of test cases from each test suite
        getTestSuiteResponse = bll.db.get_a_test_suite(loginToken, testSuiteList[input_test_suite])
        #Loop through list of test cases
        for test_case in range(len(getTestSuiteResponse['TestCases'])):
                #Build test suite list of test cases
                current_test_case_list.append(getTestSuiteResponse['TestCases'][test_case]['Id'])
        
        #testSuite_test_case_list[0][1] is the first test case of the first feature suite
        testSuite_test_case_list.append(current_test_case_list)
        #Reset current list
        current_test_case_list = []



#List to hold all TCData
finalOutputTestCaseList = []

#List to hold all testSuiteTCData
finalTestSuiteTestCaseList = []

#MultiStep Position Tracker
stepCounter = []

#List to hold new TS' TCs (Steps)
currentTS_TCCounter = []


#MultiTag Position Tracker
tagCounter = []

#List to hold new TS' TCs (Tags)
currentTag_TCCounter = []

#Loop through test suites
for ts in range(len(testSuite_test_case_list)):
        #Loop through test suite's test cases
        for tc in range(len(testSuite_test_case_list[ts])):
                #List to hold current test suite TC DATA
                currentOutputTestCase = []
                #Call function to get current TC data
                getTestCaseResponse = bll.db.get_a_test_case(loginToken, testSuite_test_case_list[ts][tc])
                currentOutputTestCase.append(getTestCaseResponse['Version'])
                currentOutputTestCase.append(str(getTestCaseResponse['Name']))
                currentOutputTestCase.append(str(getTestCaseResponse['Description']))
                currentOutputTestCase.append(str(getTestCaseResponse['Assumption']))
                currentOutputTestCase.append(str(getTestCaseResponse['Limitation']))
                currentOutputTestCase.append(str(getTestCaseResponse['Note']))
                currentOutputTestCase.append(getTestCaseResponse['EstimatedTimeToRun'])
                currentOutputTestCase.append(str(getTestCaseResponse['Data']))
                currentOutputTestCase.append(getTestCaseResponse['State']['Id'])
                currentOutputTestCase.append(getTestCaseResponse['Product']['Id'])
                currentOutputTestCase.append(getTestCaseResponse['Priority']['Id'])
                currentOutputTestCase.append(getTestCaseResponse['TestType']['Id'])
                currentOutputTestCase.append(getTestCaseResponse['HowFound']['Id'])
                currentOutputTestCase.append(getTestCaseResponse['Release']['Id'])
                currentOutputTestCase.append(getTestCaseResponse['CodeBase']['Id'])
                currentOutputTestCase.append(getTestCaseResponse['Id'])
                currentOutputTestCase.append(getTestCaseResponse['TestSuites'][0]['Id'])
                #Get tc multi step
                for testStep in range(len(getTestCaseResponse['Steps'])):
                        try:
                                currentOutputTestCase.append(str(getTestCaseResponse['Steps'][testStep]['Action']))
                                currentOutputTestCase.append(str(getTestCaseResponse['Steps'][testStep]['ExpectedResult']))
                                currentOutputTestCase.append(str(getTestCaseResponse['Steps'][testStep]['Comment']))
                        except Exception as exc:
                                print "no steps", exc
                        else:
                                if getTestCaseResponse['Steps'][testStep]:
                                        print "-----------------------------------------------------"
                                        print "step exists, current index is:"
                                        print testStep
                                else:
                                        print "no steps found!"
                
                
                getTestCaseTagsResponse = bll.db.get_a_test_case_tags(loginToken, testSuite_test_case_list[ts][tc], getTestCaseResponse['Version'])
                try:
                        for tag in range(len(getTestCaseTagsResponse)):
                                currentOutputTestCase.append(getTestCaseTagsResponse[tag])
                                
                except Exception as exc:
                    print "no Tags", exc
                else:
                    if getTestCaseTagsResponse:
                        print "tags exist"
                    else:
                        print "no tags here!"
                
                
                finalOutputTestCaseList.append(currentOutputTestCase)
                
                currentTS_TCCounter.append(str(len(getTestCaseResponse['Steps'])))
                currentTag_TCCounter.append(str(len(getTestCaseTagsResponse)))
                
        
        finalTestSuiteTestCaseList.append(finalOutputTestCaseList)
        #Reset List to hold all TCData
        finalOutputTestCaseList = []
        
        stepCounter.append(currentTS_TCCounter)
        #Reset List to hold new TS' TCs (Steps)
        currentTS_TCCounter = []
        
        tagCounter.append(currentTag_TCCounter)
        #Reset List to hold new TS' TCs (Tags)
        currentTag_TCCounter = []


#Write Data to File
# Load the excel file
wb = load_workbook(filename=excelPath)
# Get the sheet
ws = wb.get_sheet_by_name("Sheet1")
row1 = start_row + 1


#Loop through test suites
for ts_id in range(len(finalTestSuiteTestCaseList)):
        #For Each test suite id
        for tc_id in range(len(finalTestSuiteTestCaseList[ts_id])):
                #Write data to Excel
                ws['A'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][1]
                ws['B'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][2]
                ws['C'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][3]
                ws['D'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][4]
                ws['E'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][5]
                ws['F'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][6]
                ws['G'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][7]
                ws['H'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][8]
                ws['I'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][9]
                ws['J'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][10]
                ws['K'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][11]
                ws['L'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][12]
                ws['M'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][14]
                ws['N'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][13]
                ws['R'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][16]
                ws['S'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][15]
                ws['T'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][0]
                
                if stepCounter[ts_id][tc_id] > 1:
                        position = 17
                        testCaseStartRow = row1
                        
                        for currentStep in range(0, int(stepCounter[ts_id][tc_id])):
                                ws['O'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][position] # stepAction
                                position = position + 1
                                ws['P'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][position] # stepResult
                                position = position + 1
                                ws['Q'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][position] # stepComment
                                position = position + 1
                                row1 = row1 + 1
                                
                                #if currentStep == int(stepCounter[ts_id][tc_id]):
                                        #row1 = row1 - 1
                                
                        lastTestCaseRow = row1
                        row1 = testCaseStartRow
                        
                else:
                        ws['O'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][17] # stepAction
                        ws['P'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][18] # stepResult
                        ws['Q'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][19] # stepComment
                        
                position = 17 + (3 * int(stepCounter[ts_id][tc_id]))
                #If we have more than 1 tag on this test case...
                if tagCounter[ts_id][tc_id] > 1:
                        for currentTag in range(0, int(tagCounter[ts_id][tc_id])):
                                try:
                                        ws['U'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][position]
                                        position = position + 1
                                        row1 = row1 + 1
                                except Exception as exc:
                                        print "no tags", exc
                        
                        #More tags than steps?
                        if row1 > lastTestCaseRow:
                                lastTestCaseRow = row1
                else:
                        try:
                                ws['U'+str(row1)] = finalTestSuiteTestCaseList[ts_id][tc_id][position]
                        except Exception as exc:
                                print "no tags", exc
                        else:
                                if finalTestSuiteTestCaseList[ts_id][tc_id][position]:
                                        print "tags exist!"
                                else:
                                        print "no tags here!"
                
                #Increment for test case
                row1 = row1 + 1
                #
                row1 = lastTestCaseRow
        
        #Increment for test suite
        row1 = row1 + 3



# save the excel file
wb.save(excelPath)

