import requests
import simplejson as json

def login_gimpy_call(url, body, headers):
    # Send the request with parameters and store it in response
    response = requests.post(url, data=json.dumps(body), headers=headers)
    return response

def update_a_test_case_call(url, body, headers):
    # Send the request with parameters and store it in response
    response = requests.put(url, data=json.dumps(body), headers=headers)
    return response

def update_feature_suite_call(url, body, headers):
    # Send the request with parameters and store it in response
    response = requests.put(url, data=json.dumps(body), headers=headers)
    return response
    
def get_a_test_case_call(url, headers):
    # Send the request with parameters and store it in response
    response = requests.get(url, headers=headers)
    return response
    
def get_a_test_case_tags_call(url, headers):
    # Send the request with parameters and store it in response
    response = requests.get(url, headers=headers)
    return response
    
def execute_a_test_case_call(url, body, headers):
    # Send the request with parameters and store it in response
    response = requests.post(url, data=json.dumps(body), headers=headers)
    return response

def get_a_test_suite_call(url, headers):
    # Send the request with parameters and store it in response
    response = requests.get(url, headers=headers)
    return response