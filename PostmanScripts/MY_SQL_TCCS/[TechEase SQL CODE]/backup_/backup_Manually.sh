#!/bin/sh
clear
echo "................................................"
echo "TechEase mySQL DB Manual Backup/Restore ........"
echo "................Script Invoked! ................"
echo "................Welcome!......................."

echo "Menu"
echo "0 - Quit"
echo "1 - Conduct a FULL BACKUP on ALL databases"
echo "2 - Conduct an incremental backup on ALL databases"
echo "3 - Conduct a FULL RESTORE on ALL databases"
echo "4 - Conduct an incremental restore on ALL databases"
echo "5 - Conduct an auto-incremental restore on ALL databases (all days since last Sunday)"

read -p "Enter a task number:" options
    case $options in
        [1]* ) 
                echo " ...... "
                echo "Starting backup_allDatabases.sh..."
                echo "This creates a file: /bak/mysql/bak-mysql-alldb-full-%DATE%.sql"
                echo "This destroys the master binary logs"
                echo " ...... "
                mysqldump -p'|cff00|r' --single-transaction --flush-logs --master-data=2 --all-databases --delete-master-logs > /bak/mysql/bak-mysql-alldb-full-$(date +%Y%m%d).sql
                echo " Backup complete!";;
                
        [2]* )  
                echo " ...... "
                echo "Starting backup_incre_allDatabases.sh..."
                echo "This creates a file: /bak/mysql/bak-mysql-alldb-incre.000001-6 (1=sun,6=sat)"
                echo "This creates today's incremental backup."
                echo " ...... "
                mysqladmin -p'|cff00|r' flush-logs
                echo " Today's backup complete!";;
        [3]* ) 
                echo " ...... "
                echo "Restore Databases"
                echo "This loads a file: /bak/mysql/bak-mysql-alldb-full-%PROVIDED_DATE%.sql"
                echo " ...... "
                read -p "Please enter the date of backup in YYYYMMDD format (e.g. 20130623): " backupDate
                mysql -p'|cff00|r' < /bak/mysql/bak-mysql-alldb-full-$backupDate.sql
                echo " Restore complete!";;
        [4]* ) 
                echo " ...... "
                echo "Manual Restore: Incremental Databases"
                echo "This restores the database up to the day provided with the file:"
                echo "      /bak/mysql/bak-mysql-alldb-incre.000001-6 (1=sun,6=sat)"
                read -p "Please enter a day (Sun, Mon, Tue, Wed, Thu, Fri, Sat) to restore the backup:" restoreDate
                if [ restoreDate = Sun ]; 
                   then
                        mysqlbinlog -p'|cff00|r' /bak/mysql/bak-mysql-alldb-incre.000001 | mysql -p'|cff00|r'
                        echo "Sunday incremental backup restored!"
                        exit;
                fi
                if [ restoreDate = Mon ]; 
                   then
                        mysqlbinlog -p'|cff00|r' /bak/mysql/bak-mysql-alldb-incre.000002 | mysql -p'|cff00|r'
                        echo "Monday incremental backup restored!"
                        exit;
                fi
                 if [ restoreDate = Tue ]; 
                   then
                        mysqlbinlog -p'|cff00|r' /bak/mysql/bak-mysql-alldb-incre.000003 | mysql -p'|cff00|r'
                        echo "Tuesday incremental backup restored!"
                        exit;
                fi
                if [ restoreDate = Wed ]; 
                   then
                        mysqlbinlog -p'|cff00|r' /bak/mysql/bak-mysql-alldb-incre.000004 | mysql -p'|cff00|r'
                        echo "Wednesday incremental backup restored!"
                        exit;
                fi
                if [ restoreDate = Thu ]; 
                   then
                        mysqlbinlog -p'|cff00|r' /bak/mysql/bak-mysql-alldb-incre.000005 | mysql -p'|cff00|r'
                        echo "Thursday incremental backup restored!"
                        exit;
                fi
                if [ restoreDate = Fri ]; 
                   then
                        mysqlbinlog -p'|cff00|r' /bak/mysql/bak-mysql-alldb-incre.000006 | mysql -p'|cff00|r'
                        echo "Friday incremental backup restored!"
                        exit;
                fi
                if [ restoreDate = Sat ]; 
                   then
                        mysqlbinlog -p'|cff00|r' /bak/mysql/bak-mysql-alldb-incre.000007 | mysql -p'|cff00|r'
                        echo "Saturday incremental backup restored!"
                        exit;
                fi
                echo " Restore of Incremental Databases complete!";;
        
        [5]* ) 
                echo " ...... "
                echo "AUTOMATIC Restore: Incremental Databases"
                echo "This loads as many files as days since the last full backup on Sunday: "
                echo "      /bak/mysql/bak-mysql-alldb-incre.000001-6 (1=sun,6=sat)"
                echo "This is inclusive, meaning the day of the restore is included."
                echo "Unless this script is ran from another server, it will probably not"
                echo "successfully restore that day's data. See google.com:mysql binary log"
                echo " ...... "
                today=`date`
                echo "Today is: $today "
                todayCountDay=$(date +%a)
                if [ todayCountDay = Sun ]; 
                   then
                        mysqlbinlog -p'|cff00|r' /bak/mysql/bak-mysql-alldb-incre.000001 | mysql -p'|cff00|r'
                        echo "Backups since Sunday(including Sunday) restored!"
                fi
                if [ todayCountDay = Mon ]; 
                   then
                        mysqlbinlog -p'|cff00|r' /bak/mysql/bak-mysql-alldb-incre.000001 
                                                 /bak/mysql/bak-mysql-alldb-incre.000002 | mysql -p'|cff00|r'
                        echo "Backups since Monday(including Monday) restored!"
                fi
                if [ todayCountDay = Tue ]; 
                   then
                        mysqlbinlog -p'|cff00|r' /bak/mysql/bak-mysql-alldb-incre.000001 
                                                 /bak/mysql/bak-mysql-alldb-incre.000002 
                                                 /bak/mysql/bak-mysql-alldb-incre.000003 | mysql -p'|cff00|r'
                        echo "Backups since Tuesday (including Tuesday) restored!"
                fi
                if [ todayCountDay = Wed ]; 
                   then
                        mysqlbinlog -p'|cff00|r' /bak/mysql/bak-mysql-alldb-incre.000001 
                                                 /bak/mysql/bak-mysql-alldb-incre.000002 
                                                 /bak/mysql/bak-mysql-alldb-incre.000003 
                                                 /bak/mysql/bak-mysql-alldb-incre.000004 | mysql -p'|cff00|r'
                        echo "Backups since Wednesday (including Wednesday) restored!"
                fi
                if [ todayCountDay = Thu ]; 
                   then
                        mysqlbinlog -p'|cff00|r' /bak/mysql/bak-mysql-alldb-incre.000001 
                                                 /bak/mysql/bak-mysql-alldb-incre.000002 
                                                 /bak/mysql/bak-mysql-alldb-incre.000003 
                                                 /bak/mysql/bak-mysql-alldb-incre.000004 
                                                 /bak/mysql/bak-mysql-alldb-incre.000005 | mysql -p'|cff00|r'
                        echo "Backups since Wednesday (including Thursday) restored!"
                fi
                if [ todayCountDay = Fri ]; 
                   then
                        mysqlbinlog -p'|cff00|r' /bak/mysql/bak-mysql-alldb-incre.000001 
                                                 /bak/mysql/bak-mysql-alldb-incre.000002 
                                                 /bak/mysql/bak-mysql-alldb-incre.000003 
                                                 /bak/mysql/bak-mysql-alldb-incre.000004 
                                                 /bak/mysql/bak-mysql-alldb-incre.000005 
                                                 /bak/mysql/bak-mysql-alldb-incre.000006 | mysql -p'|cff00|r'
                        echo "Backups since Friday (including Friday) restored!"
                fi
                if [ todayCountDay = Sat ]; 
                   then
                        mysqlbinlog -p'|cff00|r' /bak/mysql/bak-mysql-alldb-incre.000001 
                                                 /bak/mysql/bak-mysql-alldb-incre.000002 
                                                 /bak/mysql/bak-mysql-alldb-incre.000003 
                                                 /bak/mysql/bak-mysql-alldb-incre.000004 
                                                 /bak/mysql/bak-mysql-alldb-incre.000005 
                                                 /bak/mysql/bak-mysql-alldb-incre.000006 
                                                 /bak/mysql/bak-mysql-alldb-incre.000007 | mysql -p'|cff00|r'
                        echo "Backups since Saturday (including Saturday) restored!"
                fi                
                echo " Automatic Restore of Incremental Databases complete!";;
                
        [0]* ) 
                echo "Bye! 'backup_Manually.sh' exiting..."
                exit;; 
        * ) echo "Please select a valid option.";;
    esac