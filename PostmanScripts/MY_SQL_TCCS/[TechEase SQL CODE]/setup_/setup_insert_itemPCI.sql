USE item;
\! echo Inserting itemPCI data...

INSERT INTO itemPCI ( itemPCI_quantity, itemPCI_name, itemPCI_interface, itemPCI_type,  itemPCI_model, itemPCI_cost ) 
VALUES ( 2,
         'Sound Blaster Live', 
         'PCI',
         'Sound Card',
         'CT4830', 
         9 );  


INSERT INTO itemPCI ( itemPCI_quantity, itemPCI_name, itemPCI_interface, itemPCI_type,  itemPCI_model, itemPCI_cost ) 
VALUES ( 1,
         'Sound Blaster', 
         'PCI',
         'Sound Card',
         'CT4810', 
         4 );


INSERT INTO itemPCI ( itemPCI_quantity, itemPCI_name, itemPCI_interface, itemPCI_type,  itemPCI_model, itemPCI_cost ) 
VALUES ( 1,
         'Phillips SV22', 
         'AGP',
         'TV Tuner',
         'SV22', 
         19 );


INSERT INTO itemPCI ( itemPCI_quantity, itemPCI_name, itemPCI_interface, itemPCI_type,  itemPCI_model, itemPCI_cost ) 
VALUES ( 1,
         'Generic USB', 
         'PCI',
         'USB Adapter',
         'N/A', 
         2 );


INSERT INTO itemPCI ( itemPCI_quantity, itemPCI_name, itemPCI_interface, itemPCI_type,  itemPCI_model, itemPCI_cost ) 
VALUES ( 1,
         'Sound Blaster Audigy HD', 
         'PCI',
         'Sound Card',
         'SB0090', 
         9 );


INSERT INTO itemPCI ( itemPCI_quantity, itemPCI_name, itemPCI_interface, itemPCI_type,  itemPCI_model, itemPCI_cost ) 
VALUES ( 2,
         'Matrox MGA', 
         'AGP',
         'Video Card',
         'N/A', 
         15 );


INSERT INTO itemPCI ( itemPCI_quantity, itemPCI_name, itemPCI_interface, itemPCI_type,  itemPCI_model, itemPCI_cost ) 
VALUES ( 1,
         'Dial-Up Modem', 
         'PCI',
         'Network Card',
         'N/A', 
         9 );