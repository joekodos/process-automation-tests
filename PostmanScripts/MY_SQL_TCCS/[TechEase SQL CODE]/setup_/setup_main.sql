\! echo ............................
\! echo ............................
\! echo ............................
\! echo ............................
\! echo Welcome to TechEase Database Main Setup!
\! echo ............................
\! echo ............................
\! echo This setup will initialize the TechEase Database Collection.
\! echo ............................
\! echo ............................
\! echo ............................
\! echo Starting...


\! echo Dropping Databases...
SET FOREIGN_KEY_CHECKS=0;
DROP DATABASE store;
DROP DATABASE users;
DROP DATABASE content;
DROP DATABASE item;
SET FOREIGN_KEY_CHECKS=1;

\! echo Creating Database store...
CREATE DATABASE store;
USE store;

\! echo Creating table customer...
CREATE TABLE customer ( cust_id                 SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        cust_firstName          VARCHAR(20),
                        cust_lastName           VARCHAR(20),
                        cust_shipAddr           VARCHAR(100),
                        cust_city               VARCHAR(25),
                        cust_zip                VARCHAR(10),
                        cust_country            VARCHAR(15),
                        cust_paypal_name        VARCHAR(15) UNIQUE,
                        cust_email              VARCHAR(15),
                        UNIQUE INDEX(cust_id) )
        ENGINE = INNODB;
                        
\! echo Creating table orders...                 
CREATE TABLE orders    ( order_id                SMALLINT(5) NOT NULL AUTO_INCREMENT,
                         order_cust_id           SMALLINT(5) NOT NULL UNIQUE,
                         order_item              VARCHAR(100),
                         order_item_id           SMALLINT(5),
                         order_item_count        SMALLINT(5),
                         order_total             SMALLINT(5),
                         PRIMARY KEY(order_id, order_item_id),
                         CONSTRAINT order_cust_id_ref FOREIGN KEY (order_cust_id) REFERENCES customer (cust_id),
                         UNIQUE INDEX(order_id, order_cust_id, order_item_id) )
        ENGINE = INNODB;
                        
                        
                        
                        

\! echo Creating Database users...
CREATE DATABASE users;
USE users;

\! echo Creating table guest...                         
CREATE TABLE guest    ( guest_id                SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        guest_user              VARCHAR(20),
                        guest_pass              VARCHAR(400),
                        guest_cust_id           SMALLINT(5),
                        UNIQUE INDEX(guest_id, guest_cust_id),
                        CONSTRAINT guest_cust_id_ref FOREIGN KEY (guest_cust_id) REFERENCES store.customer (cust_id) 
                                ON DELETE CASCADE )
        ENGINE = INNODB;                        
                        
\! echo Creating table member...                         
CREATE TABLE member   ( mem_id                  SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        mem_user                VARCHAR(20),
                        mem_pass                VARCHAR(400), 
                        mem_role                VARCHAR(25),
                        mem_email               VARCHAR(50), 
                        mem_cust_id             SMALLINT(5),
                        UNIQUE INDEX(mem_id, mem_cust_id),
                        CONSTRAINT mem_cust_id_ref FOREIGN KEY (mem_cust_id) REFERENCES store.customer (cust_id) 
                                ON DELETE CASCADE )
        ENGINE = INNODB; 

 
\! echo Creating table employee...                         
CREATE TABLE employee ( emp_id                  SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        emp_user                VARCHAR(30) NOT NULL,
                        emp_pass                VARCHAR(400), 
                        emp_role                VARCHAR(25), 
                        emp_lastName            VARCHAR(30),
                        emp_firstName           VARCHAR(30),
                        emp_address             VARCHAR(50),
                        emp_email               VARCHAR(50),
                        emp_zip                 VARCHAR(20),
                        emp_city                VARCHAR(25),
                        emp_cust_id             SMALLINT(5),
                        UNIQUE INDEX(emp_id, emp_cust_id),
                        INDEX(emp_user),
                        CONSTRAINT emp_cust_id_ref FOREIGN KEY (emp_cust_id) REFERENCES store.customer (cust_id) 
                                ON DELETE CASCADE )
        ENGINE = INNODB;
                        
                        
\! echo Creating Database content...
CREATE DATABASE content;
USE content;

\! echo Creating table news...    
CREATE TABLE news     ( news_id                 SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        news_author             VARCHAR(25),
                        news_title              VARCHAR(125), 
                        news_descript           TEXT(12000), 
                        news_date               VARCHAR(50) )
        ENGINE = INNODB;                        

       
\! echo Creating table blog...    
CREATE TABLE blog     ( blog_id                 SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        blog_author             VARCHAR(25),
                        blog_title              VARCHAR(125), 
                        blog_descript           TEXT(144000) )
        ENGINE = INNODB;                        
      
      
\! echo Creating table event...  
CREATE TABLE event    ( event_id                SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        event_type              VARCHAR(50),
                        event_title             VARCHAR(125),
                        event_loc               VARCHAR(125), 
                        event_descript          VARCHAR(2000),
                        event_cost              SMALLINT(5) )
        ENGINE = INNODB;                       
                        
\! echo Creating table service...  
CREATE TABLE service  ( svc_id                  SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        svc_title               VARCHAR(125),
                        svc_descript            TEXT,
                        svc_price               VARCHAR(25) )
        ENGINE = INNODB;                        
                        
\! echo Creating table logging...    
CREATE TABLE logging  ( log_id                  SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        log_type                VARCHAR(25),
                        log_category            VARCHAR(50), 
                        log_title               VARCHAR(75),
                        log_descript            VARCHAR(4000) )
        ENGINE = INNODB;
        
\! echo Creating table logAuth...                         
CREATE TABLE logAuth  ( logAuth_id              SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        logAuth_ipaddr          VARCHAR(15),
                        logAuth_username        VARCHAR(50),
                        logAuth_timestamp       DATE,
                        logAuth_success         BOOLEAN )
        ENGINE = INNODB;
        
\! echo Creating table calendar...  
CREATE TABLE calendar ( cal_id                  SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        cal_title               VARCHAR(100),
                        cal_descript            VARCHAR(2000),
                        cal_type                VARCHAR(100),
                        cal_category            VARCHAR(50),
                        cal_months              VARCHAR(15),
                        cal_days                VARCHAR(15) )                   
         ENGINE = INNODB;
         
\! echo Creating table calEntry...                       
CREATE TABLE calEntry ( calEntry_id             SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        calEntry_title          VARCHAR(50),
                        calEntry_descript       VARCHAR(500),
                        calEntry_type           VARCHAR(25),
                        calEntry_category       VARCHAR(25),
                        calEntry_date           DATE )
         ENGINE = INNODB;
         
\! echo Creating table doc...  
CREATE TABLE doc      ( doc_id                  SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        doc_type                VARCHAR(25),
                        doc_category            VARCHAR(50),                          
                        doc_title               VARCHAR(125),
                        doc_descript            TEXT(144000) )
        ENGINE = INNODB;
        
\! echo Creating table tterm...  
CREATE TABLE tterm    ( tterm_id                SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        tterm_type              VARCHAR(25),
                        tterm_category          VARCHAR(50),                          
                        tterm_title             VARCHAR(125),
                        tterm_descript          VARCHAR(5000) )
        ENGINE = INNODB;        

\! echo Creating table company...  
CREATE TABLE company  ( co_id                   SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        co_position             VARCHAR(50),
                        co_special              VARCHAR(300),                          
                        co_descript             VARCHAR(2000),
                        co_emp_user             VARCHAR(30),
                        UNIQUE INDEX(co_id),
                        INDEX(co_emp_user),
                        CONSTRAINT co_emp_user_ref FOREIGN KEY (co_emp_user) REFERENCES users.employee (emp_user) 
                                ON DELETE CASCADE )
        ENGINE = INNODB; 



\! echo Creating Database item...
CREATE DATABASE item;
USE item;

\! echo Creating table itemMain...                          
CREATE TABLE itemMain ( itemMain_id             SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        itemMain_type           VARCHAR(25),
                        itemMain_category       VARCHAR(50), 
                        itemMain_title          VARCHAR(75),
                        itemMain_descript       VARCHAR(4000),
                        itemMain_cost           SMALLINT(5),
                        itemMain_quantity       SMALLINT(5) )
        ENGINE = INNODB;                         

\! echo Creating table itemRAM...  
CREATE TABLE itemRAM  ( itemRAM_id              SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        itemRAM_type            VARCHAR(15),
                        itemRAM_quantity        SMALLINT(5),          
                        itemRAM_size            SMALLINT(5),
                        itemRAM_brand           VARCHAR(25),                          
                        itemRAM_speed           VARCHAR(25),
                        itemRAM_cost            SMALLINT(5) )
        ENGINE = INNODB;                         


\! echo Creating table itemPCI...  
CREATE TABLE itemPCI  ( itemPCI_id              SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        itemPCI_quantity        SMALLINT(5),
                        itemPCI_name            VARCHAR(25),          
                        itemPCI_interface       VARCHAR(15),
                        itemPCI_type            VARCHAR(20),                          
                        itemPCI_model           VARCHAR(25),
                        itemPCI_cost            SMALLINT(5) )
         ENGINE = INNODB;                        

\! echo Creating table itemHDD...  
CREATE TABLE itemHDD  ( itemHDD_id              SMALLINT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        itemHDD_quantity        SMALLINT(5),
                        itemHDD_size            SMALLINT(5),
                        itemHDD_interface       VARCHAR(15),
                        itemHDD_brand           VARCHAR(20),
                        itemHDD_cost            SMALLINT(5) )
        ENGINE = INNODB; 

 
\! echo ............................
\! echo ............................
\! echo Inserting Data...
\! echo ............................
\! echo ............................
USE users;

\! echo Adding Employees... 

INSERT INTO employee ( emp_user, emp_pass, emp_role, emp_lastName, emp_firstName, emp_address, emp_email, emp_zip, emp_city ) 
VALUES ( 'rcilia', 'password', 'admin', 'Cilia', 'Ryan', '38007 Lakeshore Dr.', 'service@techeasecomputers.com', '48045', 'Harrison Township' );
         
INSERT INTO employee ( emp_user, emp_pass, emp_role, emp_lastName, emp_firstName, emp_address, emp_email, emp_zip, emp_city ) 
VALUES ( 'jdkodos', 'password', 'admin', 'Kodos', 'Joseph', '21880 Peepsock', 'jdkodos@mtu.edu', '49931', 'Houghton' );

INSERT INTO employee ( emp_user, emp_pass, emp_role, emp_lastName, emp_firstName, emp_address, emp_email, emp_zip, emp_city ) 
VALUES ( 'asjoblom', 'password', 'admin', 'Sjoblom', 'Alex', '99900 Temp Address', 'asjoblom@techeasecomputers.com', '48045', 'Harrison Township' );

INSERT INTO employee ( emp_user, emp_pass, emp_role, emp_lastName, emp_firstName, emp_address, emp_email, emp_zip, emp_city ) 
VALUES ( 'aventimi', 'password', 'admin', 'Ventimiglia', 'Anthony', '38007 Lakeshore Dr.', 'aventimi@techeasecomputers.com', '48045', 'Harrison Township' );



\! echo Adding Services... 
USE content;

INSERT INTO service ( svc_title, svc_descript, svc_price ) 
VALUES ( 'Diagnosis + Clean Computer', 
         '>Remove dust, oil slow fans, check for physical damage.
          >Discover what is causing problems.
          >If the problem is minor, we will fix it.
          We’ll create a summary sheet for your computer and include suggestions 
            on how to resolve any problems you bring to us.', 
         '$20' );
          
          
INSERT INTO service ( svc_title, svc_descript, svc_price ) 
VALUES ( 'Resolve Diagnosed Problems', 
         'Resolve any problems diagnosed, if diagnosis purchased above. 
          This does not include reinstalling any operating systems. 
          A good example would be virus removal, or system optimization.', 
         '$10' );
          
          
INSERT INTO service ( svc_title, svc_descript, svc_price ) 
VALUES ( 'Reinstall', 
         'If you know that you would just like a reinstallation, we will back 
          up your data to our servers for the duration of the reinstallation 
          and restore the operating system as closely as when we found it, minus the problems.', 
         '$20' );
   
   
INSERT INTO service ( svc_title, svc_descript, svc_price ) 
VALUES ( 'Computer Build', 
         'Wish us to build a computer? Bring us the parts, or talk with us and we will help you 
          find the best parts for what you need the computer to do.
          We’ll install the desired operating system, optimize settings, and restore any files 
          and settings from an old computer if you wish.', 
         '$50' );


INSERT INTO service ( svc_title, svc_descript, svc_price ) 
VALUES ( 'Home Network Setup', 
         'Have a home network you’d like us to set up? We’ll set up a router and connect any 
          computers to your new network, in addition to configuring any settings you’d like.', 
         '$50' );

          
INSERT INTO service ( svc_title, svc_descript, svc_price ) 
VALUES ( 'Small Business Network Setup', 
         'Have a small business you’d like us to help you with?
          Please contact us and explain exactly what you’d like. A couple of examples may be:
           -Set up 0-3 servers with 5-15 workstations on one or multiple networks in your building.
           -Set up routing between multiple locations. We limit the distance between locations by 
             75 miles, unless we must only configure settings and remote access information is provided.', 
         '$100' );

  
INSERT INTO service ( svc_title, svc_descript, svc_price ) 
VALUES ( 'Multiple-PCs', 
         'If you have multiple services you’d like us to perform, for example, a diagnosis or 
           reinstall to multiple computers, please call us for more information.
           In the future, we may create a price chart if this is a popular service choice.', 
         'Contact Us' );
          
          
INSERT INTO service ( svc_title, svc_descript, svc_price ) 
VALUES ( 'Websites', 
         'If you would like to setup a website for any reason, 
           but just do not have the time or know-how, let us help!', 
         'Contact Us' );          
          
          
INSERT INTO service ( svc_title, svc_descript, svc_price ) 
VALUES ( '3d Graphics/Imaging', 
         'Want a commercial or 3d graphic made? Let us help you!', 
         'Contact Us' );          





\! echo Adding News... 


INSERT INTO news ( news_author, news_title, news_descript, news_date ) 
VALUES ( 'System', 
         'Michigan Gumball Sponsorship', 
         'Every Monday this summer the Michigan Gumball Car Show 
	   will be sponsoring us at Hooters in Roseville.',
          '6/3/2012' );


INSERT INTO news ( news_author, news_title, news_descript, news_date ) 
VALUES ( 'System', 
         'Event - Donations', 
         'Computer repair event for donations',
          '5/16/2012' );


INSERT INTO news ( news_author, news_title, news_descript, news_date ) 
VALUES ( 'System', 
         'Milestone - Website', 
         'Website Published',
          '5/14/2012' );

          
INSERT INTO news ( news_author, news_title, news_descript, news_date ) 
VALUES ( 'System', 
         'Milestone - Website', 
         'Website Created',
          '5/1/2012' );


INSERT INTO news ( news_author, news_title, news_descript, news_date ) 
VALUES ( 'System', 
         'Milestone - Domain', 
         'Network Created',
          '4/25/2012' );

 
INSERT INTO news ( news_author, news_title, news_descript, news_date ) 
VALUES ( 'System', 
         'Milestone - Established', 
         'TechEase Established',
          '3/15/2012' );




\! echo Adding Company... 


INSERT INTO company ( co_position, co_special, co_descript, co_emp_user ) 
VALUES ( 'Founder', 
         'Specialty: Laptop Assembly & Servicing', 
         'Attends Macomb Community College',
         'rcilia' );

INSERT INTO company ( co_position, co_special, co_descript, co_emp_user ) 
VALUES ( 'Co-Founder', 
         'Specialty: Computer Networking & Systems Administration', 
         'Attends Michigan Technological University
          Expected Graduation Date: Fall 2013',
         'jdkodos' );

INSERT INTO company ( co_position, co_special, co_descript, co_emp_user ) 
VALUES ( 'Technician', 
         'Specialty: I have 4 years of computer graphics experience to help 
           you along the way of making your next product!', 
         'Description coming soon!',
         'asjoblom' );

INSERT INTO company ( co_position, co_special, co_descript, co_emp_user ) 
VALUES ( 'Support', 
         'Specialty: Assisting you in helping us solve your problems!', 
         'Description coming soon!',
         'aventimi' );




\! echo Adding Items... 
USE item;

INSERT INTO itemMain ( itemMain_type, itemMain_category, itemMain_title, itemMain_descript, itemMain_cost, itemMain_quantity ) 
VALUES ( 'Hardware', 
         'SD-RAM',
         '32MB, 168-pin, PC100-133',
         'Used - Tested Working', 
         5,          
         1 );

         
INSERT INTO itemMain ( itemMain_type, itemMain_category, itemMain_title, itemMain_descript, itemMain_cost, itemMain_quantity ) 
VALUES ( 'Hardware', 
         'SD-RAM',
         '64MB, 168-pin, PC100-133',
         'Used - Tested Working', 
         10,          
         1 );


INSERT INTO itemMain ( itemMain_type, itemMain_category, itemMain_title, itemMain_descript, itemMain_cost, itemMain_quantity ) 
VALUES ( 'Hardware', 
         'SD-RAM',
         '128MB, 168-pin, PC100-133',
         'Used - Tested Working', 
         15,          
         1 );


INSERT INTO itemMain ( itemMain_type, itemMain_category, itemMain_title, itemMain_descript, itemMain_cost, itemMain_quantity ) 
VALUES ( 'Hardware', 
         'DDR1-RAM',
         '256MB, 184-pin, 200 to 400 MHz',
         'Used - Tested Working', 
         10,          
         3 );


INSERT INTO itemMain ( itemMain_type, itemMain_category, itemMain_title, itemMain_descript, itemMain_cost, itemMain_quantity ) 
VALUES ( 'Hardware', 
         'DDR1-RAM',
         '512MB, 184-pin, 200 to 400 MHz',
         'Used - Tested Working', 
         15,          
         3 );


INSERT INTO itemMain ( itemMain_type, itemMain_category, itemMain_title, itemMain_descript, itemMain_cost, itemMain_quantity ) 
VALUES ( 'Hardware', 
         'DDR1-RAM',
         '1024MB, 184-pin, 200 to 400 MHz',
         'Used - Tested Working', 
         20,          
         2 );


INSERT INTO itemMain ( itemMain_type, itemMain_category, itemMain_title, itemMain_descript, itemMain_cost, itemMain_quantity ) 
VALUES ( 'Hardware', 
         'DDR2-RAM',
         '256MB, 240-pin, 400 MHz',
         'Used - Tested Working', 
         15,          
         1 );
                

INSERT INTO itemMain ( itemMain_type, itemMain_category, itemMain_title, itemMain_descript, itemMain_cost, itemMain_quantity ) 
VALUES ( 'Hardware', 
         'DDR2-RAM',
         '512MB, 240-pin, 400 to 633 MHz',
         'Used - Tested Working', 
         20,          
         2 );
                

INSERT INTO itemMain ( itemMain_type, itemMain_category, itemMain_title, itemMain_descript, itemMain_cost, itemMain_quantity ) 
VALUES ( 'Hardware', 
         'DDR2-RAM',
         '1024MB, 240-pin, 533 MHz',
         'Used - Tested Working', 
         25,          
         1 );
                 

INSERT INTO itemMain ( itemMain_type, itemMain_category, itemMain_title, itemMain_descript, itemMain_cost, itemMain_quantity ) 
VALUES ( 'Hardware', 
         '462-CPU',
         '1500MHz to 3000MHz Single Cores.',
         'Used - Tested Working. Socket 462 for AMD Athlon Compatible Motherboards.', 
         30,          
         8 );        
                 

INSERT INTO itemMain ( itemMain_type, itemMain_category, itemMain_title, itemMain_descript, itemMain_cost, itemMain_quantity ) 
VALUES ( 'Hardware', 
         '775-CPU',
         '2000MHz Single Cores to 3000MHz Dual Cores',
         'Used - Tested Working. Socket LGA775 for Intel Compatible Motherboards.', 
         40,          
         8 );
         
         
INSERT INTO itemMain ( itemMain_type, itemMain_category, itemMain_title, itemMain_descript, itemMain_cost, itemMain_quantity ) 
VALUES ( 'Hardware', 
         '775-CPU-Heatsink',
         'Socket 775 Intel Heatsink',
         'New - In Box. LGA775 CPU Cooler.', 
         20,          
         1 );
         

    
\! echo ..........................................       
\! echo ..........................................        
\! echo Adding dummy data...  








