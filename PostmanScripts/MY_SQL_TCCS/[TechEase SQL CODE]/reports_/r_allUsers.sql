﻿/*  TechEase REPORT: GOAL - DISPLAY ALL USERS -- IN PROGRESS  */

SELECT users.member.mem_user, users.guest.guest_user 
        FROM users.member, users.guest
        GROUP BY mem_user
UNION
SELECT users.guest.guest_user, users.member.mem_user 
        FROM users.guest, users.member
        GROUP BY guest_user



/* END TECHEASE REPORT --**--**--**--**--**--**--**--**-- BEGIN SCRAP CODE */




PROMPT SAT3210/Lab 09 - Question 2, PG202-203
PROMPT Name:  Joseph Kodos

PROMPT Question ............2...............
/*   SQL   commands   

Modify the report from question 1:
        Display each employee SSN once for each group of dependents.
        Use the COUNT function to display the total number of dependents.

*/
/* Display Columns */
SELECT dep_emp_ssn "Emp SSN", dep_name "Dependent", dep_gender "Gender", 
        dep_date_of_birth "Date of Birth", dep_relationship "Relationship"
        /* Select Table */
        FROM dependent
        /* Sort ascending by dependent employee SSN */
        ORDER BY dep_emp_ssn;
        
SELECT DISTINCT users.a.emp_user, users.member.mem_user, users.guest.guest_user
        FROM users.employee a, users.member, users.guest, users.employee
        WHERE users.a.emp_id = users.employee.emp_id;
        

        
       
SELECT  users.employee.emp_user, users.member.mem_user, users.guest.guest_user
        FROM users.employee, users.member, users.guest;
        WHERE users.employee.emp_user =
        ( SELECT users.a.emp_user
                FROM users.employee a
                        WHERE users.a.emp_user = users.employee.emp_user
                                AND
                             users.a. = users.a.emp_id);

                               

SELECT users.employee.emp_user, users.guest.guest_user
        FROM users.employee, users.guest
        LEFT JOIN users.employee e
                ON users.e.emp_user;
        


SELECT users.member.mem_user 
        FROM users.member       
UNION  
SELECT users.guest.guest_user
        FROM users.guest;
        
        
SELECT users.employee.emp_user, users.guest.guest_user
        FROM users.employee, users.guest
        WHERE users.employee.emp_user = 
        (SELECT users.emp_id, users.e
                FROM users.employee e
                        WHERE users.employee.emp_user = users.e.emp_user);
        
        
        

SELECT users.employee.emp_user, users.member.mem_user
        FROM  ( SELECT DISTINCT 
                        emp_user 
        
        
        
        
        
        
        GROUP BY users.employee.emp_user AND users.guest.guest_user;
        
        
        
                WHERE users.employee.emp_user IN (
                        SELECT users.guest.guest_user
                                FROM users.guest );
                
                
                
                
        FULL OUTER JOIN users.employee
                ON users.e.emp_user;