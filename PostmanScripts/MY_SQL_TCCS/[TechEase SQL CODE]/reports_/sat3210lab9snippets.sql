﻿

/*   SQL   commands   

Modify the report from question 3:
        Use ACCEPT and PROMPT commands to allow an employee to enter an SSN
                and see that SSN's dependents.
        Replace the view with a select statement.

*/
\! echo Question ............3...............
/* Welcome and \! echo the user for a SSN */
\! echo ...............
\! echo ....WELCOME....
\! echo ...............
ACCEPT ssn_var \! echo 'Enter a SSN: '
/* This pause causes the user to hit enter twice:
        first, to input the SSN
        second, to process the report.           */
PAUSE Press Enter to continue.

/* Setup the top and bottom titles.
        Use system page number.          
        Use the employee_name_var to display the employee name. */
TTITLE          LEFT   date_var -
                RIGHT  'Page: '          FORMAT 999 sql.pno -
                CENTER 'Employee Name: ' FORMAT A13 emp_name_var SKIP 2
BTITLE  SKIP 1  CENTER 'NOT FOR EXTERNAL DISSEMINATION.'

        /* Build the SSN number using SUBSTR(column, start#,strLength) and concatenation */
SELECT   SUBSTR(e.emp_ssn, 1, 3)||'-'||SUBSTR(e.emp_ssn,4,2)||'-'||SUBSTR(e.emp_ssn,6,4) "SSN", 
                /* Display remaining Columns */
                 e.emp_last_name||', '||e.emp_first_name "Employee Name",
                 d.dep_name "Dependent Name", d.dep_gender "Gender", d.dep_relationship "Relationship"
            /* Select Tables and Set Aliases */
            FROM employee e, dependent d
             /* Match rows in the employee dependent tables via employee SSN 
                        and match rows matching the SSN that was provided */
            WHERE e.emp_ssn = '&ssn_var' AND e.emp_ssn = d.dep_emp_ssn
                /* Sort ascending by employee SSN */
                ORDER BY e.emp_ssn;
                
\! echo Question ............4...............
/*   SQL   commands   

Modify the report from question 3:
        Add dashes in the SSN number.

*/
                                          /* Create View, Display Columns */
CREATE OR REPLACE VIEW employee_dependent (     ssn,    employee_name,    dependent_name, 
                                                gender,    relationship    )        AS 
                 /* build the SSN number using SUBSTR(column, start#,strLength) and concatenation */
        SELECT   SUBSTR(e.emp_ssn, 1, 3)||'-'||SUBSTR(e.emp_ssn,4,2)||'-'||SUBSTR(e.emp_ssn,6,4) "SSN", 
                 /* Select remaining Columns */
                 e.emp_last_name||', '||e.emp_first_name,
                 d.dep_name, d.dep_gender, d.dep_relationship
            /* Select Tables and Set Aliases */
            FROM employee e, dependent d
                /* Match rows in the employee dependent tables via employee SSN */
            WHERE e.emp_ssn = d.dep_emp_ssn
                /* Sort ascending by employee SSN */
                ORDER BY e.emp_ssn;

/* Assign the Employee Name to employee_name_var to 
        display the employee name in the title.          */        
COLUMN employee_name NEW_VALUE employee_name_var NOPRINT

/* Format Columns appropriately */ 
COLUMN ssn FORMAT A12
COLUMN dependent_name FORMAT A15
COLUMN gender FORMAT A6
COLUMN relationship FORMAT A12
/* Setup the top and bottom titles. 
        Use system page number.          
        Use the employee_name_var to display the employee name. */
TTITLE  RIGHT 'Page: ' FORMAT 999 sql.pno -
        CENTER 'Employee Name: ' employee_name_var SKIP 2
BTITLE SKIP 1 CENTER 'NOT FOR EXTERNAL DISSEMINATION.'

/* Display each employee SSN once */
BREAK ON ssn SKIP PAGE
/* Display report total */
COMPUTE COUNT LABEL 'No Dep' OF "relationship" ON ssn

/* Display Columns using the view */
SELECT ssn, employee_name, dependent_name, gender, relationship
FROM employee_dependent;

/* Clean up the environment */
\! echo  Clearing variables...
UNDEFINE employee_name_var;






