{
  "Name": "DELETE",
  "Description": "DELETE",
  "Assumption": "DELETE",
  "Limitation": "DELETE",
  "Note": "DELETE",
  "EstimatedTimeToRun": 0.15,
  "Data": "DELETE",
  "State": {
    "Id": 4
  },
  "Product": {
    "Id": 37
  },
  "Priority": {
    "Id": 4
  },
  "TestType": {
    "Id": 7
  },
  "HowFound": {
    "Id": 5
  },
  "CodeBase": {
    "Id": 54
  },
  "Release": {
    "Id": 125
  },
  "Steps": [
    {
      "Action": "DELETE",
      "ExpectedResult": "DELETE",
      "Comment": "DELETE"
    }
  ]
}