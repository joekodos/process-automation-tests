		{
			"id": "itemIDnum",
			"name": "workItem_Cancel",
			"dataMode": "raw",
			"data": [],
			"rawModeData": "{\r\n    \"commandId\":\"Cancel\"\r\n}"
			"headers": "Authorization: IPC {{auth-dca}}\nININ-Session: {{sess-id}}\n",
			"method": "POST",
			"pathVariables": {},
			"url": "{{server_url_dca}}/api/v1/processautomation/workitems/instances/{{workitemInstanceId}}/commands/cancel",
			"preRequestScript": "",
			"tests": "",
			"version": 2,
			"currentHelper": "normal",
			"helperAttributes": "{}",
			"collectionId": "00000000-aaaa-0000-0000-0000000000collectionNumber",
			"write": true,
			"synced": false
		},