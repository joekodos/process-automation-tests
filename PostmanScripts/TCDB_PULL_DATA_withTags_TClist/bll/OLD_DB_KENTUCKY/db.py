import platform
import dal

def login_gimpy(username):

    """Login to gimpy"""

    # This is the gimpy URL used to login to the database       *****IMPORTANT*****
    gimpyUrl = "http://qf-kentucky/tcdbv2/login"         # <---- Use this Testing Env URL first before using Prod env
                                                    # Prod env url: "http://qf-kentucky/tcdbv2/login"
                                                    # Test env url: "http://qf-kentucky/tcdbv2/login"

    # Method body:
    loginPayload = {
        "UserName": username,
        "Password": ""
    }

    # Method Headers:
    loginHeaders = {'content-type': 'application/json'}

    # Send the request with parameters and store it in login_response
    login_response = dal.tcdb.login_gimpy_call(gimpyUrl, loginPayload, loginHeaders)

    # Use the .json function() to get the data in json format and then we store it in login_response_json variable
    login_response_json = login_response.json()

    # Store the token
    token = login_response_json["Token"]
    return token

def execute_a_test_case(body, token, testCaseNumber, testCaseVersion):

    """Create the Test Case"""

    # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
    executeTestCaseURL = 'http://qf-kentucky/tcdbv2/api/testcases/' + str(testCaseNumber) + '/' + str(testCaseVersion) + '/Executions' # <--- Use this test env url first then Prod
                                                            # Prod env url: http://qf-kentucky/tcdbv2/api/testcases


    # Method body
    executeTestCase_PayLoad = body

    # Method Headers
    executeTestCase_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    executeTestCase_response = dal.tcdb.execute_a_test_case_call(executeTestCaseURL, executeTestCase_PayLoad, executeTestCase_headers)


    """Store the addTestCase response"""

    # Use the .json function() to get the data in json format and then we store it in updateTestCase_response variable
    executeTestCase_response = executeTestCase_response.json()
    return executeTestCase_response

def update_feature_suite(fsid, testcaseid, token):

    """Update Feature Suite"""

    # This is the gimpy URL used to add a test case to the DB
    updateFsURL = "http://qf-kentucky/tcdbv2/api/testsuites/"+fsid+"/testcases"

    # Method body
    updateFsPayload = [
    {
        "Id": testcaseid
    }
    ]

    # Method Headers
    updateFsHeaders = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    updateFsResponse = dal.tcdb.update_feature_suite_call(updateFsURL, updateFsPayload, updateFsHeaders)
    return updateFsResponse

def get_a_test_case(token, testCaseNumber):

        """Get a Test Case"""
        # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
        getTestCaseURL = "http://qf-kentucky/tcdbv2/api/testcases/" + str(testCaseNumber)   # <--- Use this test env url first then Prod

                                                                                                  # Prod env url: http://qf-kentucky/tcdbv2/api/testcases
        # Method Headers
        getTestCase_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}
        # Specify request body json data and headers
        getTestCase_response = dal.tcdb.get_a_test_case_call(getTestCaseURL, getTestCase_headers)

        """Store the getTestCase response"""
        # Use the .json function() to get the data in json format and then we store it in getTestCase_response variable
        getTestCase_response = getTestCase_response.json()
        return getTestCase_response




def get_a_test_suite(token, testSuiteNumber):

        """Get a Test SUITE"""
        # This is the gimpy URL used to get a test suite from the DB                       *****IMPORTANT*****
        getTestSuiteURL = "http://qf-kentucky/tcdbv2/api/testsuites/" + str(testSuiteNumber)   # <--- Use this test env url first then Prod

                                                                                                  # Prod env url: http://qf-kentucky/tcdbv2/api/testcases
        # Method Headers
        getTestSuite_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}
        # Specify request body json data and headers
        getTestSuite_response = dal.tcdb.get_a_test_suite_call(getTestSuiteURL, getTestSuite_headers)
        
        """Store the getTestSuite response"""
        # Use the .json function() to get the data in json format and then we store it in getTestCase_response variable
        getTestSuite_response = getTestSuite_response.json()
        return getTestSuite_response

def get_a_test_case_tags(token, testCaseNumber, testCaseVersion):

        """Get a Test Case's Tags"""
        # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
        getTestCaseTagsURL = "http://qf-kentucky/tcdbv2/api/testcases/" + str(testCaseNumber) + "/" + str(testCaseVersion) + "/tags"    # <--- Use this test env url first then Prod

                                                                                                  # Prod env url: http://qf-kentucky/tcdbv2/api/testcases
        # Method Headers
        getTestCaseTags_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}
        # Specify request body json data and headers
        getTestCaseTags_response = dal.tcdb.get_a_test_case_tags_call(getTestCaseTagsURL, getTestCaseTags_headers)

        """Store the getTestCaseTags response"""
        # Use the .json function() to get the data in json format and then we store it in getTestCaseTags_response variable
        getTestCaseTags_response = getTestCaseTags_response.json()
        print "getTestCaseTags_response: "
        print getTestCaseTags_response
        return getTestCaseTags_response
        
        

def checkPathConfig ():
    # Get os type
    os_type = platform.system()
    if os_type == "Windows":
        dal.config.checkWindowsConfig()
    else:
        dal.config.checkMacConfig()

def setDirPath():
    dirPath = dal.config.getDirPath()
    return dirPath


def setConfigPath():
    configPath = dal.config.getConfigPath()
    return configPath


def setExcelPath():
    excelPath = dal.config.getExcelPath()
    return excelPath

def setTSname(name):
    dal.config.createTCSet(name)


def openWB(path):
    wb = dal.excel.openExcileFile(path)
    return wb

def openWS(path):
    ws = dal.excel.getSheet(path)
    return ws