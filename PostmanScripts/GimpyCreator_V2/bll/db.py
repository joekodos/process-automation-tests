import platform
import dal


def login_gimpy(username):

    """Login to gimpy"""

    # This is the gimpy URL used to login to the database       *****IMPORTANT*****
    gimpyUrl = "http://tcdb.inin.com/tcdbv2/login"         # <---- Use this Testing Env URL first before using Prod env
                                                    # Prod env url: "http://tcdb.inin.com/tcdbv2/login"
                                                    # Test env url: "http://qf-kentucky/tcdbv2/login"

    # Method body:
    loginPayload = {
        "UserName": username,
        "Password": ""
    }

    # Method Headers:
    loginHeaders = {'content-type': 'application/json'}

    # Send the request with parameters and store it in login_response
    login_response = dal.tcdb.login_gimpy_call(gimpyUrl, loginPayload, loginHeaders)

    # Use the .json function() to get the data in json format and then we store it in login_response_json variable
    login_response_json = login_response.json()

    # Store the token
    token = login_response_json["Token"]
    return token

def add_a_test_case(body, token):

    """Create the Test Case"""

    # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
    addTestCaseURL = "http://tcdb.inin.com/tcdbv2/api/testcases" # <--- Use this test env url first then Prod
                                                            # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases

    # Method body
    addTestCase_PayLoad = body

    # Method Headers
    addTestCase_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    addTestCase_response = dal.tcdb.add_a_test_case_call(addTestCaseURL, addTestCase_PayLoad, addTestCase_headers)


    """Store the addTestCase response"""

    # Use the .json function() to get the data in json format and then we store it in addTestCase_response variable
    addTestCase_response = addTestCase_response.json()
    return addTestCase_response

def patch_a_test_case_tags(token, testCaseNumber, testCaseVersion, patchTestCaseTags_data):
        print "patchTestCaseTags_data *********"
        print "patchTestCaseTags_data *********"
        print "patchTestCaseTags_data *********"
        print patchTestCaseTags_data
        """Add to a Test Case's Tags"""
        # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
        patchTestCaseTagsURL = "http://tcdb.inin.com/tcdbv2/api/testcases/" + str(testCaseNumber) + "/" + str(testCaseVersion) + "/tags"    # <--- Use this test env url first then Prod

                                                                                                  # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases
        # Method Headers
        patchTestCaseTags_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}
        
        # Specify request body json data and headers
        patchTestCaseTags_response = dal.tcdb.patch_a_test_case_tags_call(patchTestCaseTagsURL, patchTestCaseTags_data, patchTestCaseTags_headers)
        
        """Store the getTestCaseTags response"""
        # Use the .json function() to get the data in json format and then we store it in getTestCaseTags_response variable
        patchTestCaseTags_response = patchTestCaseTags_response.json()
        print "patchTestCaseTags_response: "
        print patchTestCaseTags_response
        return patchTestCaseTags_response




def update_feature_suite(fsid, testcaseid, token):
    fixedFSID = fsid.split(".")
    fsidX = fixedFSID[0]
    """Update Feature Suite"""

    # This is the gimpy URL used to add a test case to the DB
    updateFsURL = "http://tcdb.inin.com/tcdbv2/api/testsuites/"+fsidX+"/testcases"
    print "updateFsURL updateFsURL updateFsURL updateFsURL"
    print updateFsURL
    # Method body
    updateFsPayload = [
    {
        "Id": testcaseid
    }
    ]
    print "updateFsPayload updateFsPayload updateFsPayload"
    print updateFsPayload

    # Method Headers
    updateFsHeaders = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    updateFsResponse = dal.tcdb.update_feature_suite_call(updateFsURL, updateFsPayload, updateFsHeaders)
    return updateFsResponse

def checkPathConfig ():
    # Get os type
    os_type = platform.system()
    if os_type == "Windows":
        dal.config.checkWindowsConfig()
    else:
        dal.config.checkMacConfig()

def setDirPath():
    dirPath = dal.config.getDirPath()
    return dirPath


def set_add_a_test_case_01():
    add_a_test_case_01 = dal.config.get_add_a_test_case_01()
    return add_a_test_case_01

def set_add_a_test_case_02():
    add_a_test_case_02 = dal.config.get_add_a_test_case_02()
    return add_a_test_case_02

def set_add_a_test_case_03():
    add_a_test_case_03 = dal.config.get_add_a_test_case_03()
    return add_a_test_case_03

def setExcelPath():
    excelPath = dal.config.getExcelPath()
    return excelPath

def setTSname(name):
    dal.config.createTCSet(name)


def openWB(path):
    wb = dal.excel.openExcileFile(path)
    return wb

def openWS(path):
    ws = dal.excel.getSheet(path)
    return ws