import requests
import simplejson as json

def login_gimpy_call(url, body, headers):
    # Send the request with parameters and store it in response
    response = requests.post(url, data=json.dumps(body), headers=headers)
    return response


''' ----------- Test Case ----------- '''
def add_a_test_case_call(url, body, headers):
    # Send the request with parameters and store it in response
    response = requests.post(url, data=json.dumps(body), headers=headers)
    return response

def update_a_test_case_call(url, body, headers):
    # Send the request with parameters and store it in response
    response = requests.put(url, data=json.dumps(body), headers=headers)
    return response

def get_a_test_case_call(url, headers):
    # Send the request with parameters and store it in response
    response = requests.get(url, headers=headers)
    return response

def execute_a_test_case_call(url, body, headers):
    # Send the request with parameters and store it in response
    response = requests.post(url, data=json.dumps(body), headers=headers)
    return response


''' ----------- Feature Suite ----------- '''
def update_feature_suite_call(url, body, headers):
    # Send the request with parameters and store it in response
    response = requests.put(url, data=json.dumps(body), headers=headers)
    return response

def delete_tc_from_feature_suite_call(url, headers):
    # Send the request with parameters and store it in response
    response = requests.delete(url, headers=headers)
    return response

def get_a_test_suite_call(url, headers):
    # Send the request with parameters and store it in response
    response = requests.get(url, headers=headers)
    return response


''' ----------- Tags ----------- '''
def get_a_test_case_tags_call(url, headers):
    # Send the request with parameters and store it in response
    response = requests.get(url, headers=headers)
    return response

def patch_a_test_case_tags_call(url, body, headers):
    # Send the request with parameters and store it in response
    print "patch_a_test_case_tags_call patch_a_test_case_tags_call patch_a_test_case_tags_call ***&&&&&&&"
    print body
    print "json.dumps(body)"
    print json.dumps(body)
    response = requests.patch(url, body, headers=headers)
    return response

def delete_a_test_case_tag_call(url, headers):
    # Send the request with parameters and store it in response
    response = requests.delete(url, headers=headers)
    return response
