import requests as http


def login(user):
    body = {"UserName": user, "Password": ""}
    res = http.post("http://tcdb.inin.com/tcdbv2/login", data=body)
    return res.json()["Token"]


def removeTestcaseFromTestSuite(user, testCaseId):
    testCase = http.get("http://tcdb.inin.com/tcdbv2/api/testcases/{0}".format(testCaseId)).json()
    token = login(user)
    putHeader = {"Token": token, "Content-Type": "application/json"}
    testSuites = []
    for testSuite in testCase["TestSuites"]:
        testSuites.append(testSuite["Id"])

    for testSuiteId in testSuites:
        testSuite = http.get("http://tcdb.inin.com/tcdbv2/api/testsuites/{0}".format(testSuiteId)).json()
        testSuitePutBody = {}

        testSuitePutBody["Name"] = testSuite["Name"]
        testSuitePutBody["Description"] = testSuite["Description"]
        testSuitePutBody["State"] = {}
        testSuitePutBody["State"]["Id"] = testSuite["State"]["Id"]
        testSuitePutBody["Codebase"] = {}
        testSuitePutBody["Codebase"]["Id"] = testSuite["Codebase"]["Id"]

        testCasesPutList = []
        testCases = testSuite["TestCases"]
        for testCase in testCases:
            if testCase["Id"] != testCaseId:
                newTestCase = {}
                newTestCase["Id"] = testCase["Id"]
                testCasesPutList.append(newTestCase)

        testSuitePutBody["TestCases"] = testCasesPutList
        res = http.put("http://tcdb.inin.com/tcdbv2/api/testsuites/{0}".format(testSuiteId), json=testSuitePutBody, headers=putHeader)
        print(res.status_code)

testCases = [
    {"Id": 9578},
    {"Id": 6820},
    {"Id": 6821},
    {"Id": 6822},
    {"Id": 6823},
    {"Id": 6839},
    {"Id": 20256},
    {"Id": 20229},
    {"Id": 20273},
    {"Id": 20428},
    {"Id": 20274},
    {"Id": 20457},
    {"Id": 23136},
    {"Id": 23236},
    {"Id": 25146},
    {"Id": 21696},
    {"Id": 21697},
    {"Id": 21698},
    {"Id": 21716},
    {"Id": 21910},
    {"Id": 22330},
    {"Id": 22332},
    {"Id": 10548},
    {"Id": 10520},
    {"Id": 10542}
]
for testCase in testCases:
    removeTestcaseFromTestSuite("Nicholas.Hubbard", testCase["Id"])




