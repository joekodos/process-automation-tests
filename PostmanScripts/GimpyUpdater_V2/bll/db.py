import platform
import dal

def login_gimpy(username):

    """Login to gimpy"""

    # This is the gimpy URL used to login to the database       *****IMPORTANT*****
    gimpyUrl = "http://tcdb.inin.com/tcdbv2/login"         # <---- Use this Testing Env URL first before using Prod env
                                                    # Prod env url: "http://tcdb.inin.com/tcdbv2/login"
                                                    # Test env url: "http://qf-kentucky/tcdbv2/login"

    # Method body:
    loginPayload = {
        "UserName": username,
        "Password": ""
    }

    # Method Headers:
    loginHeaders = {'content-type': 'application/json'}

    # Send the request with parameters and store it in login_response
    login_response = dal.tcdb.login_gimpy_call(gimpyUrl, loginPayload, loginHeaders)

    # Use the .json function() to get the data in json format and then we store it in login_response_json variable
    login_response_json = login_response.json()

    # Store the token
    token = login_response_json["Token"]
    return token

def update_a_test_case(body, token, testCaseNumber, testCaseVersion):

    """Create the Test Case"""

    # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
    updateTestCaseURL = 'http://tcdb.inin.com/tcdbv2/api/testcases/' + str(testCaseNumber) + '/' + str(testCaseVersion) # <--- Use this test env url first then Prod
                                                            # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases

    # Method body
    updateTestCase_PayLoad = body

    # Method Headers
    updateTestCase_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    updateTestCase_response = dal.tcdb.update_a_test_case_call(updateTestCaseURL, updateTestCase_PayLoad, updateTestCase_headers)


    """Store the addTestCase response"""

    # Use the .json function() to get the data in json format and then we store it in updateTestCase_response variable
    updateTestCase_response = updateTestCase_response.json()
    
    return updateTestCase_response

def update_feature_suite(fsid, testcaseid, token):
    fixedFSID = fsid.split(".")
    fsidX = fixedFSID[0]
    """Update Feature Suite"""

    # This is the gimpy URL used to add a test case to the DB
    updateFsURL = "http://tcdb.inin.com/tcdbv2/api/testsuites/"+fsidX+"/testcases"
    print "updateFsURL updateFsURL updateFsURL updateFsURL"
    print updateFsURL
    # Method body
    updateFsPayload = [
    {
        "Id": testcaseid
    }
    ]
    print "updateFsPayload updateFsPayload updateFsPayload"
    print updateFsPayload

    # Method Headers
    updateFsHeaders = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    updateFsResponse = dal.tcdb.update_feature_suite_call(updateFsURL, updateFsPayload, updateFsHeaders)
    return updateFsResponse
    
def delete_tc_from_feature_suite(fsid, testcaseid, token):
    fsidStr = str(fsid)
    fixedFSID = fsidStr.split(".")
    fsidX = fixedFSID[0]
    """Delete From Feature Suite"""

    # This is the gimpy URL used to add a test case to the DB
    deleteFsURL = "http://tcdb.inin.com/tcdbv2/api/testsuites/"+fsidX+"/testcases/" + testcaseid
    print "deleteFsURL deleteFsURLdeleteFsURL"
    print deleteFsURL

    # Method Headers
    deleteFsHeaders = {'content-type': 'application/json',
                               'Token': '%s' % token}

    # Specify request body json data and headers
    deleteFsResponse = dal.tcdb.delete_tc_from_feature_suite_call(deleteFsURL, deleteFsHeaders)
    return deleteFsResponse

def get_a_test_case(token, testCaseNumber):

        """Get a Test Case"""
        # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
        getTestCaseURL = "http://tcdb.inin.com/tcdbv2/api/testcases/" + str(testCaseNumber)   # <--- Use this test env url first then Prod

                                                                                                  # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases
        # Method Headers
        getTestCase_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}
        # Specify request body json data and headers
        getTestCase_response = dal.tcdb.get_a_test_case_call(getTestCaseURL, getTestCase_headers)
        
        """Store the getTestCase response"""
        # Use the .json function() to get the data in json format and then we store it in getTestCase_response variable
        getTestCase_response = getTestCase_response.json()
        return getTestCase_response



def get_a_test_case_tags(token, testCaseNumber, testCaseVersion):

        """Get a Test Case's Tags"""
        # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
        getTestCaseTagsURL = "http://tcdb.inin.com/tcdbv2/api/testcases/" + str(testCaseNumber) + "/" + str(testCaseVersion) + "/tags"    # <--- Use this test env url first then Prod

                                                                                                  # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases
        # Method Headers
        getTestCaseTags_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}
        # Specify request body json data and headers
        getTestCaseTags_response = dal.tcdb.get_a_test_case_tags_call(getTestCaseTagsURL, getTestCaseTags_headers)

        """Store the getTestCaseTags response"""
        # Use the .json function() to get the data in json format and then we store it in getTestCaseTags_response variable
        getTestCaseTags_response = getTestCaseTags_response.json()
        print "getTestCaseTags_response: "
        print getTestCaseTags_response
        return getTestCaseTags_response
        
        
def patch_a_test_case_tags(token, testCaseNumber, testCaseVersion, patchTestCaseTags_data):
        print "patchTestCaseTags_data *********"
        print "patchTestCaseTags_data *********"
        print "patchTestCaseTags_data *********"
        print patchTestCaseTags_data
        """Add to a Test Case's Tags"""
        # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
        patchTestCaseTagsURL = "http://tcdb.inin.com/tcdbv2/api/testcases/" + str(testCaseNumber) + "/" + str(testCaseVersion) + "/tags"    # <--- Use this test env url first then Prod

                                                                                                  # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases
        # Method Headers
        patchTestCaseTags_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}
        
        # Specify request body json data and headers
        patchTestCaseTags_response = dal.tcdb.patch_a_test_case_tags_call(patchTestCaseTagsURL, patchTestCaseTags_data, patchTestCaseTags_headers)
        
        """Store the getTestCaseTags response"""
        # Use the .json function() to get the data in json format and then we store it in getTestCaseTags_response variable
        patchTestCaseTags_response = patchTestCaseTags_response.json()
        print "patchTestCaseTags_response: "
        print patchTestCaseTags_response
        return patchTestCaseTags_response


def delete_a_test_case_tag(token, testCaseNumber, testCaseVersion, tagToDelete):
        
        """Add to a Test Case's Tags"""
        # This is the gimpy URL used to add a test case to the DB                       *****IMPORTANT*****
        deleteTestCaseTagsURL = "http://tcdb.inin.com/tcdbv2/api/testcases/" + str(testCaseNumber) + "/" + str(testCaseVersion) + "/tags/" + tagToDelete + "/"   # <--- Use this test env url first then Prod
        
                                                                                                  # Prod env url: http://tcdb.inin.com/tcdbv2/api/testcases
        # Method Headers
        deleteTestCaseTags_headers = {'content-type': 'application/json',
                               'Token': '%s' % token}
                               
        print "deleteTestCaseTagsURL deleteTestCaseTagsURL deleteTestCaseTagsURL"
        print deleteTestCaseTagsURL
        
        # Specify request body json data and headers
        deleteTestCaseTags_response = dal.tcdb.delete_a_test_case_tag_call(deleteTestCaseTagsURL, deleteTestCaseTags_headers)
        print "deleteTestCaseTags_response deleteTestCaseTags_response deleteTestCaseTags_response"
        print deleteTestCaseTags_response
        """Store the getTestCaseTags response"""
        # Use the .json function() to get the data in json format and then we store it in getTestCaseTags_response variable
        deleteTestCaseTags_response = deleteTestCaseTags_response.json()
        print "deleteTestCaseTags_response: "
        print deleteTestCaseTags_response
        return deleteTestCaseTags_response
        

def checkPathConfig ():
    # Get os type
    os_type = platform.system()
    if os_type == "Windows":
        dal.config.checkWindowsConfig()
    else:
        dal.config.checkMacConfig()

def setDirPath():
    dirPath = dal.config.getDirPath()
    return dirPath


def setTemplatePath_update_01():
    templatePath_update_01 = dal.config.getTemplatePath_update_01()
    return templatePath_update_01
    
def setTemplatePath_update_02():
    templatePath_update_02 = dal.config.getTemplatePath_update_02()
    return templatePath_update_02
    
def setTemplatePath_update_03():
    templatePath_update_03 = dal.config.getTemplatePath_update_03()
    return templatePath_update_03


def setExcelPath():
    excelPath = dal.config.getExcelPath()
    return excelPath

def setTSname(name):
    dal.config.createTCSet(name)


def openWB(path):
    wb = dal.excel.openExcileFile(path)
    return wb

def openWS(path):
    ws = dal.excel.getSheet(path)
    return ws