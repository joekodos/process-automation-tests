#!/usr/bin/python



# Core Written by Alex Hernandez
# Updater Created by Joseph Kodos
# 10/03/2015

# Updated 05/23/2016 -- Version 2.0:
        #Enable updating test cases with multiple steps and multiple tags
        
""" This script will read an excel file and will automatically
    update the test cases in GIMPY.
"""
# Import all necessary modules.
import xlrd
from collections import OrderedDict
import simplejson as json
import shutil
from openpyxl import load_workbook
import bll
import sys
# Title
print "\n"
print "                                             |---- GIMPY EXPORTER v1.2 ----|"
print "\n"
print "\n"

"""Login to GIMPY"""

print "   -Login into GIMPY-"

print "\n"

# Stores the username in username variable
username = raw_input(" > What is your username: ")

# Logs user in and stores the token
loginToken = bll.db.login_gimpy(username)

print "\n"

print "  -Login Successful-"

print "\n"

"""Check Paths"""

# Check path config
bll.db.checkPathConfig()

# Set directory path
dirPath = bll.db.setDirPath()

# Set template path
templatePath_update_01 = bll.db.setTemplatePath_update_01()
templatePath_update_02 = bll.db.setTemplatePath_update_02()
templatePath_update_03 = bll.db.setTemplatePath_update_03()


# Set excel path
excelPath = bll.db.setExcelPath()

print "\n"

# Set Test Case set

name = raw_input("What do you want to name this set of Test Cases?: ")

# Set Test Case Set name

testCaseSet = dirPath + name
print "\n"
print "Your test cases will be stored here: " + testCaseSet

# Create testCaseSet

bll.db.setTSname(testCaseSet)

print "\n"
print "\n"
print "\n"

# Declare the starting position of the test cases that have been created.
print " /!\ " 
print " The Excel document may be rendered unreadable if this process is killed! "
print "\n"
print "Do NOT CTRL+C or exit this script after this point! "
print " /!\ "
print "\n"
print "\n"
ExcelQuestion = raw_input("*** Please save and close the Excel file *** \n\n"
                          "   Continue?: (Y/N) " ).lower()
if ExcelQuestion == "y":
    print "\n"
    # Declare the starting position of the test cases that have been created.
    start_row = int(raw_input(" > What row does your first test case start at: ")) - 1
    while start_row <= 1:
        start_row = int(raw_input("     > *** Please enter a number higher than 1 or 2: *** ")) - 1
else:
    sys.exit(0)

print "\n"

print "\n"

print "---------------------------- TEST CASE RESULTS---------------------------------"

print "\n"

"""Open Excel File"""

# Open the workbook and select the first worksheet
wb = bll.db.openWB(excelPath)
sh = bll.db.openWS(wb)

# List to hold dictionaries
test_cases_list = []

# List to hold feature suite id's
fs_list = []

# List to hold test Case id's
test_case_id = []

# List to hold test Case versions
test_case_version = []

# List to hold the current TC's tags
currentTCexcelTagList = []


""" Iterate through each row in worksheet and fetch values into dict  """
for rownum in range(start_row, sh.nrows):
        test_case = OrderedDict()
        #Get number of rows in excel sheet
        row_values = sh.row_values(rownum)
        #A title (row_values[0]) is required to update a test case
        if row_values[0] != "":
                test_case['Name'] = row_values[0]
                test_case['Description'] = row_values[1]
                test_case['Assumption'] = row_values[2]
                test_case['Limitation'] = row_values[3]
                test_case['Note'] = row_values[4]
                test_case['EstimatedTimeToRun'] = row_values[5]
                test_case['Data'] = row_values[6]
                test_case['State ID'] = int(row_values[7])
                test_case['Product ID'] = int(row_values[8])
                test_case['Priority ID'] = int(row_values[9])
                test_case['TestType ID'] = int(row_values[10])
                test_case['HowFound ID'] = int(row_values[11])
                test_case['CodeBase ID'] = int(row_values[12])
                test_case['Release ID'] = int(row_values[13])
                
                test_case['Action'] = row_values[14]
                test_case['Result'] = row_values[15]
                test_case['Comment'] = row_values[16]
                test_case['Test Case #'] = row_values[18]
                
                # Append the values to the test_cases_list
                test_cases_list.append(test_case)
        
        #IF FS exists
        if row_values[17] != "":
                test_case['FeatureSuite'] = row_values[17]
                # Append the values to the fs_list
                #fs_list.append(int(row_values[17]))
        
        #IF Yes Title, Yes Tags
        if row_values[0] != "" and row_values[20] != "":
                test_case['Tags'] = row_values[20]
        
        #IF No Title, Yes Action, No Tag: Acquire step from test case
        if row_values[0] == "" and row_values[14] != "" and row_values[20] == "":
                test_case['Action'] = row_values[14]
                test_case['Result'] = row_values[15]
                test_case['Comment'] = row_values[16]
                
                print "****  test_case ********"
                print test_case
                #wait = input("PRESS ENTER TO CONTINUE.")
                

                # Append the values to the test_cases_list
                test_cases_list.append(test_case)
                
                
        #IF No Title, Yes Action, Yes Tag: Acquire step and tag from test case
        if row_values[0] == "" and row_values[14] != "" and row_values[20] != "":
                test_case['Action'] = row_values[14]
                test_case['Result'] = row_values[15]
                test_case['Comment'] = row_values[16]
                test_case['Tags'] = row_values[20]
                
                # Append the values to the test_cases_list
                test_cases_list.append(test_case)
        
        #IF No Title, No Action, Yes Tag:
        if row_values[0] == "" and row_values[14] == "" and row_values[20] != "":
                test_case['Tags'] = row_values[20]
                

                # Append the values to the test_cases_list
                test_cases_list.append(test_case)
        

"""
    Iterate through every item on the list, create a file for it
    and copy the information from the excel information to the template file copied.
"""

#Above: grab all data
#
# tcTracker -- array of tcs and steps
#       e.g. ["tc", "step", "step", "tc", "tc", "step"]
tcTracker = []

for j in range(len(test_cases_list)):
        #Need to traverse through array to detect TCs and Steps
        if ('Name' in test_cases_list[j]):
                tcTracker.append("tc")
                #print "XXXXXXXXXX tcTracker  tc  tc  tc"
                #print tcTracker
                        
        if not ('Name' in test_cases_list[j]):
                if ('Action' in test_cases_list[j]):
                        if ('Tags' in test_cases_list[j]):
                                tcTracker.append("stepTag")
                                #print "XXXXXXXXXX tcTracker stepTag stepTag stepTag"
                                #print tcTracker
                        else:
                                tcTracker.append("step")
                                #print "XXXXXXXXXX tcTracker step step step"
                                #print tcTracker
                else:
                        tcTracker.append("tag")
                        #print "XXXXXXXXXX tcTracker  tag tag tag"
                        #print tcTracker
                


# currentTCtracker -- array of tc status
#       e.g. ["startTC","endTC","startTC","midTC","midTC","endTC","oneStepTC"]
#       "startTC" --> a full TC entry, but a step, a tag, or a stepTag will follow
#       "midTC" --> the current and next entry are step or stepTag or tag
#       "endTC" --> the current entry is a step, tag, or stepTag and the next entry is a TC 
#       "oneStepTC" --> the current entry is a TC and the next entry is a TC
#

print "tcTracker tcTracker tcTracker tcTracker tcTracker"
print tcTracker
currentTCtracker = []

#For each item in tcTracker
for k in range(len(tcTracker)):
        #if current index is the length of tcTracker list
        #e.g. len(tcTracker) == 5;
        #     k+1 = 5 (5th iteration: 0,1,2,3,4) == len(tcTracker) = 1,2,3,4,5
        #If 1+currentIndex is equal to length of the list --> we're at the end
        if(k+1 == len(tcTracker)):
                print "0xAAA  ENTERING LAST TEST CASE"
                if(tcTracker[-1] == "tc"):
                        tcFlag = "oneStepTC"
                        if not k == len(tcTracker):
                                currentTCtracker.append(tcFlag)
                                print "LAST! TC! -- Appended oneStepTC ; current is TC"
                                print "(tcTracker[-1])"
                                print tcTracker[-1]
                                print "\n"
                        
                #if last entry is step or tag or stepTag... end the current TC
                if(tcTracker[-1] == "step" or tcTracker[-1] == "stepTag" or tcTracker[-1] == "tag"):
                        tcFlag = "endTC"
                        currentTCtracker.append(tcFlag)
                        print "LAST! TC! -- Appended endTC ; current is step"
                        print "(tcTracker[-1])"
                        print tcTracker[-1]
                        print "\n"
        else:
                #Need to mark where test cases begin and end, and if a tc has only one step
                if (tcTracker[k] == "tc" and (tcTracker[k+1] == "step" or tcTracker[k+1] == "tag" or tcTracker[k+1] == "stepTag")):
                        tcFlag = "startTC"
                        currentTCtracker.append(tcFlag)
                        print "NOT Last TC -- Appended startTC ; current is TC and next (tcTracker[k+1]) is step/tag/stepTag"
                        print "(tcTracker[k])"
                        print tcTracker[k]
                        print "(tcTracker[k+1])"
                        print tcTracker[k+1]
                        print "\n"
                
                #If current is a Step, stepTag, or tag, and next is TC
                if ((tcTracker[k] == "step" or tcTracker[k] == "stepTag" or tcTracker[k] == "tag") and tcTracker[k+1] == "tc"):
                        #if current is a step and next is a tc
                        tcFlag = "endTC"
                        currentTCtracker.append(tcFlag)
                        print "NOT Last TC -- Appended endTC ; current is step and next (tcTracker[k+1]) is TC"
                        print "(tcTracker[k])"
                        print tcTracker[k]
                        print "(tcTracker[k+1])"
                        print tcTracker[k+1]
                        print "\n"
                
                #if current is step and next is a step
                if ((tcTracker[k] == "step" or tcTracker[k] == "stepTag" or tcTracker[k] == "tag") and (tcTracker[k+1] == "step" or tcTracker[k+1] == "stepTag" or tcTracker[k+1] == "tag")):
                        tcFlag = "midTC"
                        currentTCtracker.append(tcFlag)
                        print "NOT Last TC -- Appended midTC ; tcTracker[k] is step/tag/stepTag and next is also a step"
                        print "(tcTracker[k])"
                        print tcTracker[k]
                        print "(tcTracker[k+1])"
                        print tcTracker[k+1]
                        print "\n"
                        
                #if current and next is a tc
                if(tcTracker[k] == "tc" and tcTracker[k+1] == "tc"):
                        tcFlag = "oneStepTC"
                        print "NOT Last TC -- Appended oneStepTC ; current is TC and next is TC"
                        currentTCtracker.append(tcFlag)
                        print "(tcTracker[k])"
                        print tcTracker[k]
                        print "(tcTracker[k+1])"
                        print tcTracker[k+1]
                        print "\n"


test_case_id_str_counter = 0
currentTCTagList = []
tcIDtracker = 0
#For Each Test Case (or set of TC Steps):
for i in range(len(test_cases_list)):
        #Need to Create File based on # of Steps and on # of Issues (if applicable)
    
        #if a new TC
        if(currentTCtracker[i] == "startTC"):
                
                #New TC, reset tag list
                currentTCTagList = []
                #New TC, reset currentTCexcelTagList
                currentTCexcelTagList = []
                
                #New TC, reset fs list
                currentTC_FSList = []
                
                #New TC, reset currentTCexcelFSList
                currentTCexcelFSList = []
                
                #When writing file, use this to write to the correct file
                curTCindex = i
                
                #Add test case number to TC ID list
                test_case_id.append(test_cases_list[i]['Test Case #'])

                test_case_id_str = str(test_case_id[test_case_id_str_counter])
                test_case_id_trimmed = test_case_id_str.split('.')
                
                #New TC, increment test_case_id_str_counter
                test_case_id_str_counter = test_case_id_str_counter + 1
                
                """Get the Test Case Version"""
                """Get the Test Case Feature Suite List"""
                getTestCase_response = bll.db.get_a_test_case(loginToken, test_case_id_trimmed[0])
                #add to FS list if FS exists in TCDB TC
                if len(getTestCase_response['TestSuites']) > 0:
                        currentTC_FSList.append(getTestCase_response['TestSuites'][0]['Id'])
                
                
                #Get existing tags
                getTestCaseTagsResponse = bll.db.get_a_test_case_tags(loginToken, test_case_id_trimmed[0], getTestCase_response['Version'])
                #print "getTestCaseTagsResponse getTestCaseTagsResponse getTestCaseTagsResponse"
                #print getTestCaseTagsResponse
                try:
                        for tag in range(len(getTestCaseTagsResponse)):
                                currentTCTagList.append(getTestCaseTagsResponse[tag])
                                
                except Exception as exc:
                    print "no Tags", exc
                else:
                    if getTestCaseTagsResponse:
                        print "tags exist"
                    else:
                        print "no tags here!"
                        
                #Add to current tc tag excel list
                if('Tags' in test_cases_list[i]):
                        currentTCexcelTagList.append(test_cases_list[i]['Tags'])
                        
                #Add to current tc fs excel list
                if('FeatureSuite' in test_cases_list[i]):
                        currentTCexcelFSList.append(test_cases_list[i]['FeatureSuite'])
                
                #Assemble update_a_test_case
                currentTestCase = open(templatePath_update_01, "r").read()
                #for first step
                currentTestCase = currentTestCase + open(templatePath_update_02, "r").read()
                #append comma
                currentTestCase = currentTestCase + ","
                #print "AAAAAAAAAAAAAAAAAAAAAAAAAA"
                #print currentTestCase
                #print "AAAAAAAAAAAAAAAAAAAAAAAAAA"
        
        #if complete TC
        if(currentTCtracker[i] == "oneStepTC"):
                #New TC, reset currentTCexcelTagList
                currentTCexcelTagList = []
                
                #New TC, reset currentTCexcelFSList
                currentTCexcelFSList = []
                
                #New TC, reset tag list
                currentTCTagList = []
                
                #New TC, reset fs list
                currentTC_FSList = []
                
                #When writing file, use this to write to the correct file
                curTCindex = i

                #Add test case number to TC ID list
                test_case_id.append(test_cases_list[i]['Test Case #'])

                test_case_id_str = str(test_case_id[test_case_id_str_counter])
                test_case_id_trimmed = test_case_id_str.split('.')
                
                #New TC, increment test_case_id_str_counter
                test_case_id_str_counter = test_case_id_str_counter + 1
                
                """Get the Test Case Version"""
                """Get the Test Case Feature Suite List"""
                getTestCase_response = bll.db.get_a_test_case(loginToken, test_case_id_trimmed[0])
                #add to FS list if FS exists in TCDB TC
                if len(getTestCase_response['TestSuites']) > 0:
                        currentTC_FSList.append(getTestCase_response['TestSuites'][0]['Id'])
                
                #Get existing tags
                getTestCaseTagsResponse = bll.db.get_a_test_case_tags(loginToken, test_case_id_trimmed[0], getTestCase_response['Version'])
                try:
                        for tag in range(len(getTestCaseTagsResponse)):
                                currentTCTagList.append(getTestCaseTagsResponse[tag])
                                
                except Exception as exc:
                    print "no Tags", exc
                else:
                    if getTestCaseTagsResponse:
                        print "tags exist"
                    else:
                        print "no tags here!"
                        
                
                #Add to current tc tag excel list
                if('Tags' in test_cases_list[i]):
                        currentTCexcelTagList.append(test_cases_list[i]['Tags'])
                
                #Add to current tc fs excel list
                if('FeatureSuite' in test_cases_list[i]):
                        currentTCexcelFSList.append(test_cases_list[i]['FeatureSuite'])
                
                #Assemble update_a_test_case
                currentTestCase = open(templatePath_update_01, "r").read()
                #for first step
                currentTestCase = currentTestCase + open(templatePath_update_02, "r").read()
                #append end
                currentTestCase = currentTestCase + open(templatePath_update_03, "r").read()
                print "oneStepTC ASSEMBLY oneStepTC ASSEMBLY "
                #print currentTestCase
                print "oneStepTC ASSEMBLY oneStepTC ASSEMBLY "
                
        
        #if a step
        if(currentTCtracker[i] == "midTC"):
                
                #Add to current tc tag excel list
                if('Tags' in test_cases_list[i]):
                        currentTCexcelTagList.append(test_cases_list[i]['Tags'])
                
                #Add to current tc fs excel list
                if('FeatureSuite' in test_cases_list[i]):
                        currentTCexcelFSList.append(test_cases_list[i]['FeatureSuite'])
                
                #append step
                currentTestCase = currentTestCase + open(templatePath_update_02, "r").read()
                #append comma
                currentTestCase = currentTestCase + ","
                print "midTC ASSEMBLY midTC ASSEMBLY "
                #print currentTestCase
                print "midTC ASSEMBLY midTC ASSEMBLY "
                
        
        #if a final step
        if(currentTCtracker[i] == "endTC"):
                print "test_cases_list[i]  $$$$"
                print test_cases_list[i]
                
                #Add to current tc tag excel list
                if('Tags' in test_cases_list[i]):
                        currentTCexcelTagList.append(test_cases_list[i]['Tags'])
                        
                #Add to current tc fs excel list
                if('FeatureSuite' in test_cases_list[i]):
                        currentTCexcelFSList.append(test_cases_list[i]['FeatureSuite'])
                
                #append comma
                #currentTestCase = currentTestCase + ","
                #append final step
                currentTestCase = currentTestCase + open(templatePath_update_02, "r").read()
                #append end
                currentTestCase = currentTestCase + open(templatePath_update_03, "r").read()
                print "endTC ASSEMBLY endTC ASSEMBLY "
                #print currentTestCase
                print "endTC ASSEMBLY endTC ASSEMBLY "
                
        
        #Write template file
        templateFile = open(testCaseSet + '/%s%d.json' % (name,i), 'w')
        templateFile.write(currentTestCase)
        templateFile.close()




        print "currentTCtracker[i]"
        print currentTCtracker[i]
        print "currentTCtracker"
        print currentTCtracker
        
        print "iiiii xxxxxx"
        print i
        
        
        #if a completeTC
        if(currentTCtracker[i] == "endTC" or currentTCtracker[i] == "oneStepTC"):
                print "currentTCtracker 0000EEEEE"
                print currentTCtracker
                
                
                
                
                """Update the Feature suite"""
                fsInTCDB_NotExcel_toDelete = []
                fsInExcel_NotTCDB_toAdd = []
                
                
                #if currentTC_FSList is empty, and currentTCexcelFSList is NOT empty... //main TC row has a FS
                if ( len(currentTC_FSList) == 0 and len(currentTCexcelFSList) >= 1):
                        #Add TC to FS's
                        for fsAdd in range(len(currentTCexcelFSList)):
                               updatefs_response = bll.db.update_feature_suite(str(currentTCexcelFSList[fsAdd]), test_case_id_trimmed[0], loginToken)
                               print "updatefs_response  updatefs_response  updatefs_response"
                               print updatefs_response
                               
                #if currentTC_FSList is NOT empty, and currentTCexcelFSList is NOT empty... //main TC row has a fs
                if ( len(currentTC_FSList) >= 1 and len(currentTCexcelFSList) >= 1):
                        #For each fs in TCDBv2 TC
                        for fsAA in range(len(currentTC_FSList)):
                                #if a fs exists in TCDB but not in excel...
                                if currentTC_FSList[fsAA] not in currentTCexcelFSList:
                                        fsInTCDB_NotExcel_toDelete.append(currentTC_FSList[fsAA])
                        
                        #For each fs in the excel file TC
                        for fsBB in range(len(currentTCexcelFSList)):
                                #if a fs exists in excel but not in TCDBv2...
                                if currentTCexcelFSList[fsBB] not in currentTC_FSList:
                                        fsInExcel_NotTCDB_toAdd.append(currentTCexcelFSList[fsBB])
                        
                        #Delete test case fs in respective list
                        if len(fsInTCDB_NotExcel_toDelete) > 0:
                                for tcDelete in range(len(fsInTCDB_NotExcel_toDelete)):
                                        deleteTC_FromFS_response = bll.db.delete_tc_from_feature_suite(fsInTCDB_NotExcel_toDelete[tcDelete], test_case_id_trimmed[0], loginToken)
                                        print "deleteTC_FromFS_response deleteTC_FromFS_response AAAAAAAA"
                                        print deleteTC_FromFS_response
                        
                        
                        #Add Test Case to FS
                        if len(fsInExcel_NotTCDB_toAdd) > 0:
                                #Add TC to FS's
                                for fsAdd in range(len(currentTCexcelFSList)):
                                       updatefs_response = bll.db.update_feature_suite(currentTCexcelFSList[fsAdd], test_case_id_trimmed[0], loginToken)
                                       print "updatefs_response  updatefs_response  updatefs_response"
                                       print updatefs_response
                                       
                                       
                #if currentTC_FSList is NOT empty, and currentTCexcelFSList is empty... //delete fs
                if ( len(currentTC_FSList) >= 1 and len(currentTCexcelFSList) == 0):
                
                        #For each fs in TCDBv2 TC
                        for fsCC in range(len(currentTC_FSList)):
                                #if a fs exists in TCDB but not in excel...
                                if currentTC_FSList[fsCC] not in currentTCexcelFSList:
                                        fsInTCDB_NotExcel_toDelete.append(currentTC_FSList[fsCC])
                                        
                        #Delete test case tags in respective list
                        if len(fsInTCDB_NotExcel_toDelete) > 0:
                                for tcDelete in range(len(fsInTCDB_NotExcel_toDelete)):
                                        deleteTC_FromFS_response = bll.db.delete_tc_from_feature_suite(fsInTCDB_NotExcel_toDelete[tcDelete], test_case_id_trimmed[0], loginToken)
                                        print "deleteTC_FromFS_response deleteTC_FromFS_response BBBBBBBBBB"
                                        print deleteTC_FromFS_response
                                       
                
                """Update the Tags"""
                #reset temp lists
                tagsInTCDB_NotExcel_toDelete = []
                tagsInExcel_NotTCDB_toAdd = []
                tagAssembler = ""
                
                #if currentTCTagList is empty, and currentTCexcelTagList is NOT empty... //main TC row has a tag
                print " **** START TAGGING  ****  START TAGGING **** START TAGGING ***"
                print "currentTCTagList"
                print currentTCTagList
                print "currentTCexcelTagList"
                print currentTCexcelTagList
                
                if ( len(currentTCTagList) == 0 and len(currentTCexcelTagList) >= 1):
                        #Assemble TagsToAdd:
                        #       ["tag1","tag2","tag3"]
                        #       ["tag1"]
                        
                        for toAdd in range(len(currentTCexcelTagList)):
                                if toAdd == (len(currentTCexcelTagList) - 1):
                                        tagAssembler = tagAssembler + "'" + str(currentTCexcelTagList[toAdd]) + "'"
                                else:
                                        tagAssembler = tagAssembler + "'" + str(currentTCexcelTagList[toAdd]) + "',"
                                
                                jsonTagsToAdd = "[" +  tagAssembler + "]"
                                print "jsonTagsToAdd AAAAA"
                                print jsonTagsToAdd
                                patchTestCaseResponse = bll.db.patch_a_test_case_tags(loginToken, test_case_id_trimmed[0], getTestCase_response['Version'], jsonTagsToAdd)
                
                #if currentTCTagList is NOT empty, and currentTCexcelTagList is NOT empty... //main TC row has a tag
                if ( len(currentTCTagList) >= 1 and len(currentTCexcelTagList) >= 1):
                        
                        #For each tag in TCDBv2 TC
                        for tagTT in range(len(currentTCTagList)):
                                #if a tag exists in TCDB but not in excel...
                                if currentTCTagList[tagTT] not in currentTCexcelTagList:
                                        tagsInTCDB_NotExcel_toDelete.append(currentTCTagList[tagTT])
                        
                        #For each tag in the excel file TC
                        for tagTY in range(len(currentTCexcelTagList)):
                                #if a tag exists in excel but not in TCDBv2...
                                if currentTCexcelTagList[tagTY] not in currentTCTagList:
                                        tagsInExcel_NotTCDB_toAdd.append(currentTCexcelTagList[tagTY])
                        
                        #Delete test case tags in respective list
                        if len(tagsInTCDB_NotExcel_toDelete) > 0:
                                for toDelete in range(len(tagsInTCDB_NotExcel_toDelete)):
                                        deleteTestCaseResponse = bll.db.delete_a_test_case_tag(loginToken, test_case_id_trimmed[0], getTestCase_response['Version'], tagsInTCDB_NotExcel_toDelete[toDelete])
                        
                        
                        #Add Test Case Tags to TC
                        if len(tagsInExcel_NotTCDB_toAdd) > 0:
                                #Assemble TagsToAdd:
                                #       ["tag1","tag2","tag3"]
                                #       ["tag1"]
                                for toAdd in range(len(tagsInExcel_NotTCDB_toAdd)):
                                        if toAdd == (len(tagsInExcel_NotTCDB_toAdd) - 1):
                                                tagAssembler = tagAssembler + "'" + tagsInExcel_NotTCDB_toAdd[toAdd] + "'"
                                        else:
                                                tagAssembler = tagAssembler + "'" + tagsInExcel_NotTCDB_toAdd[toAdd] + "',"
                                
                                jsonTagsToAdd = "[" +  tagAssembler + "]"
                                print "jsonTagsToAdd XXXXX"
                                print jsonTagsToAdd
                                patchTestCaseResponse = bll.db.patch_a_test_case_tags(loginToken, test_case_id_trimmed[0], getTestCase_response['Version'], jsonTagsToAdd)
                
                
                #if currentTCTagList is NOT empty, and currentTCexcelTagList is empty... //delete tags
                if ( len(currentTCTagList) >= 1 and len(currentTCexcelTagList) == 0):
                
                        #For each tag in TCDBv2 TC
                        for tagTN in range(len(currentTCTagList)):
                                #if a tag exists in TCDB but not in excel...
                                if currentTCTagList[tagTN] not in currentTCexcelTagList:
                                        tagsInTCDB_NotExcel_toDelete.append(currentTCTagList[tagTN])
                                        
                        #Delete test case tags in respective list
                        if len(tagsInTCDB_NotExcel_toDelete) > 0:
                                for toDelete in range(len(tagsInTCDB_NotExcel_toDelete)):
                                        deleteTestCaseResponse = bll.db.delete_a_test_case_tag(loginToken, test_case_id_trimmed[0], getTestCase_response['Version'], tagsInTCDB_NotExcel_toDelete[toDelete])
                
                """Update the Test Case"""
                #At this point, we deleted each tag that is in TCDBv2 but not Excel
                #We also added a list of tags that is in Excel but not TCDBv2
                #Next, we need to Write multiple Steps and Tags (or delete) back to the template file and then Update TC in TCDBv2
                
                # Opens, loads file we want to be written and stores it in the "data" variable
                jsonFile = open(testCaseSet + '/%s%d.json' % (name,i), "r")
                data = json.load(jsonFile, object_pairs_hook=OrderedDict)
                jsonFile.close()

                # Fetches each entity in JSON file and replaces element with new imported information
                print "test_cases_list[curTCindex] test_cases_list[curTCindex] test_cases_list[curTCindex] test_cases_list[curTCindex] "
                print test_cases_list[curTCindex]
                data["Name"] = test_cases_list[curTCindex]['Name']
                data['Description'] = test_cases_list[curTCindex]['Description']
                data['Assumption'] = test_cases_list[curTCindex]['Assumption']
                data['Limitation'] =  test_cases_list[curTCindex]['Limitation']
                data['Note'] = test_cases_list[curTCindex]['Note']
                data['EstimatedTimeToRun'] = test_cases_list[curTCindex]['EstimatedTimeToRun']
                data['Data'] = test_cases_list[curTCindex]['Data']
                data['State']['Id'] = test_cases_list[curTCindex]['State ID']
                data['Product']['Id'] = test_cases_list[curTCindex]['Product ID']
                data['Priority']['Id'] = test_cases_list[curTCindex]['Priority ID']
                data['TestType']['Id'] = test_cases_list[curTCindex]['TestType ID']
                data['HowFound']['Id'] = test_cases_list[curTCindex]['HowFound ID']
                data['CodeBase']['Id'] = test_cases_list[curTCindex]['CodeBase ID']
                data['Release']['Id'] = test_cases_list[curTCindex]['Release ID']
                print "len(data['Steps']) mmmmm"
                print len(data['Steps'])
                tempIndex = curTCindex
                for m in range(len(data['Steps'])):
                        data['Steps'][m]['Action'] = test_cases_list[tempIndex]['Action']
                        data['Steps'][m]['ExpectedResult'] = test_cases_list[tempIndex]['Result']
                        data['Steps'][m]['Comment'] = test_cases_list[tempIndex]['Comment']
                        tempIndex = tempIndex + 1
                
                
                """Update the Test Case"""
                # tc_id will hold the value of the id in the test_case_id list
                #tc_id = str(test_case_id[curTCindex])
                updateTestCase_response = bll.db.update_a_test_case(data, loginToken, test_case_id_trimmed[0], getTestCase_response['Version'])

                # Print the results of test cases of each test case that has been created
                print "Test Case"
                print "---------"
                print "Test Case Name:" +    str(data['Name'])
                print "Test Case ID: " + str(updateTestCase_response['Id'])
                print "Updated Successfully!"

                # Append the responses to test_case_version list to write to excel file
                test_case_version.append(updateTestCase_response['Version'])


                """Writes new information to new file and closes it"""

                jsonFile = open(testCaseSet + "/%s%d.json" % (name,i), "w+")
                jsonFile.write(json.dumps(data['Name'], indent=4 * ' '))
                jsonFile.write(json.dumps(updateTestCase_response, indent=4 * ' '))
                jsonFile.close()

                print "\n"

    
"""Writes the Test Case Version back to the excel file"""
# Load the excel file
wb = load_workbook(filename=excelPath)
# Get the sheet
ws = wb.get_sheet_by_name("Sheet1")
row1 = start_row + 1


#item counter for test_case_version array
item = 0

#Iterate over tcs and steps
for entry in range(len(tcTracker)):
        #Check if current row is blank
        if str(ws['A'+str(row1)].value) == "None" and str(ws['O'+str(row1)].value) == "None":
                print "Entering blank row!"
                #Skip 3 excel rows
                row1 = row1 + 3
                # Write the test case test_case_version id to the excel file
                ws['T'+str(row1)] = test_case_version[item]
                #Move to next excel row
                row1 = row1 + 1
                #Increment test_case_version counter
                item = item + 1
        #Check if current row is just a step
        elif str(ws['A'+str(row1)].value) == "None" and str(ws['O'+str(row1)].value) != "None":
                #Skip the row
                row1 = row1 + 1
        #Else, current row is not blank and not a step -- a full TC
        else:
                # Write the test_case_version to the excel file
                ws['T'+str(row1)] = test_case_version[item]
                row1 = row1 + 1
                item = item + 1



# save the excel file
wb.save(excelPath)

print "\n"

print "---------------------------- UPDATES COMPLETED---------------------------------"

print "\n"















