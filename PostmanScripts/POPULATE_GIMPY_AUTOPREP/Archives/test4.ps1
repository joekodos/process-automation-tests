function touch {set-content -Path ($args[0]) -Value ($null)}
function GetLineAt([String] $path, [Int32] $index)
{
    [System.IO.FileMode] $mode = [System.IO.FileMode]::Open;
    [System.IO.FileAccess] $access = [System.IO.FileAccess]::Read;
    [System.IO.FileShare] $share = [System.IO.FileShare]::Read;
    [Int32] $bufferSize = 16 * 1024;
    [System.IO.FileOptions] $options = [System.IO.FileOptions]::SequentialScan;
    [System.Text.Encoding] $defaultEncoding = [System.Text.Encoding]::UTF8;
    # FileStream(String, FileMode, FileAccess, FileShare, Int32, FileOptions) constructor
    # http://msdn.microsoft.com/library/d0y914c5.aspx
    [System.IO.FileStream] $input = New-Object `
        -TypeName 'System.IO.FileStream' `
        -ArgumentList ($path, $mode, $access, $share, $bufferSize, $options);
    # StreamReader(Stream, Encoding, Boolean, Int32) constructor
    # http://msdn.microsoft.com/library/ms143458.aspx
    [System.IO.StreamReader] $reader = New-Object `
        -TypeName 'System.IO.StreamReader' `
        -ArgumentList ($input, $defaultEncoding, $true, $bufferSize);
    [String] $line = $null;
    [Int32] $currentIndex = 0;

    try
    {
        while (($line = $reader.ReadLine()) -ne $null)
        {
            if ($currentIndex++ -eq $index)
            {
                return $line;
            }
        }
    }
    finally
    {
        # Close $reader and $input
        $reader.Close();
    }

    # There are less than ($index + 1) lines in the file
    return $null;
}
if (-Not (Test-Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\prepared.txt) ) {
        touch prepared.txt
}
$test_case_prepared_set = (Get-Content U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\TC_current_prepared_set.txt)
$test_case_array = @()
for($ee=0; $ee -le $test_case_prepared_set.Length; $ee++){
        $test_case_array  += GetLineAt 'U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\TC_current_prepared_set.txt' $ee;
}

for($aa=0; $aa -le $test_case_prepared_set.Length; $aa++){
        #$test_case_array[0] will contain 1st line of file
        #Determine Test Case Boundary & Increment TC Counter
        if ( $test_case_array[$aa] -eq "::----------------------------------------------------------------------------------------------------------------------------------------::" ) {
                $currentTestCasePos = $currentTestCasePos + 1;
                Write-Host "$currentTestCasePos currentTestCasePos"
                $test_case_arrayInternal = @()
                for($hh=$aa; $hh -le $aa+8; $hh++){
                        Write-Host "$hh Inside Loop Counter"
                        $test_case_arrayInternal  += GetLineAt 'U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\TC_current_prepared_set.txt' $hh;
                        #$test_case_arrayInternal[0] will contain 1st line of file
                        
                        #Get Current Test Case Number
                        if ( $test_case_arrayInternal[$hh] -like 'TC#*' ) {
                                $curLine = $test_case_arrayInternal[$hh].Split("=");
                                $currentTestCaseNum = $curLine[1];
                        }
                        
                        #Get Current Test Case Version
                        if ( $test_case_arrayInternal[$hh] -like 'Version*' ) {
                                $curLine = $test_case_arrayInternal[$hh].Split("=");
                                $currentTestCaseVer = $curLine[1];
                        }
                        
                        #Get Current Test Case Contents
                        if ( $test_case_arrayInternal[$hh] -like '*|*' ) {
                                #Get Current Test Case Blob
                                Add-Content -Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\prepared.txt -Value $test_case_arrayInternal[$hh]
                        }
                        Write-Host "$currentTestCaseNum currentTestCaseNum"
                } #END NESTED FOR
                #$aa = $hh
                Write-Host "$aa OutSideLoop Counter"
                #Execute POPULATE_GIMPY_TEMPLATE.BAT
                U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\POPULATE_GIMPY_TEMPLATE.bat
                #Start-Sleep -s 2
                
                #Rename Insert_Gimpy_TC_ready.txt
                if ( Test-Path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\Insert_Gimpy_TC_ready.txt ) {
                #        Rename-Item -path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\Insert_Gimpy_TC_ready.txt -newname $hh.txt
                        #Rename-Item -path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\Insert_Gimpy_TC_ready.txt -newname ($currentTestCaseNum + "v" + $currentTestCaseVer + ".txt")
                        Copy-Item U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\Insert_Gimpy_TC_ready.txt -destination ($currentTestCaseNum + "v" + $currentTestCaseVer + ".txt")
                        Remove-Item -path U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\Insert_Gimpy_TC_ready.txt -force
                }
                #Clear Prepared.Txt, ready for next TestCase
                Clear-Content U:\PostmanScripts\POPULATE_GIMPY_AUTOPREP\prepared.txt
        } #END IF
} #END FOR