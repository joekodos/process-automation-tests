Title=          |API - Unlock Versioned Flow Without Definition
Description=    |Verify the correct behavior upon unlocking a versioned flow that has no flow definition
Priority=       |P1
Assumptions=    |A flow with a version exists.
Step00Action=   |Call an endpoint to unlock the (base) flow while it has a version with no definition uploaded for it
Step00Result=   |A HTTP response is returned indicating the operation was successful