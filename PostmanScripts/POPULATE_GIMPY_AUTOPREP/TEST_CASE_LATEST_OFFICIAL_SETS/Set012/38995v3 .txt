<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gim="http://gimpy.i3domain.inin.com">
  <soapenv:Header/>
  <soapenv:Body>
    <gim:CreateInactiveTestCase>
      <!--Optional:-->
      <gim:testCaseXml>       
                <![CDATA[         
    <testcases username="{{gimpyUsername}}">
    <testcase codebase="PureCloud" id="{{testCaseId}}" name="ClientUI - Work Item Edit Mode - Text Edit Box Bound Integer - Invalid" priority="P4" state="Inactive">
        <description>Verify the correct behavior upon entering text into a text edit box bound to an integer variable during edit mode</description>
        <estimatedruntime>0.20</estimatedruntime>
        <assumptions>A Work Item exists in the queue.
 The current user owns the Work Item.
 A text edit box bound to an Integer variable exists on the Work Item</assumptions>
        <limitations> </limitations>
        <notes> </notes>
        <data> </data>
        <type>Public API Endpoint Only</type>
        <howfound>Tester Input</howfound>
        <targetrelease>PureCloud 1.0GA</targetrelease>
        <pscim>
            <product>PureCloud</product>
        </pscim>
        <teststeps>
            <teststep action="Enter three strings (an alphanumeric, a period, and an umlaut character) in a text edit box bound to an Integer variable" expectedresult="The field is marked as invalid each time because only numeric characters are allowed"/>
        </teststeps>
    </testcase>
    </testcases>
		]]>
      </gim:testCaseXml>
    </gim:CreateInactiveTestCase>
  </soapenv:Body>
</soapenv:Envelope>